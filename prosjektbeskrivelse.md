# Prosjektbeskrivelse

Vi skal implementere spillet pong. Til input skal trykkknappene på utviklingskortet brukes. Vi ønsker å vise spillet på en ekstern skjerm, vi skal derfor implementere en VGA-kontroller i hardware for å løse dette problemet. Resten av spillet slik som spilllogik og grafikk blir implementert i software, da ingen av oppgavene har behov for å aksellereres i hardware. Tanken er at prossesoren leser input fra kanppen, før den så rendrer grafikken for neste frame. Denne blir sendt til onchip minne, fra minnet blir pixlene hentet ut av VGA kontrolleren som bestemmer timingen for signalet og hvilken pixler som blir sendt ut når. Vi er også avhengige av DACen som er innbygd i utviklingskortet for å gjøre om våre 8-bits verdier til analoge singaler for rød, grønn og blå i området 0-0.7V. 

## Hvordan kan dette prosjektet knyttes til FNs bærekraftsmål?

Vi tenker at prosjektet har et stort fokus på gjenbruk da alt av utstyr som blir brukt er gammelt og utdatert. Vi ønsker derfor å gi det et nytt liv der det fortsatt kan være til glede. Det skal også nevnes at hele prosjektet kan drives av ren norsk strøm. 

## Implementeres i hardware

- VGA-kontroller
- Prosessor
- Sammenkobling
- Minne

## Implementeres i software

- Lese input fra knapper
- Spilllogikk
- Grafikk
