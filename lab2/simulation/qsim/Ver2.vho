-- Copyright (C) 2018  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 18.1.0 Build 625 09/12/2018 SJ Lite Edition"

-- DATE "01/20/2022 11:21:19"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_ASDO_DATA1~	=>  Location: PIN_F4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_FLASH_nCE_nCSO~	=>  Location: PIN_E2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DCLK~	=>  Location: PIN_P3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DATA0~	=>  Location: PIN_N7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_nCEO~	=>  Location: PIN_P28,	 I/O Standard: 2.5 V,	 Current Strength: 8mA


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~padout\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~padout\ : std_logic;
SIGNAL \~ALTERA_DATA0~~padout\ : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_DATA0~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	StudentDesign IS
    PORT (
	DVO : OUT std_logic;
	RESULT_A : OUT std_logic_vector(15 DOWNTO 0);
	C : IN std_logic_vector(7 DOWNTO 0);
	A : IN std_logic_vector(15 DOWNTO 0);
	RESULT_B : OUT std_logic_vector(15 DOWNTO 0);
	STATUS : OUT std_logic_vector(7 DOWNTO 0);
	CLK : IN std_logic;
	RESET_N : IN std_logic;
	DVI : IN std_logic;
	B : IN std_logic_vector(15 DOWNTO 0);
	D : IN std_logic_vector(7 DOWNTO 0)
	);
END StudentDesign;

-- Design Ports Information
-- DVO	=>  Location: PIN_AE15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_A[15]	=>  Location: PIN_V7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_A[14]	=>  Location: PIN_W4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_A[13]	=>  Location: PIN_W8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_A[12]	=>  Location: PIN_AA5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_A[11]	=>  Location: PIN_Y7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_A[10]	=>  Location: PIN_AH4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_A[9]	=>  Location: PIN_AD7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_A[8]	=>  Location: PIN_AB5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_A[7]	=>  Location: PIN_AF4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_A[6]	=>  Location: PIN_AD4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_A[5]	=>  Location: PIN_AC5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_A[4]	=>  Location: PIN_AB6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_A[3]	=>  Location: PIN_AF6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_A[2]	=>  Location: PIN_AC4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_A[1]	=>  Location: PIN_AG4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_A[0]	=>  Location: PIN_AC7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- C[7]	=>  Location: PIN_J28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- C[6]	=>  Location: PIN_J27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- C[5]	=>  Location: PIN_D13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- C[3]	=>  Location: PIN_AE8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- C[2]	=>  Location: PIN_L27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- C[1]	=>  Location: PIN_J4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- C[0]	=>  Location: PIN_AE21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_B[15]	=>  Location: PIN_AB11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_B[14]	=>  Location: PIN_A11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_B[13]	=>  Location: PIN_V6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_B[12]	=>  Location: PIN_AF8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_B[11]	=>  Location: PIN_A19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_B[10]	=>  Location: PIN_U25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_B[9]	=>  Location: PIN_W2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_B[8]	=>  Location: PIN_D22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_B[7]	=>  Location: PIN_U4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_B[6]	=>  Location: PIN_C6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_B[5]	=>  Location: PIN_AD17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_B[4]	=>  Location: PIN_B11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_B[3]	=>  Location: PIN_L8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_B[2]	=>  Location: PIN_AB23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_B[1]	=>  Location: PIN_J12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESULT_B[0]	=>  Location: PIN_C8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- STATUS[7]	=>  Location: PIN_AD24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- STATUS[6]	=>  Location: PIN_B19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- STATUS[5]	=>  Location: PIN_H25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- STATUS[4]	=>  Location: PIN_G13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- STATUS[3]	=>  Location: PIN_T7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- STATUS[2]	=>  Location: PIN_P2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- STATUS[1]	=>  Location: PIN_AE20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- STATUS[0]	=>  Location: PIN_A12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLK	=>  Location: PIN_D7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RESET_N	=>  Location: PIN_AH10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DVI	=>  Location: PIN_B3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[15]	=>  Location: PIN_Y16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[14]	=>  Location: PIN_AE16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[13]	=>  Location: PIN_C9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[12]	=>  Location: PIN_AB20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[11]	=>  Location: PIN_F7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[10]	=>  Location: PIN_AC8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[9]	=>  Location: PIN_AF25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[8]	=>  Location: PIN_C3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[7]	=>  Location: PIN_AF27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[6]	=>  Location: PIN_M4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[5]	=>  Location: PIN_AH12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[4]	=>  Location: PIN_AF22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[3]	=>  Location: PIN_W1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[2]	=>  Location: PIN_AA13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[1]	=>  Location: PIN_F22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B[0]	=>  Location: PIN_M7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[7]	=>  Location: PIN_AB18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[6]	=>  Location: PIN_D4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[5]	=>  Location: PIN_AA17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[4]	=>  Location: PIN_AG21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[3]	=>  Location: PIN_AD21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[2]	=>  Location: PIN_AF9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[1]	=>  Location: PIN_AC21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[0]	=>  Location: PIN_AH8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[15]	=>  Location: PIN_AA7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- C[4]	=>  Location: PIN_AA6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[3]	=>  Location: PIN_Y10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[4]	=>  Location: PIN_AH3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[2]	=>  Location: PIN_AD5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[1]	=>  Location: PIN_AF5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[0]	=>  Location: PIN_AB8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[5]	=>  Location: PIN_AE4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[6]	=>  Location: PIN_AE6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[7]	=>  Location: PIN_AF3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[8]	=>  Location: PIN_AG3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[9]	=>  Location: PIN_AD8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[10]	=>  Location: PIN_AE5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[11]	=>  Location: PIN_Y6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[12]	=>  Location: PIN_W7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[13]	=>  Location: PIN_Y5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A[14]	=>  Location: PIN_W3,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF StudentDesign IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_DVO : std_logic;
SIGNAL ww_RESULT_A : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_C : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_A : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_RESULT_B : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_STATUS : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_CLK : std_logic;
SIGNAL ww_RESET_N : std_logic;
SIGNAL ww_DVI : std_logic;
SIGNAL ww_B : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_D : std_logic_vector(7 DOWNTO 0);
SIGNAL \C[7]~input_o\ : std_logic;
SIGNAL \C[6]~input_o\ : std_logic;
SIGNAL \C[5]~input_o\ : std_logic;
SIGNAL \C[3]~input_o\ : std_logic;
SIGNAL \C[2]~input_o\ : std_logic;
SIGNAL \C[1]~input_o\ : std_logic;
SIGNAL \C[0]~input_o\ : std_logic;
SIGNAL \CLK~input_o\ : std_logic;
SIGNAL \RESET_N~input_o\ : std_logic;
SIGNAL \DVI~input_o\ : std_logic;
SIGNAL \B[15]~input_o\ : std_logic;
SIGNAL \B[14]~input_o\ : std_logic;
SIGNAL \B[13]~input_o\ : std_logic;
SIGNAL \B[12]~input_o\ : std_logic;
SIGNAL \B[11]~input_o\ : std_logic;
SIGNAL \B[10]~input_o\ : std_logic;
SIGNAL \B[9]~input_o\ : std_logic;
SIGNAL \B[8]~input_o\ : std_logic;
SIGNAL \B[7]~input_o\ : std_logic;
SIGNAL \B[6]~input_o\ : std_logic;
SIGNAL \B[5]~input_o\ : std_logic;
SIGNAL \B[4]~input_o\ : std_logic;
SIGNAL \B[3]~input_o\ : std_logic;
SIGNAL \B[2]~input_o\ : std_logic;
SIGNAL \B[1]~input_o\ : std_logic;
SIGNAL \B[0]~input_o\ : std_logic;
SIGNAL \D[7]~input_o\ : std_logic;
SIGNAL \D[6]~input_o\ : std_logic;
SIGNAL \D[5]~input_o\ : std_logic;
SIGNAL \D[4]~input_o\ : std_logic;
SIGNAL \D[3]~input_o\ : std_logic;
SIGNAL \D[2]~input_o\ : std_logic;
SIGNAL \D[1]~input_o\ : std_logic;
SIGNAL \D[0]~input_o\ : std_logic;
SIGNAL \DVO~output_o\ : std_logic;
SIGNAL \RESULT_A[15]~output_o\ : std_logic;
SIGNAL \RESULT_A[14]~output_o\ : std_logic;
SIGNAL \RESULT_A[13]~output_o\ : std_logic;
SIGNAL \RESULT_A[12]~output_o\ : std_logic;
SIGNAL \RESULT_A[11]~output_o\ : std_logic;
SIGNAL \RESULT_A[10]~output_o\ : std_logic;
SIGNAL \RESULT_A[9]~output_o\ : std_logic;
SIGNAL \RESULT_A[8]~output_o\ : std_logic;
SIGNAL \RESULT_A[7]~output_o\ : std_logic;
SIGNAL \RESULT_A[6]~output_o\ : std_logic;
SIGNAL \RESULT_A[5]~output_o\ : std_logic;
SIGNAL \RESULT_A[4]~output_o\ : std_logic;
SIGNAL \RESULT_A[3]~output_o\ : std_logic;
SIGNAL \RESULT_A[2]~output_o\ : std_logic;
SIGNAL \RESULT_A[1]~output_o\ : std_logic;
SIGNAL \RESULT_A[0]~output_o\ : std_logic;
SIGNAL \RESULT_B[15]~output_o\ : std_logic;
SIGNAL \RESULT_B[14]~output_o\ : std_logic;
SIGNAL \RESULT_B[13]~output_o\ : std_logic;
SIGNAL \RESULT_B[12]~output_o\ : std_logic;
SIGNAL \RESULT_B[11]~output_o\ : std_logic;
SIGNAL \RESULT_B[10]~output_o\ : std_logic;
SIGNAL \RESULT_B[9]~output_o\ : std_logic;
SIGNAL \RESULT_B[8]~output_o\ : std_logic;
SIGNAL \RESULT_B[7]~output_o\ : std_logic;
SIGNAL \RESULT_B[6]~output_o\ : std_logic;
SIGNAL \RESULT_B[5]~output_o\ : std_logic;
SIGNAL \RESULT_B[4]~output_o\ : std_logic;
SIGNAL \RESULT_B[3]~output_o\ : std_logic;
SIGNAL \RESULT_B[2]~output_o\ : std_logic;
SIGNAL \RESULT_B[1]~output_o\ : std_logic;
SIGNAL \RESULT_B[0]~output_o\ : std_logic;
SIGNAL \STATUS[7]~output_o\ : std_logic;
SIGNAL \STATUS[6]~output_o\ : std_logic;
SIGNAL \STATUS[5]~output_o\ : std_logic;
SIGNAL \STATUS[4]~output_o\ : std_logic;
SIGNAL \STATUS[3]~output_o\ : std_logic;
SIGNAL \STATUS[2]~output_o\ : std_logic;
SIGNAL \STATUS[1]~output_o\ : std_logic;
SIGNAL \STATUS[0]~output_o\ : std_logic;
SIGNAL \A[15]~input_o\ : std_logic;
SIGNAL \C[4]~input_o\ : std_logic;
SIGNAL \A[14]~input_o\ : std_logic;
SIGNAL \A[13]~input_o\ : std_logic;
SIGNAL \inst|inst7~combout\ : std_logic;
SIGNAL \A[12]~input_o\ : std_logic;
SIGNAL \A[11]~input_o\ : std_logic;
SIGNAL \A[10]~input_o\ : std_logic;
SIGNAL \A[6]~input_o\ : std_logic;
SIGNAL \A[5]~input_o\ : std_logic;
SIGNAL \A[1]~input_o\ : std_logic;
SIGNAL \A[2]~input_o\ : std_logic;
SIGNAL \A[0]~input_o\ : std_logic;
SIGNAL \inst|inst3|inst9~combout\ : std_logic;
SIGNAL \A[3]~input_o\ : std_logic;
SIGNAL \A[4]~input_o\ : std_logic;
SIGNAL \inst|inst2|inst11~combout\ : std_logic;
SIGNAL \inst|inst2|inst9~combout\ : std_logic;
SIGNAL \A[8]~input_o\ : std_logic;
SIGNAL \A[7]~input_o\ : std_logic;
SIGNAL \inst|inst1|inst11~combout\ : std_logic;
SIGNAL \A[9]~input_o\ : std_logic;
SIGNAL \inst|inst1|inst9~combout\ : std_logic;
SIGNAL \inst|inst|inst11~combout\ : std_logic;
SIGNAL \inst|inst|inst9~combout\ : std_logic;
SIGNAL \inst|inst|inst7~combout\ : std_logic;
SIGNAL \inst|inst|inst6~combout\ : std_logic;
SIGNAL \inst|inst|inst5~combout\ : std_logic;
SIGNAL \inst|inst|inst4~combout\ : std_logic;
SIGNAL \inst|inst1|inst7~combout\ : std_logic;
SIGNAL \inst|inst1|inst6~combout\ : std_logic;
SIGNAL \inst|inst1|inst5~combout\ : std_logic;
SIGNAL \inst|inst1|inst4~combout\ : std_logic;
SIGNAL \inst|inst2|inst7~combout\ : std_logic;
SIGNAL \inst|inst2|inst6~combout\ : std_logic;
SIGNAL \inst|inst2|inst5~combout\ : std_logic;
SIGNAL \inst|inst2|inst4~combout\ : std_logic;
SIGNAL \inst|inst3|inst7~combout\ : std_logic;
SIGNAL \inst|inst3|inst6~combout\ : std_logic;
SIGNAL \inst|inst3|inst5~0_combout\ : std_logic;
SIGNAL \inst|inst3|ALT_INV_inst7~combout\ : std_logic;
SIGNAL \inst|inst3|ALT_INV_inst6~combout\ : std_logic;

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

DVO <= ww_DVO;
RESULT_A <= ww_RESULT_A;
ww_C <= C;
ww_A <= A;
RESULT_B <= ww_RESULT_B;
STATUS <= ww_STATUS;
ww_CLK <= CLK;
ww_RESET_N <= RESET_N;
ww_DVI <= DVI;
ww_B <= B;
ww_D <= D;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\inst|inst3|ALT_INV_inst7~combout\ <= NOT \inst|inst3|inst7~combout\;
\inst|inst3|ALT_INV_inst6~combout\ <= NOT \inst|inst3|inst6~combout\;
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: IOOBUF_X60_Y0_N9
\DVO~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \DVO~output_o\);

-- Location: IOOBUF_X0_Y14_N2
\RESULT_A[15]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|inst|inst7~combout\,
	devoe => ww_devoe,
	o => \RESULT_A[15]~output_o\);

-- Location: IOOBUF_X0_Y14_N9
\RESULT_A[14]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|inst|inst6~combout\,
	devoe => ww_devoe,
	o => \RESULT_A[14]~output_o\);

-- Location: IOOBUF_X0_Y11_N16
\RESULT_A[13]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|inst|inst5~combout\,
	devoe => ww_devoe,
	o => \RESULT_A[13]~output_o\);

-- Location: IOOBUF_X0_Y10_N23
\RESULT_A[12]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|inst|inst4~combout\,
	devoe => ww_devoe,
	o => \RESULT_A[12]~output_o\);

-- Location: IOOBUF_X0_Y11_N23
\RESULT_A[11]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|inst1|inst7~combout\,
	devoe => ww_devoe,
	o => \RESULT_A[11]~output_o\);

-- Location: IOOBUF_X9_Y0_N16
\RESULT_A[10]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|inst1|inst6~combout\,
	devoe => ww_devoe,
	o => \RESULT_A[10]~output_o\);

-- Location: IOOBUF_X3_Y0_N2
\RESULT_A[9]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|inst1|inst5~combout\,
	devoe => ww_devoe,
	o => \RESULT_A[9]~output_o\);

-- Location: IOOBUF_X0_Y4_N23
\RESULT_A[8]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|inst1|inst4~combout\,
	devoe => ww_devoe,
	o => \RESULT_A[8]~output_o\);

-- Location: IOOBUF_X1_Y0_N2
\RESULT_A[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|inst2|inst7~combout\,
	devoe => ww_devoe,
	o => \RESULT_A[7]~output_o\);

-- Location: IOOBUF_X1_Y0_N9
\RESULT_A[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|inst2|inst6~combout\,
	devoe => ww_devoe,
	o => \RESULT_A[6]~output_o\);

-- Location: IOOBUF_X0_Y5_N16
\RESULT_A[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|inst2|inst5~combout\,
	devoe => ww_devoe,
	o => \RESULT_A[5]~output_o\);

-- Location: IOOBUF_X0_Y4_N9
\RESULT_A[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|inst2|inst4~combout\,
	devoe => ww_devoe,
	o => \RESULT_A[4]~output_o\);

-- Location: IOOBUF_X7_Y0_N16
\RESULT_A[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|inst3|ALT_INV_inst7~combout\,
	devoe => ww_devoe,
	o => \RESULT_A[3]~output_o\);

-- Location: IOOBUF_X0_Y4_N2
\RESULT_A[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|inst3|ALT_INV_inst6~combout\,
	devoe => ww_devoe,
	o => \RESULT_A[2]~output_o\);

-- Location: IOOBUF_X9_Y0_N23
\RESULT_A[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|inst3|inst5~0_combout\,
	devoe => ww_devoe,
	o => \RESULT_A[1]~output_o\);

-- Location: IOOBUF_X9_Y0_N2
\RESULT_A[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \A[0]~input_o\,
	devoe => ww_devoe,
	o => \RESULT_A[0]~output_o\);

-- Location: IOOBUF_X27_Y0_N9
\RESULT_B[15]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \RESULT_B[15]~output_o\);

-- Location: IOOBUF_X42_Y73_N2
\RESULT_B[14]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \RESULT_B[14]~output_o\);

-- Location: IOOBUF_X0_Y16_N23
\RESULT_B[13]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \RESULT_B[13]~output_o\);

-- Location: IOOBUF_X23_Y0_N16
\RESULT_B[12]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \RESULT_B[12]~output_o\);

-- Location: IOOBUF_X81_Y73_N16
\RESULT_B[11]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \RESULT_B[11]~output_o\);

-- Location: IOOBUF_X115_Y27_N2
\RESULT_B[10]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \RESULT_B[10]~output_o\);

-- Location: IOOBUF_X0_Y26_N16
\RESULT_B[9]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \RESULT_B[9]~output_o\);

-- Location: IOOBUF_X111_Y73_N2
\RESULT_B[8]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \RESULT_B[8]~output_o\);

-- Location: IOOBUF_X0_Y34_N16
\RESULT_B[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \RESULT_B[7]~output_o\);

-- Location: IOOBUF_X5_Y73_N23
\RESULT_B[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \RESULT_B[6]~output_o\);

-- Location: IOOBUF_X74_Y0_N16
\RESULT_B[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \RESULT_B[5]~output_o\);

-- Location: IOOBUF_X42_Y73_N9
\RESULT_B[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \RESULT_B[4]~output_o\);

-- Location: IOOBUF_X0_Y48_N9
\RESULT_B[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \RESULT_B[3]~output_o\);

-- Location: IOOBUF_X115_Y7_N16
\RESULT_B[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \RESULT_B[2]~output_o\);

-- Location: IOOBUF_X40_Y73_N9
\RESULT_B[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \RESULT_B[1]~output_o\);

-- Location: IOOBUF_X16_Y73_N9
\RESULT_B[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \RESULT_B[0]~output_o\);

-- Location: IOOBUF_X105_Y0_N23
\STATUS[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \STATUS[7]~output_o\);

-- Location: IOOBUF_X81_Y73_N23
\STATUS[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \STATUS[6]~output_o\);

-- Location: IOOBUF_X115_Y58_N16
\STATUS[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \STATUS[5]~output_o\);

-- Location: IOOBUF_X38_Y73_N16
\STATUS[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \STATUS[4]~output_o\);

-- Location: IOOBUF_X0_Y31_N16
\STATUS[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \STATUS[3]~output_o\);

-- Location: IOOBUF_X0_Y43_N16
\STATUS[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \STATUS[2]~output_o\);

-- Location: IOOBUF_X85_Y0_N23
\STATUS[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \STATUS[1]~output_o\);

-- Location: IOOBUF_X47_Y73_N2
\STATUS[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \STATUS[0]~output_o\);

-- Location: IOIBUF_X0_Y9_N15
\A[15]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(15),
	o => \A[15]~input_o\);

-- Location: IOIBUF_X0_Y10_N15
\C[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_C(4),
	o => \C[4]~input_o\);

-- Location: IOIBUF_X0_Y13_N1
\A[14]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(14),
	o => \A[14]~input_o\);

-- Location: IOIBUF_X0_Y12_N15
\A[13]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(13),
	o => \A[13]~input_o\);

-- Location: LCCOMB_X5_Y4_N0
\inst|inst7\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|inst7~combout\ = (\A[15]~input_o\ & \C[4]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A[15]~input_o\,
	datac => \C[4]~input_o\,
	combout => \inst|inst7~combout\);

-- Location: IOIBUF_X0_Y12_N22
\A[12]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(12),
	o => \A[12]~input_o\);

-- Location: IOIBUF_X0_Y13_N8
\A[11]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(11),
	o => \A[11]~input_o\);

-- Location: IOIBUF_X5_Y0_N22
\A[10]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(10),
	o => \A[10]~input_o\);

-- Location: IOIBUF_X1_Y0_N15
\A[6]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(6),
	o => \A[6]~input_o\);

-- Location: IOIBUF_X3_Y0_N22
\A[5]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(5),
	o => \A[5]~input_o\);

-- Location: IOIBUF_X5_Y0_N15
\A[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(1),
	o => \A[1]~input_o\);

-- Location: IOIBUF_X1_Y0_N22
\A[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(2),
	o => \A[2]~input_o\);

-- Location: IOIBUF_X11_Y0_N1
\A[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(0),
	o => \A[0]~input_o\);

-- Location: LCCOMB_X5_Y4_N10
\inst|inst3|inst9\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|inst3|inst9~combout\ = ((\A[1]~input_o\) # ((\A[2]~input_o\) # (\A[0]~input_o\))) # (!\inst|inst7~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst7~combout\,
	datab => \A[1]~input_o\,
	datac => \A[2]~input_o\,
	datad => \A[0]~input_o\,
	combout => \inst|inst3|inst9~combout\);

-- Location: IOIBUF_X7_Y0_N8
\A[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(3),
	o => \A[3]~input_o\);

-- Location: IOIBUF_X5_Y0_N8
\A[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(4),
	o => \A[4]~input_o\);

-- Location: LCCOMB_X5_Y4_N28
\inst|inst2|inst11\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|inst2|inst11~combout\ = (!\inst|inst3|inst9~combout\ & ((\inst|inst7~combout\ & (!\A[3]~input_o\ & !\A[4]~input_o\)) # (!\inst|inst7~combout\ & (\A[3]~input_o\ & \A[4]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst3|inst9~combout\,
	datab => \inst|inst7~combout\,
	datac => \A[3]~input_o\,
	datad => \A[4]~input_o\,
	combout => \inst|inst2|inst11~combout\);

-- Location: LCCOMB_X5_Y4_N14
\inst|inst2|inst9\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|inst2|inst9~combout\ = (\inst|inst2|inst11~combout\ & ((\A[6]~input_o\ & (\A[5]~input_o\ & !\inst|inst7~combout\)) # (!\A[6]~input_o\ & (!\A[5]~input_o\ & \inst|inst7~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A[6]~input_o\,
	datab => \A[5]~input_o\,
	datac => \inst|inst7~combout\,
	datad => \inst|inst2|inst11~combout\,
	combout => \inst|inst2|inst9~combout\);

-- Location: IOIBUF_X3_Y0_N15
\A[8]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(8),
	o => \A[8]~input_o\);

-- Location: IOIBUF_X7_Y0_N22
\A[7]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(7),
	o => \A[7]~input_o\);

-- Location: LCCOMB_X5_Y4_N16
\inst|inst1|inst11\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|inst1|inst11~combout\ = (\inst|inst2|inst9~combout\ & ((\inst|inst7~combout\ & (!\A[8]~input_o\ & !\A[7]~input_o\)) # (!\inst|inst7~combout\ & (\A[8]~input_o\ & \A[7]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst7~combout\,
	datab => \inst|inst2|inst9~combout\,
	datac => \A[8]~input_o\,
	datad => \A[7]~input_o\,
	combout => \inst|inst1|inst11~combout\);

-- Location: IOIBUF_X9_Y0_N8
\A[9]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A(9),
	o => \A[9]~input_o\);

-- Location: LCCOMB_X5_Y4_N18
\inst|inst1|inst9\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|inst1|inst9~combout\ = (\inst|inst1|inst11~combout\ & ((\A[10]~input_o\ & (!\inst|inst7~combout\ & \A[9]~input_o\)) # (!\A[10]~input_o\ & (\inst|inst7~combout\ & !\A[9]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A[10]~input_o\,
	datab => \inst|inst1|inst11~combout\,
	datac => \inst|inst7~combout\,
	datad => \A[9]~input_o\,
	combout => \inst|inst1|inst9~combout\);

-- Location: LCCOMB_X1_Y12_N24
\inst|inst|inst11\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|inst|inst11~combout\ = (\inst|inst1|inst9~combout\ & ((\A[12]~input_o\ & (\A[11]~input_o\ & !\inst|inst7~combout\)) # (!\A[12]~input_o\ & (!\A[11]~input_o\ & \inst|inst7~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A[12]~input_o\,
	datab => \A[11]~input_o\,
	datac => \inst|inst7~combout\,
	datad => \inst|inst1|inst9~combout\,
	combout => \inst|inst|inst11~combout\);

-- Location: LCCOMB_X1_Y12_N10
\inst|inst|inst9\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|inst|inst9~combout\ = (\inst|inst|inst11~combout\ & ((\A[14]~input_o\ & (\A[13]~input_o\ & !\inst|inst7~combout\)) # (!\A[14]~input_o\ & (!\A[13]~input_o\ & \inst|inst7~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A[14]~input_o\,
	datab => \A[13]~input_o\,
	datac => \inst|inst7~combout\,
	datad => \inst|inst|inst11~combout\,
	combout => \inst|inst|inst9~combout\);

-- Location: LCCOMB_X1_Y12_N0
\inst|inst|inst7\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|inst|inst7~combout\ = \inst|inst|inst9~combout\ $ (((\A[15]~input_o\ & !\C[4]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A[15]~input_o\,
	datac => \C[4]~input_o\,
	datad => \inst|inst|inst9~combout\,
	combout => \inst|inst|inst7~combout\);

-- Location: LCCOMB_X1_Y12_N28
\inst|inst|inst6\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|inst|inst6~combout\ = \A[14]~input_o\ $ (((\inst|inst|inst11~combout\ & (\A[13]~input_o\)) # (!\inst|inst|inst11~combout\ & ((\inst|inst7~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A[14]~input_o\,
	datab => \A[13]~input_o\,
	datac => \inst|inst7~combout\,
	datad => \inst|inst|inst11~combout\,
	combout => \inst|inst|inst6~combout\);

-- Location: LCCOMB_X1_Y12_N26
\inst|inst|inst5\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|inst|inst5~combout\ = \A[13]~input_o\ $ (\inst|inst|inst11~combout\ $ (((\A[15]~input_o\ & \C[4]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001101101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A[15]~input_o\,
	datab => \A[13]~input_o\,
	datac => \C[4]~input_o\,
	datad => \inst|inst|inst11~combout\,
	combout => \inst|inst|inst5~combout\);

-- Location: LCCOMB_X1_Y12_N30
\inst|inst|inst4\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|inst|inst4~combout\ = \A[12]~input_o\ $ (((\inst|inst1|inst9~combout\ & (\A[11]~input_o\)) # (!\inst|inst1|inst9~combout\ & ((\inst|inst7~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A[12]~input_o\,
	datab => \A[11]~input_o\,
	datac => \inst|inst7~combout\,
	datad => \inst|inst1|inst9~combout\,
	combout => \inst|inst|inst4~combout\);

-- Location: LCCOMB_X1_Y12_N12
\inst|inst1|inst7\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|inst1|inst7~combout\ = \A[11]~input_o\ $ (\inst|inst1|inst9~combout\ $ (((\A[15]~input_o\ & \C[4]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001101101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A[15]~input_o\,
	datab => \A[11]~input_o\,
	datac => \C[4]~input_o\,
	datad => \inst|inst1|inst9~combout\,
	combout => \inst|inst1|inst7~combout\);

-- Location: LCCOMB_X5_Y4_N12
\inst|inst1|inst6\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|inst1|inst6~combout\ = \A[10]~input_o\ $ (((\inst|inst1|inst11~combout\ & ((\A[9]~input_o\))) # (!\inst|inst1|inst11~combout\ & (\inst|inst7~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101011010011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A[10]~input_o\,
	datab => \inst|inst1|inst11~combout\,
	datac => \inst|inst7~combout\,
	datad => \A[9]~input_o\,
	combout => \inst|inst1|inst6~combout\);

-- Location: LCCOMB_X5_Y4_N24
\inst|inst1|inst5\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|inst1|inst5~combout\ = \inst|inst1|inst11~combout\ $ (\A[9]~input_o\ $ (((\A[15]~input_o\ & \C[4]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001101101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A[15]~input_o\,
	datab => \inst|inst1|inst11~combout\,
	datac => \C[4]~input_o\,
	datad => \A[9]~input_o\,
	combout => \inst|inst1|inst5~combout\);

-- Location: LCCOMB_X5_Y4_N22
\inst|inst1|inst4\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|inst1|inst4~combout\ = \A[8]~input_o\ $ (((\inst|inst2|inst9~combout\ & ((\A[7]~input_o\))) # (!\inst|inst2|inst9~combout\ & (\inst|inst7~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111011010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst7~combout\,
	datab => \inst|inst2|inst9~combout\,
	datac => \A[8]~input_o\,
	datad => \A[7]~input_o\,
	combout => \inst|inst1|inst4~combout\);

-- Location: LCCOMB_X5_Y4_N2
\inst|inst2|inst7\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|inst2|inst7~combout\ = \inst|inst2|inst9~combout\ $ (\A[7]~input_o\ $ (((\A[15]~input_o\ & \C[4]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001101101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A[15]~input_o\,
	datab => \inst|inst2|inst9~combout\,
	datac => \C[4]~input_o\,
	datad => \A[7]~input_o\,
	combout => \inst|inst2|inst7~combout\);

-- Location: LCCOMB_X5_Y4_N8
\inst|inst2|inst6\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|inst2|inst6~combout\ = \A[6]~input_o\ $ (((\inst|inst2|inst11~combout\ & (\A[5]~input_o\)) # (!\inst|inst2|inst11~combout\ & ((\inst|inst7~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A[6]~input_o\,
	datab => \A[5]~input_o\,
	datac => \inst|inst7~combout\,
	datad => \inst|inst2|inst11~combout\,
	combout => \inst|inst2|inst6~combout\);

-- Location: LCCOMB_X5_Y4_N20
\inst|inst2|inst5\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|inst2|inst5~combout\ = \A[5]~input_o\ $ (\inst|inst2|inst11~combout\ $ (((\A[15]~input_o\ & \C[4]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001101101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A[15]~input_o\,
	datab => \A[5]~input_o\,
	datac => \C[4]~input_o\,
	datad => \inst|inst2|inst11~combout\,
	combout => \inst|inst2|inst5~combout\);

-- Location: LCCOMB_X5_Y4_N26
\inst|inst2|inst4\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|inst2|inst4~combout\ = \A[4]~input_o\ $ (((\inst|inst3|inst9~combout\ & (\inst|inst7~combout\)) # (!\inst|inst3|inst9~combout\ & ((\A[3]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010011111011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst3|inst9~combout\,
	datab => \inst|inst7~combout\,
	datac => \A[3]~input_o\,
	datad => \A[4]~input_o\,
	combout => \inst|inst2|inst4~combout\);

-- Location: LCCOMB_X5_Y4_N30
\inst|inst3|inst7\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|inst3|inst7~combout\ = \A[3]~input_o\ $ (\inst|inst3|inst9~combout\ $ (((\A[15]~input_o\ & \C[4]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001101101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A[15]~input_o\,
	datab => \A[3]~input_o\,
	datac => \C[4]~input_o\,
	datad => \inst|inst3|inst9~combout\,
	combout => \inst|inst3|inst7~combout\);

-- Location: LCCOMB_X5_Y4_N4
\inst|inst3|inst6\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|inst3|inst6~combout\ = \A[2]~input_o\ $ ((((!\A[1]~input_o\ & !\A[0]~input_o\)) # (!\inst|inst7~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010110000111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst7~combout\,
	datab => \A[1]~input_o\,
	datac => \A[2]~input_o\,
	datad => \A[0]~input_o\,
	combout => \inst|inst3|inst6~combout\);

-- Location: LCCOMB_X5_Y4_N6
\inst|inst3|inst5~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|inst3|inst5~0_combout\ = \A[1]~input_o\ $ (((\A[15]~input_o\ & (\C[4]~input_o\ & \A[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A[15]~input_o\,
	datab => \A[1]~input_o\,
	datac => \C[4]~input_o\,
	datad => \A[0]~input_o\,
	combout => \inst|inst3|inst5~0_combout\);

-- Location: IOIBUF_X115_Y37_N8
\C[7]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_C(7),
	o => \C[7]~input_o\);

-- Location: IOIBUF_X115_Y37_N1
\C[6]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_C(6),
	o => \C[6]~input_o\);

-- Location: IOIBUF_X54_Y73_N8
\C[5]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_C(5),
	o => \C[5]~input_o\);

-- Location: IOIBUF_X23_Y0_N22
\C[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_C(3),
	o => \C[3]~input_o\);

-- Location: IOIBUF_X115_Y48_N8
\C[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_C(2),
	o => \C[2]~input_o\);

-- Location: IOIBUF_X0_Y57_N15
\C[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_C(1),
	o => \C[1]~input_o\);

-- Location: IOIBUF_X85_Y0_N1
\C[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_C(0),
	o => \C[0]~input_o\);

-- Location: IOIBUF_X13_Y73_N1
\CLK~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLK,
	o => \CLK~input_o\);

-- Location: IOIBUF_X31_Y0_N1
\RESET_N~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_RESET_N,
	o => \RESET_N~input_o\);

-- Location: IOIBUF_X5_Y73_N1
\DVI~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_DVI,
	o => \DVI~input_o\);

-- Location: IOIBUF_X96_Y0_N15
\B[15]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(15),
	o => \B[15]~input_o\);

-- Location: IOIBUF_X65_Y0_N22
\B[14]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(14),
	o => \B[14]~input_o\);

-- Location: IOIBUF_X23_Y73_N15
\B[13]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(13),
	o => \B[13]~input_o\);

-- Location: IOIBUF_X100_Y0_N1
\B[12]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(12),
	o => \B[12]~input_o\);

-- Location: IOIBUF_X9_Y73_N8
\B[11]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(11),
	o => \B[11]~input_o\);

-- Location: IOIBUF_X18_Y0_N22
\B[10]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(10),
	o => \B[10]~input_o\);

-- Location: IOIBUF_X83_Y0_N1
\B[9]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(9),
	o => \B[9]~input_o\);

-- Location: IOIBUF_X1_Y73_N22
\B[8]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(8),
	o => \B[8]~input_o\);

-- Location: IOIBUF_X115_Y8_N22
\B[7]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(7),
	o => \B[7]~input_o\);

-- Location: IOIBUF_X0_Y52_N22
\B[6]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(6),
	o => \B[6]~input_o\);

-- Location: IOIBUF_X54_Y0_N1
\B[5]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(5),
	o => \B[5]~input_o\);

-- Location: IOIBUF_X96_Y0_N1
\B[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(4),
	o => \B[4]~input_o\);

-- Location: IOIBUF_X0_Y25_N15
\B[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(3),
	o => \B[3]~input_o\);

-- Location: IOIBUF_X52_Y0_N1
\B[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(2),
	o => \B[2]~input_o\);

-- Location: IOIBUF_X107_Y73_N22
\B[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(1),
	o => \B[1]~input_o\);

-- Location: IOIBUF_X0_Y45_N22
\B[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B(0),
	o => \B[0]~input_o\);

-- Location: IOIBUF_X98_Y0_N15
\D[7]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(7),
	o => \D[7]~input_o\);

-- Location: IOIBUF_X1_Y73_N1
\D[6]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(6),
	o => \D[6]~input_o\);

-- Location: IOIBUF_X89_Y0_N22
\D[5]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(5),
	o => \D[5]~input_o\);

-- Location: IOIBUF_X74_Y0_N8
\D[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(4),
	o => \D[4]~input_o\);

-- Location: IOIBUF_X102_Y0_N15
\D[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(3),
	o => \D[3]~input_o\);

-- Location: IOIBUF_X20_Y0_N1
\D[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(2),
	o => \D[2]~input_o\);

-- Location: IOIBUF_X102_Y0_N22
\D[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(1),
	o => \D[1]~input_o\);

-- Location: IOIBUF_X20_Y0_N22
\D[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(0),
	o => \D[0]~input_o\);

ww_DVO <= \DVO~output_o\;

ww_RESULT_A(15) <= \RESULT_A[15]~output_o\;

ww_RESULT_A(14) <= \RESULT_A[14]~output_o\;

ww_RESULT_A(13) <= \RESULT_A[13]~output_o\;

ww_RESULT_A(12) <= \RESULT_A[12]~output_o\;

ww_RESULT_A(11) <= \RESULT_A[11]~output_o\;

ww_RESULT_A(10) <= \RESULT_A[10]~output_o\;

ww_RESULT_A(9) <= \RESULT_A[9]~output_o\;

ww_RESULT_A(8) <= \RESULT_A[8]~output_o\;

ww_RESULT_A(7) <= \RESULT_A[7]~output_o\;

ww_RESULT_A(6) <= \RESULT_A[6]~output_o\;

ww_RESULT_A(5) <= \RESULT_A[5]~output_o\;

ww_RESULT_A(4) <= \RESULT_A[4]~output_o\;

ww_RESULT_A(3) <= \RESULT_A[3]~output_o\;

ww_RESULT_A(2) <= \RESULT_A[2]~output_o\;

ww_RESULT_A(1) <= \RESULT_A[1]~output_o\;

ww_RESULT_A(0) <= \RESULT_A[0]~output_o\;

ww_RESULT_B(15) <= \RESULT_B[15]~output_o\;

ww_RESULT_B(14) <= \RESULT_B[14]~output_o\;

ww_RESULT_B(13) <= \RESULT_B[13]~output_o\;

ww_RESULT_B(12) <= \RESULT_B[12]~output_o\;

ww_RESULT_B(11) <= \RESULT_B[11]~output_o\;

ww_RESULT_B(10) <= \RESULT_B[10]~output_o\;

ww_RESULT_B(9) <= \RESULT_B[9]~output_o\;

ww_RESULT_B(8) <= \RESULT_B[8]~output_o\;

ww_RESULT_B(7) <= \RESULT_B[7]~output_o\;

ww_RESULT_B(6) <= \RESULT_B[6]~output_o\;

ww_RESULT_B(5) <= \RESULT_B[5]~output_o\;

ww_RESULT_B(4) <= \RESULT_B[4]~output_o\;

ww_RESULT_B(3) <= \RESULT_B[3]~output_o\;

ww_RESULT_B(2) <= \RESULT_B[2]~output_o\;

ww_RESULT_B(1) <= \RESULT_B[1]~output_o\;

ww_RESULT_B(0) <= \RESULT_B[0]~output_o\;

ww_STATUS(7) <= \STATUS[7]~output_o\;

ww_STATUS(6) <= \STATUS[6]~output_o\;

ww_STATUS(5) <= \STATUS[5]~output_o\;

ww_STATUS(4) <= \STATUS[4]~output_o\;

ww_STATUS(3) <= \STATUS[3]~output_o\;

ww_STATUS(2) <= \STATUS[2]~output_o\;

ww_STATUS(1) <= \STATUS[1]~output_o\;

ww_STATUS(0) <= \STATUS[0]~output_o\;
END structure;


