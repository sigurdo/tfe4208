## Styresignal til LDI

- Styreenheten må sette PS=0, PL=1, SR=X, RW=1, IOW=X, MD1=X, MD2=1

## Avlusing


Antar at BNZ står for branch if not zero, slik at beskrivelsen PC=Imm hvis Z != 0 er feil og skulle vært PC=Imm hvis Z = 0.

Vi forventer at programmet først setter R1 og R2 til 5 og 1, deretter trekker de fra hverandre og lagrer 4 på R1. BNZ på instruksjon 3 vil hoppe til instruksjon 2 fordi resultatet var ulikt 0. Etter å ha subtrahert 5 ganer vil instruksjon 3 ikke branche lenger, siden Så laster den inn 11 på R3 og hopper til starten og gjentar dette i en evig loop.


Hva programmet var tiltenkt å gjøre er vanskelig å si. Det kan hende det var meningen å branche på instruksjon 3 hvis resultatet var likt 0 og ikke ulikt 0, men dette er dessverre ikke en støtta instruksjon i instruksjonssettet. Og selv da vil jo programmet gå i evig loop uten å gjøre noe IO, så det er vanskelig å si hva som er riktig.


## Elementær I/O

| Adresse | Kode      | Opkode | FU-funksjon | Immediate | DR  | SB  | SA  |
| ------- | --------- | ------ | ----------- | --------- | --- | --- | --- |
| 0       | LDI R4, 1 | 0x3    | x           | 0x1       | 0x4 | x   | x   |
| 1       | LDI R5, 2 | 0x3    | x           | 0x2       | 0x5 | x   | x   |
| 2       | IN R1, R4 | 0x5    | x           | x         | 0x1 | x   | 0x4 |
| 3       | IN R2, R5 | 0x5    | x           | x         | 0x2 | x   | 0x5 |
| 4       | JMP 2     | 0x4    | x           | 0x2       | x   | x   | x   |

- Dette programmet leser inn verdien som står på KEY1 og KEY2 til henholdsvis register R1 og R2 i en evig loop.

```vhdl
mem_storage(0) <= X"30001400";
mem_storage(1) <= X"30002500";
mem_storage(2) <= X"50000104";
mem_storage(3) <= X"50000205";
mem_storage(4) <= X"40002000";
```

## Volumstyring


```arm
0x00 LDI R1, 60 (current volume)
0x01 LDI R4, 1 (Port 1 is KEY1)
0x02 IN  R2, R4 (Read port R4 and store the result in R2. 0 means that the key has been pressed.)
0x03 LDI R4, 1
0x04 SUB R2, R4, R2 ( R2 = R2 - R4)
0x05 BNZ 30 (if the subtraction was not zero then the key was pressed). Jump to 0x30 (48)to handle it.)
0x06 LDI R4, 2 (Port 2 is KEY2)
0x07 IN  R3, R4 (Read port R4 and store the result in R3 0 means that the key has been pressed.)
0x08 LDI R4, 1
0x09 SUB R3, R4, R3 ( R3 = R3 - R4)
0x0A BNZ 50 (if the subtraction was not zero then the key was pressed). Jump to 0x50 (80) to handle it)
0x0B JMP 1 (Repeat until something is pressed) 

0x30 LDI R4, 1 (Port 1 is KEY1)
0x31 IN  R2, R4 (Read port R4 and store the result in R2. 0 means that the key has been pressed.)
0x32 LDI R4, 1
0x33 SUB R2, R4, R2 ( R2 = R2 - R4)
0x34 BNZ 30 (if the subtraction was not zero then the key is still pressed). Jump to 0x30 to keep waiting for it to be released.)
0x35 LDI R5, 0x7F
0x36 SUB R6, R1, R5 (Maks volum - volum)
0x37 BNZ 0x39 
0x38 JMP 0x1 
0x39 LDI R6, 0x1
0x3A ADD R1, R6, R1
0x3B JMP 0x1

0x50 LDI R4, 2 (Port 2 is KEY2)
0x51 IN  R3, R4 (Read port R4 and store the result in R3 0 means that the key is still pressed.)
0x52 LDI R4, 1
0x53 SUB R3, R4, R3 ( R3 = R3 - R4)
0x54 BNZ 0x50 (if the subtraction was not zero then the key is still pressed). Jump to 0x50 (80) to keep waiting for it to be released)
0x55 LDI R5, 0x60
0x56 SUB R6, R5, R1 ( volum- Min volum )
0x57 BNZ 0x59
0x58 JMP 0x1 
0x59 LDI R6, 0x1
0x5A SUB R1, R6, R1
0x5B JMP 0x1
```

```vhdl
mem_storage(48) <= X"30001400";
mem_storage(49) <= X"50000204";
mem_storage(50) <= X"30001400";
mem_storage(51) <= X"10800242";
mem_storage(52) <= X"20030000";
mem_storage(53) <= X"3007F500";
mem_storage(54) <= X"10800615";
mem_storage(55) <= X"20039000";
mem_storage(56) <= X"40001000";
mem_storage(57) <= X"30001600";
mem_storage(58) <= X"10700161";
mem_storage(59) <= X"40001000";

mem_storage(80) <= X"30002400";
mem_storage(81) <= X"50000304"; 
mem_storage(82) <= X"30001400";
mem_storage(83) <= X"10800343";
mem_storage(84) <= X"20050000";
mem_storage(85) <= X"30060500";
mem_storage(86) <= X"10800651";
mem_storage(87) <= X"20059000";
mem_storage(88) <= X"40001000";
mem_storage(89) <= X"30001600";
mem_storage(90) <= X"10800161";
mem_storage(91) <= X"40001000";
```
