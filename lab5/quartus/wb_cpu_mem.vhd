-------------------------------------------------------------------------------
-- Title      : memory block
-- Project    : Lab5, DigDat
-------------------------------------------------------------------------------
-- File       : mem.vhd
-- Author     : Asbj�rn Djupdal  <asbjoern@djupdal.org>
-- Company    : 
-- Last update: 2008/06/18
-- Platform   : BenERA, Virtex 1000E
-------------------------------------------------------------------------------
-- Description: Generic memory block
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2003/08/05  1.0      djupdal	Created
-- 2008/09/26  1.1      grannas Adapted to DigDat
-------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
USE work.wb_cpu_package.ALL;

-------------------------------------------------------------------------------

ENTITY wb_cpu_mem IS

  PORT (
    -- read port
    address : IN STD_LOGIC_VECTOR(MEM_ADDR_BUS - 1 DOWNTO 0);
    instruction : OUT STD_LOGIC_VECTOR(MEM_DATA_BUS - 1 DOWNTO 0);

    core_rst_n : IN STD_LOGIC;
    core_clk : IN STD_LOGIC);

END wb_cpu_mem;

-------------------------------------------------------------------------------

ARCHITECTURE mem_arch OF wb_cpu_mem IS

  -- number of words in memory
  CONSTANT MEM_SIZE : INTEGER := 2 ** MEM_ADDR_BUS;

  TYPE mem_type IS ARRAY (0 TO MEM_SIZE - 1)
  OF STD_LOGIC_VECTOR(MEM_DATA_BUS - 1 DOWNTO 0);

  SIGNAL mem_storage : mem_type := (OTHERS => (OTHERS => '0'));
BEGIN

  PROCESS (core_clk, core_rst_n)
  BEGIN

    IF core_rst_n = '0' THEN

      -- Oppgave 1
      -- mem_storage(0) <= X"300FF100";    -- LDI R1, FF
      -- mem_storage(1) <= X"40000000";    -- JMP 0

      -- Oppgave 2

      -- mem_storage(0) <= X"30005100";    -- LDI R1, 5
      -- mem_storage(1) <= X"30001200";    -- LDI R2, 1 
      -- mem_storage(2) <= X"10800121";    -- SUB R1, R2,R1
      -- mem_storage(3) <= X"20002000";    -- BNZ 2 
      -- mem_storage(4) <= X"30011300";    -- LDI R3, 11
      -- mem_storage(5) <= X"40000000";    -- JMP 0

      -- mem_storage(0) <= X"30001400";
      -- mem_storage(1) <= X"30002500";
      -- mem_storage(2) <= X"50000104";
      -- mem_storage(3) <= X"50000205";
      -- mem_storage(4) <= X"40002000";
      -- Oppgave 3

      --     -- LDI R4, 1
      --     -- LDI R5, 2
      --     -- IN  R1, R4
      --     -- IN  R2, R5
      --     -- JMP 2     

      -- Oppgave 4

      -- Initial values
      mem_storage(0) <= X"30060100"; -- LDI R1, 60 (current volume)

      --Read key 1
      mem_storage(1) <= X"30001400"; -- LDI R4, 1 (Port 1 is key 1)
      mem_storage(2) <= X"50000204"; -- IN R2, R4 (Read port R4 and store the result in R2
      --0 means that the key has been pressed.
      mem_storage(3) <= X"30001400"; -- LDI R4, 1 
      mem_storage(4) <= X"10800242"; -- Sub R2, R4, R2 ( R2 = R2 - R4)
      mem_storage(5) <= X"20030000"; -- BNZ 30 (if the substraction was not zero, then the key
      --was pressed). Jump to 0x30 (48) to handle it
      --Read key 2
      mem_storage(6) <= X"30002400"; -- LDI R4, 2 (Port 2 is key 2)
      mem_storage(7) <= X"50000304"; -- IN R3, R4 (Read port R4 and store the result in R3
      --0 means that the key has been pressed.
      mem_storage(8) <= X"30001400"; -- LDI R4, 1 
      mem_storage(9) <= X"10800343"; -- Sub R3, R4, R3 ( R3 = R3 - R4)
      mem_storage(10) <= X"20050000"; -- BNZ 50 (if the substraction was not zero, then the key
      --was pressed). Jump to 0x50 (80) to handle it
      --Jump back to top
      mem_storage(11) <= X"40001000"; -- JMP 1   (Repeat until something is pressed)

      -- Student-kode begynner her:

      -- Handle keypress 1
      -- First, wait til user lets go of the key
      -- mem_storage(48) <= 
      -- Don't increase it beyond 7F (max volume)

      -- Handle keypress 2
      -- First, wait til user lets go of the key
      -- mem_storage(80) <= 
      -- Don't increase it below 1 (min volume)

      mem_storage(48) <= X"30001400";
      mem_storage(49) <= X"50000204";
      mem_storage(50) <= X"30001400";
      mem_storage(51) <= X"10800242";
      mem_storage(52) <= X"20030000";
      mem_storage(53) <= X"3007F500";
      mem_storage(54) <= X"10800615";
      mem_storage(55) <= X"20039000";
      mem_storage(56) <= X"40001000";
      mem_storage(57) <= X"30001600";
      mem_storage(58) <= X"10700161";
      mem_storage(59) <= X"40001000";

      mem_storage(80) <= X"30002400";
      mem_storage(81) <= X"50000304";
      mem_storage(82) <= X"30001400";
      mem_storage(83) <= X"10800343";
      mem_storage(84) <= X"20050000";
      mem_storage(85) <= X"30060500";
      mem_storage(86) <= X"10800651";
      mem_storage(87) <= X"20059000";
      mem_storage(88) <= X"40001000";
      mem_storage(89) <= X"30001600";
      mem_storage(90) <= X"10800161";
      mem_storage(91) <= X"40001000";
      -- Adjust volume
      mem_storage(110) <= X"30003400"; -- LDI R4, 3 (Port 3 is volume)
      mem_storage(111) <= X"60000014"; -- OUT R4, R1 (Write value R1 into R4)
      mem_storage(112) <= X"40001000"; -- JMP 1 (Back to main)

    ELSIF rising_edge (core_clk) THEN

      instruction <= mem_storage (to_integer(unsigned (address)));
    END IF;
  END PROCESS;

END mem_arch;