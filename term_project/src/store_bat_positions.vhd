library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity store_bat_positions is
    port (
        -- Processor interface
        clk         : in std_logic;
        clk_en      : in std_logic;
        start       : in std_logic;
        reset       : in std_logic;
        done        : out std_logic;
        dataa       : in std_logic_vector(31 downto 0);
        datab       : in std_logic_vector(31 downto 0);
        result      : out std_logic_vector(31 downto 0);

        -- External interface
        bat_left_y  : out std_logic_vector(15 downto 0);
        bat_right_y : out std_logic_vector(15 downto 0)
    );
end store_bat_positions;

architecture behavior of store_bat_positions is
    signal bat_left_y_internal  : std_logic_vector(15 downto 0);
    signal bat_right_y_internal : std_logic_vector(15 downto 0);
begin
    bat_left_y  <= bat_left_y_internal;
    bat_right_y <= bat_right_y_internal;

    main_p : process (clk)
    begin
        if rising_edge(clk) and clk_en = '1' then
            if reset = '1' then
                bat_left_y_internal  <= (others => '0');
                bat_right_y_internal <= (others => '0');
                result               <= (others => '0');
                done                 <= '0';
            else
                bat_left_y_internal  <= bat_left_y_internal;
                bat_right_y_internal <= bat_right_y_internal;
                result               <= (others => '0');
                done                 <= '0';
                if start = '1' then
                    bat_left_y_internal  <= dataa(15 downto 0);
                    bat_right_y_internal <= datab(15 downto 0);
                    result               <= x"0000002A";
                    done                 <= '1';
                end if;
            end if;
        end if;
    end process main_p;
end;
