# Drawer module interface description

The drawer module is used to draw 2 bats and a ball on given positions on a screen.

## Inputs
- Ballposisjon
- Høyde venstre og høyre kølle
- Start drawing

## Outputs
- Pikselverdier til framebuffer
- X/Y-verdier til framebuffer
- Write enable til framebuffer
- Ready to start new drawing

## Generics
- Oppløsning
- Ballradius
- Køllehøyde
- Køllebredde

## Ports


