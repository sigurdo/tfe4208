library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
use work.all;

entity fpga_top is
    port (
        -- Inputs
        CLOCK_50    : in std_logic;
        KEY         : in std_logic_vector(3 downto 0);

        -- Outputs
        VGA_CLK     : out std_logic;
        VGA_HS      : out std_logic;
        VGA_VS      : out std_logic;
        VGA_SYNC_N  : out std_logic;
        VGA_BLANK_N : out std_logic;
        VGA_R       : out std_logic_vector(7 downto 0);
        VGA_G       : out std_logic_vector(7 downto 0);
        VGA_B       : out std_logic_vector(7 downto 0);
        HEX0        : out std_logic_vector(6 downto 0);
        HEX1        : out std_logic_vector(6 downto 0);
        HEX2        : out std_logic_vector(6 downto 0);
        HEX3        : out std_logic_vector(6 downto 0);
        HEX4        : out std_logic_vector(6 downto 0);
        HEX5        : out std_logic_vector(6 downto 0);
        HEX6        : out std_logic_vector(6 downto 0);
        HEX7        : out std_logic_vector(6 downto 0)
    );
end fpga_top;

architecture behavior of fpga_top is
    -- Inputs
    signal clk              : std_logic;
    signal buttons          : std_logic_vector(3 downto 0);

    -- Signals from SOPC
    signal reset            : std_logic;
    signal ball_pos_x       : std_logic_vector(15 downto 0);
    signal ball_pos_y       : std_logic_vector(15 downto 0);
    signal left_bat_y       : std_logic_vector(15 downto 0);
    signal right_bat_y      : std_logic_vector(15 downto 0);
    signal left_score       : std_logic_vector(3 downto 0);
    signal right_score      : std_logic_vector(3 downto 0);

    -- Signals from VGA controller
    signal h_sync           : std_logic;
    signal v_sync           : std_logic;
    signal n_sync           : std_logic;
    signal n_blank          : std_logic;
    signal write_r           : std_logic_vector(7 downto 0);
    signal write_g           : std_logic_vector(7 downto 0);
    signal write_b           : std_logic_vector(7 downto 0);

    component FRESH_SOPC is
        port (
            clk_clk                                       : in std_logic := 'X';               -- clk
            reset_reset_n                                 : in std_logic := 'X';               -- reset_n
            reset_hardware_0_conduit_reset_hardware       : out std_logic;                     -- reset_hardware
            store_ball_position_0_conduit_ball_x          : out std_logic_vector(15 downto 0); -- ball_x
            store_ball_position_0_conduit_ball_y          : out std_logic_vector(15 downto 0); -- ball_y
            store_bat_positions_0_conduit_end_bat_left_y  : out std_logic_vector(15 downto 0); -- bat_left_y
            store_bat_positions_0_conduit_end_bat_right_y : out std_logic_vector(15 downto 0); -- bat_right_y
            get_button_statuses_0_conduit_end_0_button_0  : in std_logic := 'X';               -- button_0
            get_button_statuses_0_conduit_end_0_button_1  : in std_logic := 'X';               -- button_1
            get_button_statuses_0_conduit_end_0_button_2  : in std_logic := 'X';               -- button_2
            get_button_statuses_0_conduit_end_0_button_3  : in std_logic := 'X';               -- button_3
            get_vsync_0_conduit_beginbursttransfer        : in std_logic := 'X';               -- beginbursttransfer
            set_score_0_conduit_end_left_score            : out std_logic_vector(3 downto 0);  -- left_score
            set_score_0_conduit_end_right_score           : out std_logic_vector(3 downto 0)   -- right_score
        );
    end component FRESH_SOPC;

begin
    -- Inputs
    clk     <= CLOCK_50;
    buttons <= KEY;

    -- SOPC
    sopc_i : component FRESH_SOPC port map(
        clk_clk                                       => clk,         --                                 clk.clk
        reset_reset_n                                 => '1',         --                               reset.reset_n
        reset_hardware_0_conduit_reset_hardware       => reset,       --            reset_hardware_0_conduit.reset_hardware
        store_ball_position_0_conduit_ball_x          => ball_pos_x,  --       store_ball_position_0_conduit.ball_x
        store_ball_position_0_conduit_ball_y          => ball_pos_y,  --                                    .ball_y
        store_bat_positions_0_conduit_end_bat_left_y  => left_bat_y,  --   store_bat_positions_0_conduit_end.bat_left_y
        store_bat_positions_0_conduit_end_bat_right_y => right_bat_y, --                                    .bat_right_y
        get_button_statuses_0_conduit_end_0_button_0  => buttons(0),  -- get_button_statuses_0_conduit_end_0.button_0
        get_button_statuses_0_conduit_end_0_button_1  => buttons(1),  --                                    .button_1
        get_button_statuses_0_conduit_end_0_button_2  => buttons(2),  --                                    .button_2
        get_button_statuses_0_conduit_end_0_button_3  => buttons(3),  --                                    .button_3
        get_vsync_0_conduit_beginbursttransfer        => v_sync,      --                 get_vsync_0_conduit.beginbursttransfer
        set_score_0_conduit_end_left_score            => left_score,  --             set_score_0_conduit_end.left_score
        set_score_0_conduit_end_right_score           => right_score  --                                    .right_score
    );

    -- VGA controller
    vga_controller_i : entity work.vga_controller port map(
        pixel_clock      => clk,
        reset            => reset,
        ball_pos_x       => ball_pos_x,
        ball_pos_y       => ball_pos_y,
        left_bat_y       => left_bat_y,
        right_bat_y      => right_bat_y,
        h_sync           => h_sync,
        v_sync           => v_sync,
        n_sync           => n_sync,
        n_blank          => n_blank,
        write_r           => write_r,
        write_g           => write_g,
        write_b           => write_b
    );

    -- Score viewer
    score_viewer_i : entity work.score_viewer port map(
        left_score  => left_score,
        right_score => right_score,
        HEX0        => HEX0,
        HEX1        => HEX1,
        HEX2        => HEX2,
        HEX3        => HEX3,
        HEX4        => HEX4,
        HEX5        => HEX5,
        HEX6        => HEX6,
        HEX7        => HEX7
        );

    -- Outputs
    VGA_CLK     <= clk;
    VGA_HS      <= h_sync;
    VGA_VS      <= v_sync;
    VGA_SYNC_N  <= n_sync;
    VGA_BLANK_N <= n_blank;
    VGA_R       <= write_r;
    VGA_G       <= write_g;
    VGA_B       <= write_b;
end;
