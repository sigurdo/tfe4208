library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity reset_hardware is
    port (
        -- Processor interface
        clk              : in std_logic;
        clk_en           : in std_logic;
        start            : in std_logic;
        reset            : in std_logic;
        done             : out std_logic;
        dataa            : in std_logic_vector(31 downto 0);
        datab            : in std_logic_vector(31 downto 0);
        result           : out std_logic_vector(31 downto 0);

        -- External interface
        reset_hardware   : out std_logic
    );
end reset_hardware;

architecture behavior of reset_hardware is
    signal reset_hardware_internal : std_logic;
begin
    reset_hardware <= reset_hardware_internal;

    main_p: process (clk)
    begin
        if rising_edge(clk) and clk_en = '1' then
            if reset = '1' then
                reset_hardware_internal <= '0';
                result <= (others => '0');
                done <= '0';
            else
                reset_hardware_internal <= '0';
                result <= (others => '0');
                done <= '0';
                if start = '1' then
                    reset_hardware_internal <= '1';
                    result <= x"0000002A";
                    done <='1';
                end if;
            end if;
        end if;
    end process main_p;
end;
