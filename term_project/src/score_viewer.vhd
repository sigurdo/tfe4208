library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity score_viewer is
    port (
        -- Inputs
        left_score  : in std_logic_vector(3 downto 0);
        right_score : in std_logic_vector(3 downto 0);

        -- Outputs
        HEX0        : out std_logic_vector(0 to 6);
        HEX1        : out std_logic_vector(0 to 6);
        HEX2        : out std_logic_vector(0 to 6);
        HEX3        : out std_logic_vector(0 to 6);
        HEX4        : out std_logic_vector(0 to 6);
        HEX5        : out std_logic_vector(0 to 6);
        HEX6        : out std_logic_vector(0 to 6);
        HEX7        : out std_logic_vector(0 to 6)
    );
end score_viewer;

architecture behavior of score_viewer is
begin
    left_score_p : process (left_score)
        variable left_score_int : integer range 0 to 15;
    begin
        left_score_int := to_integer(unsigned(left_score));

        HEX7_case : case left_score_int is
            when 10     => HEX7     <= "1111001";
            when others => HEX7 <= "1111111";
        end case;

        HEX6_case : case left_score_int is
            when 1      => HEX6      <= "1111001";
            when 2      => HEX6      <= "0100100";
            when 3      => HEX6      <= "0110000";
            when 4      => HEX6      <= "0011001";
            when 5      => HEX6      <= "0010010";
            when 6      => HEX6      <= "0000010";
            when 7      => HEX6      <= "1111000";
            when 8      => HEX6      <= "0000000";
            when 9      => HEX6      <= "0010000";
            when others => HEX6      <= "1000000";
        end case;
    end process;

    right_score_p : process (right_score)
        variable right_score_int : integer range 0 to 15;
    begin
        right_score_int := to_integer(unsigned(right_score));

        HEX5_case : case right_score_int is
            when 10     => HEX5     <= "1111001";
            when others => HEX5 <= "1111111";
        end case;

        HEX4_case : case right_score_int is
            when 1      => HEX4      <= "1111001";
            when 2      => HEX4      <= "0100100";
            when 3      => HEX4      <= "0110000";
            when 4      => HEX4      <= "0011001";
            when 5      => HEX4      <= "0010010";
            when 6      => HEX4      <= "0000010";
            when 7      => HEX4      <= "1111000";
            when 8      => HEX4      <= "0000000";
            when 9      => HEX4      <= "0010000";
            when others => HEX4      <= "1000000";
        end case;
    end process;

    HEX0 <= "0111111";
    HEX1 <= "0111111";
    HEX2 <= "0111111";
    HEX3 <= "0111111";
end;
