library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity get_vsync is
    port (
        -- Processor interface
        clk              : in std_logic;
        clk_en           : in std_logic;
        start            : in std_logic;
        reset            : in std_logic;
        done             : out std_logic;
        dataa            : in std_logic_vector(31 downto 0);
        datab            : in std_logic_vector(31 downto 0);
        result           : out std_logic_vector(31 downto 0);

        -- External interface
        v_sync : in std_logic
    );
end get_vsync;

architecture behavior of get_vsync is
begin
    main_p: process (clk)
    begin
        if rising_edge(clk) and clk_en = '1' then
            if reset = '1' then
                result <= (others => '0');
                done <= '0';
            else
                result <= (others => '0');
                done <= '0';
                if start = '1' then
                    result <= (31 downto 1 => '0') & v_sync;
                    done <='1';
                end if;
            end if;
        end if;
    end process main_p;
end;
