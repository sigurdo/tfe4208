library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity get_button_statuses is
    port (
        -- Processor interface
        clk              : in std_logic;
        clk_en           : in std_logic;
        start            : in std_logic;
        reset            : in std_logic;
        done             : out std_logic;
        dataa            : in std_logic_vector(31 downto 0);
        datab            : in std_logic_vector(31 downto 0);
        result           : out std_logic_vector(31 downto 0);

        -- External interface
        button_0 : in std_logic;
        button_1 : in std_logic;
        button_2 : in std_logic;
        button_3 : in std_logic
    );
end get_button_statuses;

architecture behavior of get_button_statuses is
begin
    main_p: process (clk)
    begin
        if rising_edge(clk) and clk_en = '1' then
            if reset = '1' then
                result <= (others => '0');
                done <= '0';
            else
                result <= (others => '0');
                done <= '0';
                if start = '1' then
                    -- Must invert all buttons to be active high instead of active low
                    result <= (31 downto 4 => '0') & (not button_3) & (not button_2) & (not button_1) & (not button_0);
                    done <='1';
                end if;
            end if;
        end if;
    end process main_p;
end;
