# Hvordan kommunisere fra SW til drawer i HW

På samme måte som CRC-akselleratoren i den siste laben.


Dette skal være fullt mulig å få til ved å bruke "External interface" som instruction type.

## Custom instructions
- Store ball position (X, Y)
- Store left bat position (Y)
- Store right bat position (Y)

## External ports
- Ball X
- Ball Y
- Left bat Y
- Right bat H

## Frame synchronization

No explicit frame synchronization between hardware and software
