library ieee;
use ieee.std_logic_1164.all;
package ram_package is
    -- 1024 * 768 = 786432 bits = 98304 bytes
    -- log_2(786432) <= 20 bit addressevektor
    constant ram_width : integer := 1;
    constant ram_depth : integer := 1024 * 768;

    type word is array(0 to ram_width - 1) of std_logic;
    type ram is array(0 to ram_depth - 1) of word;
    subtype address_vector is integer range 0 to ram_depth - 1;
end ram_package;
--------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.ram_package.all;
entity ram_dual is
    port (
        clock1        : in std_logic;
        clock2        : in std_logic;
        data          : in word;
        write_address : in address_vector;
        read_address  : in address_vector;
        we            : in std_logic;
        q             : out word
    );
end ram_dual;

architecture rtl of ram_dual is
    signal ram_block : RAM;
begin
    process (clock1)
    begin
        if (clock1'event and clock1 = '1') then
            if (we = '1') then
                ram_block(write_address) <= data;
            end if;
        end if;
    end process;
    process (clock2)
    begin
        if (clock2'event and clock2 = '1') then
            q <= ram_block(read_address);
        end if;
    end process;
end rtl;
--------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
-- use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use work.ram_package.all;
-- library work;
-- use work.all;

entity framebuffer is
    generic (
        horizontal_pixels : integer := 1024;
        vertical_pixels   : integer := 768
    );
    port (
        clk          : in std_logic;
        reset        : in std_logic;
        write_x      : in std_logic_vector(15 downto 0);
        write_y      : in std_logic_vector(15 downto 0);
        write_enable : in std_logic;
        write_data   : in std_logic;
        read_enable  : in std_logic;
        read_r       : out std_logic_vector(7 downto 0);
        read_g       : out std_logic_vector(7 downto 0);
        read_b       : out std_logic_vector(7 downto 0)
    );
end entity framebuffer;

architecture behavior of framebuffer is
    signal ram_data      : word;
    signal write_address : address_vector;
    signal read_address  : address_vector;
    signal ram_q         : word;
begin
    ram_data(0)   <= write_data;
    write_address <= (conv_integer(unsigned(write_y)) * horizontal_pixels) + conv_integer(unsigned(write_x));
    read_r        <= (others => ram_q(0));
    read_g        <= (others => ram_q(0));
    read_b        <= (others => ram_q(0));

    read_address_p : process (clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                read_address <= 0;
            else
                if read_enable = '1' then
                    if read_address = ram_depth - 1 then
                        read_address <= 0;
                    else
                        read_address <= read_address + 1;
                    end if;
                else
                    read_address <= read_address;
                end if;
            end if;
        end if;
    end process read_address_p;

    ram_i : entity work.ram_dual
        port map(
            clock1        => clk,
            clock2        => clk,
            data          => ram_data,
            write_address => write_address,
            read_address  => read_address,
            we            => write_enable,
            q             => ram_q
        );
end behavior;
