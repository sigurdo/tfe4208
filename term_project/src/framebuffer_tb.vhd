
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.ram_package.all;
use std.env.finish;
-- library work;
-- use work.all;

entity framebuffer_tb is
end entity framebuffer_tb;

architecture behavior of framebuffer_tb is
    constant horizontal_pixels : integer := 1024;
    constant vertical_pixels : integer := 768;

    signal clk          : std_logic := '0';
    signal reset        : std_logic;
    signal write_x      : std_logic_vector(15 downto 0);
    signal write_y      : std_logic_vector(15 downto 0);
    signal write_enable : std_logic;
    signal write_data   : std_logic;
    signal read_enable  : std_logic;
    signal read_r       : std_logic_vector(7 downto 0);
    signal read_g       : std_logic_vector(7 downto 0);
    signal read_b       : std_logic_vector(7 downto 0);

begin
    dut : entity work.framebuffer
        port map(
            clk          => clk,
            reset        => reset,
            write_x      => write_x,
            write_y      => write_y,
            write_enable => write_enable,
            write_data   => write_data,
            read_enable  => read_enable,
            read_r    => read_r,
            read_g    => read_g,
            read_b    => read_b
        );

    clk <= not clk after 0.5 ns;

    main_p : process
    begin
        reset        <= '0';
        write_x      <= (others => '0');
        write_y      <= (others => '0');
        write_enable <= '0';
        write_data   <= '0';
        read_enable  <= '0';

        wait until rising_edge(clk);

        reset <= '1';
        wait until rising_edge(clk);

        reset <= '0';
        wait until rising_edge(clk);

        for y in 0 to vertical_pixels - 1 loop
            for x in 0 to horizontal_pixels - 1 loop
                write_x <= std_logic_vector(to_unsigned(x, 16));
                write_y <= std_logic_vector(to_unsigned(y, 16));
                write_data <= '1';
                write_enable <= '1';
                wait until rising_edge(clk);
            end loop;
        end loop;

        write_enable <= '0';
        wait until rising_edge(clk);

        for y in 0 to vertical_pixels - 1 loop
            for x in 0 to horizontal_pixels - 1 loop
                read_enable <= '1';
                assert read_r = ((7 downto 0) => '1');
                assert read_g = ((7 downto 0) => '1');
                assert read_b = ((7 downto 0) => '1');
                wait until rising_edge(clk);
            end loop;
        end loop;
        wait until rising_edge(clk);
        finish;
    end process main_p;
end behavior;
