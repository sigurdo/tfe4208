library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity vga_controller is
    generic (
        --Every value is in number of pixels
        horizontal_pixels : integer := 800;
        front_porch_h     : integer := 56;
        sync_puls_h       : integer := 120;
        back_porch_h      : integer := 64;

        vertical_pixels   : integer := 600;
        front_porch_v     : integer := 37;
        sync_puls_v       : integer := 6;
        back_porch_v      : integer := 23;

        ball_radius       : integer := 20;
        --Width and hight of the ball is from center to one edge
        --Totall width is therfore 2*width
        bat_width         : integer := 20;
        bat_height        : integer := 40;
        bat_pos_x         : integer := 30
    );
    port (
        pixel_clock : in std_logic;
        reset       : in std_logic;

        ball_pos_x  : in std_logic_vector(15 downto 0);
        ball_pos_y  : in std_logic_vector(15 downto 0);
        left_bat_y  : in std_logic_vector(15 downto 0);
        right_bat_y : in std_logic_vector(15 downto 0);

        h_sync      : out std_logic;
        v_sync      : out std_logic;
        n_sync      : out std_logic;
        n_blank     : out std_logic;

        write_r     : out std_logic_vector(7 downto 0);
        write_g     : out std_logic_vector(7 downto 0);
        write_b     : out std_logic_vector(7 downto 0)
    );
end vga_controller;
architecture behavior of vga_controller is
    constant horizontal_pixel_line  : integer := horizontal_pixels + front_porch_h + sync_puls_h + back_porch_h;
    constant vertical_pixel_line    : integer := vertical_pixels + front_porch_v + sync_puls_v + back_porch_v;

    constant horizontal_before_sync : integer := horizontal_pixels + front_porch_h;
    constant horizontal_after_sync  : integer := horizontal_pixels + front_porch_h + sync_puls_h;

    constant vertical_before_sync   : integer := vertical_pixels + front_porch_v;
    constant vertical_after_sync    : integer := vertical_pixels + front_porch_v + sync_puls_v;

    constant right_bat_x            : integer := horizontal_pixels - bat_width;
    constant left_bat_x             : integer := bat_pos_x;

    signal ball_pos_x_reg           : integer range horizontal_pixels downto 0;
    signal ball_pos_y_reg           : integer range vertical_pixels downto 0;
    signal left_bat_y_reg           : integer range vertical_pixels downto 0;
    signal right_bat_y_reg          : integer range vertical_pixels downto 0;
    signal v_sync_in                : std_logic;
begin

    n_sync <= '0';
    process (pixel_clock, reset)
        variable horizontal_current_count : integer range horizontal_pixel_line - 1 downto 0 := 0;
        variable vertical_current_count   : integer range vertical_pixel_line - 1 downto 0   := 0;

        function is_inside_bat(
            constant bat_x   : integer;
            constant bat_y   : integer;
            constant v_count : integer;
            constant h_count : integer
        ) return std_logic is
            variable bool : std_logic;
        begin
            if (h_count >= bat_x - bat_width) and (h_count <= bat_x + bat_width) and (v_count >= bat_y - bat_height) and (v_count <= bat_y + bat_height) then
                bool := '1';
                return bool;
            else
                bool := '0';
                return bool;
            end if;
        end function is_inside_bat;

    begin
        if (reset = '1') then
            horizontal_current_count := 0;
            vertical_current_count   := 0;
            h_sync    <= '0';
            v_sync_in <= '0';
            write_r   <= (others => '0');
            write_g   <= (others => '0');
            write_b   <= (others => '0');
        elsif (rising_edge(pixel_clock)) then

            if (horizontal_current_count < horizontal_pixel_line) then
                horizontal_current_count := horizontal_current_count + 1;
            else
                horizontal_current_count := 0;

                if (vertical_current_count < vertical_pixel_line) then
                    vertical_current_count := vertical_current_count + 1;
                else
                    vertical_current_count := 0;
                end if;
            end if;
            if ((horizontal_current_count < horizontal_before_sync) or (horizontal_current_count > horizontal_after_sync)) then
                h_sync  <= '1';
                n_blank <= '1';
            else
                h_sync  <= '0';
                n_blank <= '0';
            end if;

            if ((vertical_current_count < vertical_before_sync) or (vertical_current_count > vertical_after_sync)) then
                v_sync_in <= '1';
            else
                v_sync_in <= '0';
            end if;
            --Right bat
            if (is_inside_bat(right_bat_x, right_bat_y_reg, vertical_current_count, horizontal_current_count) = '1') then
                write_r <= (others => '1');
                write_g <= (others => '1');
                write_b <= (others => '1');

                --Left bat
            elsif (is_inside_bat(left_bat_x, left_bat_y_reg, vertical_current_count, horizontal_current_count) = '1') then
                write_r <= (others => '1');
                write_g <= (others => '1');
                write_b <= (others => '1');

                --Ball  x^2 + y^2 = ball_radius^2 equation of a circle
            elsif ((horizontal_current_count - ball_pos_x_reg) ** 2 + (vertical_current_count - ball_pos_y_reg) ** 2 <= ball_radius ** 2) then
                write_r <= (others => '1');
                write_g <= (others => '1');
                write_b <= (others => '1');

                --Draw set every other pixel to zero
            else
                write_r <= (others => '0');
                write_g <= (others => '0');
                write_b <= (others => '0');
            end if;

        end if;

    end process;

    registers : process (pixel_clock)
    begin
        if (rising_edge(pixel_clock) and v_sync_in = '0') then
            ball_pos_x_reg  <= to_integer(unsigned(ball_pos_x));
            ball_pos_y_reg  <= to_integer(unsigned(ball_pos_y));
            left_bat_y_reg  <= to_integer(unsigned(left_bat_y));
            right_bat_y_reg <= to_integer(unsigned(right_bat_y));
        else
            ball_pos_x_reg  <= ball_pos_x_reg;
            ball_pos_y_reg  <= ball_pos_y_reg;
            left_bat_y_reg  <= left_bat_y_reg;
            right_bat_y_reg <= right_bat_y_reg;
        end if;
    end process registers;

    v_sync <= v_sync_in;

end;
