# Frame buffer module

Infererer RAM og oversetter X/Y koordinat til riktig addresse-offset i RAMen.

Lagrer pikselverdier internt som kun 0 eller 1 (1 bit), men sender ut 8 bit, altså 0 eller 255.

FPGAen skal ha 3.888 Mb on-chip RAM, såkalt embedded memory (https://www.intel.com/content/www/us/en/products/sku/210467/cyclone-iv-ep4ce115-fpga/specifications.html), altså sånn ca 500KB, som skal være mer enn nok til 1024*720 piksler svart-hvitt, ingen grayscale, altså sånn ca 90 KB.

Se eksempel [her](https://www.intel.com/content/www/us/en/programmable/quartushelp/13.0/mergedProjects/hdl/vlog/vlog_pro_ram_inferred.htm).

## Generics
- Oppløsning

## Inputs
- Read enable
- X read address
- Y read address
- Read data, 1 bit
- Write enable
- X write address
- Y write address

## Outputs
- Write data, 8 bi
