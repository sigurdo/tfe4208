library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity drawer is
    generic (
        horizontal_pixels : integer := 1024;
        vertical_pixels   : integer := 768;

        ball_radius       : integer := 20;

        bat_width         : integer := 20;
        bat_height        : integer := 40;
        bat_pos_x         : integer := 30
    );
    port (
        clk              : in std_logic;
        ball_pos_x       : in std_logic_vector(15 downto 0);
        ball_pos_y       : in std_logic_vector(15 downto 0);

        left_bat_y       : in std_logic_vector(15 downto 0);
        right_bat_y      : in std_logic_vector(15 downto 0);

        v_sync           : in std_logic;
        ready            : out std_logic;

        pixel_val        : out std_logic;
        framebuffer_x    : out std_logic_vector(15 downto 0);
        framebuffer_y    : out std_logic_vector(15 downto 0);
        framebuffer_w_en : out std_logic

    );
end drawer;

architecture behavior of drawer is

    type state is (IDLE, DRAW);
    signal state_curr, state_next : state;

    -- Position registers 
    signal ball_pos_x_reg         : std_logic_vector(15 downto 0);
    signal ball_pos_y_reg         : std_logic_vector(15 downto 0);
    signal left_bat_y_reg         : std_logic_vector(15 downto 0);
    signal right_bat_y_reg        : std_logic_vector(15 downto 0);

    signal frame_done :std_logic;
    signal draw_frame :std_logic;

begin
    FSM : process (v_sync, state_curr, frame_done)
    begin
        case(state_curr) is
            when IDLE =>
            ready         <= '1';
            
            if (v_sync = '0') then
                state_next <= DRAW;

            else
                state_next <= IDLE;
            end if;

            when DRAW =>
            ready         <= '0';
            draw_frame <= '1';
            if (frame_done = '1') then
                state_next <= IDLE;
                draw_frame <= '0';
            else
                state_next <= DRAW;
            end if;

            

            when others =>
            ready         <= '0';
            draw_frame    <= '0';
            state_next    <= IDLE;
        end case;

    end process FSM;
    
    draw_image : process (clk)
    variable horizontal_current_count : integer range horizontal_pixels - 1 downto 0 := 0;
    variable vertical_current_count   : integer range vertical_pixels - 1 downto 0   := 0;
    begin
        if(draw_frame = '0')then
            horizontal_current_count := 0;
            vertical_current_count := 0;
        end if;

        if (horizontal_current_count < horizontal_pixels) then
            horizontal_current_count := horizontal_current_count + 1;
            frame_done <= '0';
        else
            horizontal_current_count := 0;

            if (vertical_current_count < vertical_pixels) then
                vertical_current_count := vertical_current_count + 1;
            else
                vertical_current_count := 0;
                frame_done <= '1';
            end if;
        end if;
        

        if(draw_frame = '1') then
            --Left bat
            if(((vertical_current_count >= (to_integer(unsigned(right_bat_y_reg)) - bat_height) )and (vertical_current_count <= (to_integer(unsigned(right_bat_y_reg)) + bat_height)) ) and 
            ((horizontal_current_count >=  (horizontal_pixels - bat_pos_x - bat_width)) and (horizontal_current_count <= (horizontal_pixels - bat_pos_x + bat_width)) )) then
                framebuffer_w_en <= '1';
                pixel_val        <= '1';
            --Right bat
            elsif((vertical_current_count >= (to_integer(unsigned(left_bat_y_reg)) - bat_height)) and (vertical_current_count <= (to_integer(unsigned(left_bat_y_reg)) + bat_height)) and 
            ((horizontal_current_count >=  (bat_pos_x - bat_width)) and (horizontal_current_count <= (bat_pos_x + bat_width)  )))then
                framebuffer_w_en <= '1';
                pixel_val        <= '1';
            --Ball
            elsif ((horizontal_current_count - to_integer(unsigned(ball_pos_x_reg)))**2 + (vertical_current_count - to_integer(unsigned(ball_pos_y_reg)))**2 <= ball_radius**2) then
                framebuffer_w_en <= '1';
                pixel_val        <= '1';
            --Draw set every other pixel to zero
            else
                framebuffer_w_en <= '1';
                pixel_val        <= '0';
            end if;
        end if;
        framebuffer_x <= std_logic_vector(to_unsigned(horizontal_current_count, framebuffer_x'length));       
        framebuffer_y <= std_logic_vector(to_unsigned(vertical_current_count, framebuffer_y'length));
        framebuffer_w_en <= '0';
        pixel_val        <= '0';
    end process draw_image;


    registers : process (clk)
    begin
        if (rising_edge(clk) and v_sync = '1') then
            ball_pos_x_reg  <= ball_pos_x;
            ball_pos_y_reg  <= ball_pos_y;
            left_bat_y_reg  <= left_bat_y;
            right_bat_y_reg <= right_bat_y;

        else
            ball_pos_x_reg  <= ball_pos_x_reg;
            ball_pos_y_reg  <= ball_pos_y_reg;
            left_bat_y_reg  <= left_bat_y_reg;
            right_bat_y_reg <= right_bat_y_reg;
        end if;
    end process registers;

    state_reg : process (clk)
    begin
        if (rising_edge(clk)) then
            state_curr <= state_next;
        else
            state_curr <= state_curr;
        end if;
    end process state_reg;

    
end;
