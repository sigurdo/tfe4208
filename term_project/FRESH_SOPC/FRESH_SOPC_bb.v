
module FRESH_SOPC (
	clk_clk,
	reset_reset_n,
	store_ball_position_0_conduit_ball_x,
	store_ball_position_0_conduit_ball_y);	

	input		clk_clk;
	input		reset_reset_n;
	output	[31:0]	store_ball_position_0_conduit_ball_x;
	output	[31:0]	store_ball_position_0_conduit_ball_y;
endmodule
