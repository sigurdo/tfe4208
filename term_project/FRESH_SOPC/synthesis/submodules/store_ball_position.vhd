library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity store_ball_position is
    port (
        -- Processor interface
        clk              : in std_logic;
        clk_en           : in std_logic;
        start            : in std_logic;
        reset            : in std_logic;
        done             : out std_logic;
        dataa            : in std_logic_vector(31 downto 0);
        datab            : in std_logic_vector(31 downto 0);
        result           : out std_logic_vector(31 downto 0);

        -- External interface
        ball_x : out std_logic_vector(31 downto 0);
        ball_y : out std_logic_vector(31 downto 0)
    );
end store_ball_position;

architecture behavior of store_ball_position is
    signal ball_x_internal : std_logic_vector(31 downto 0);
    signal ball_y_internal : std_logic_vector(31 downto 0);
begin
    ball_x <= ball_x_internal;
    ball_y <= ball_y_internal;

    main_p: process (clk)
    begin
        if rising_edge(clk) and clk_en = '1' then
            if reset = '1' then
                ball_x_internal <= (others => '0');
                ball_y_internal <= (others => '0');
                result <= (others => '0');
                done <= '0';
            else
                ball_x_internal <= ball_x_internal;
                ball_y_internal <= ball_y_internal;
                result <= (others => '0');
                done <= '0';
                if start = '1' then
                    ball_x_internal <= dataa;
                    ball_y_internal <= datab;
                    result <= x"0000002A";
                    done <='1';
                end if;
            end if;
        end if;
    end process main_p;
end;
