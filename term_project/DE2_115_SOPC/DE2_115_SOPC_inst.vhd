	component DE2_115_SOPC is
		port (
			ball_position_conduit_ball_y       : out std_logic_vector(31 downto 0);        -- ball_y
			ball_position_conduit_ball_x       : out std_logic_vector(31 downto 0);        -- ball_x
			clk_clk                            : in  std_logic                     := 'X'; -- clk
			pio_led_external_connection_export : out std_logic_vector(7 downto 0);         -- export
			reset_reset_n                      : in  std_logic                     := 'X'  -- reset_n
		);
	end component DE2_115_SOPC;

	u0 : component DE2_115_SOPC
		port map (
			ball_position_conduit_ball_y       => CONNECTED_TO_ball_position_conduit_ball_y,       --       ball_position_conduit.ball_y
			ball_position_conduit_ball_x       => CONNECTED_TO_ball_position_conduit_ball_x,       --                            .ball_x
			clk_clk                            => CONNECTED_TO_clk_clk,                            --                         clk.clk
			pio_led_external_connection_export => CONNECTED_TO_pio_led_external_connection_export, -- pio_led_external_connection.export
			reset_reset_n                      => CONNECTED_TO_reset_reset_n                       --                       reset.reset_n
		);

