
module DE2_115_SOPC (
	ball_position_conduit_ball_y,
	ball_position_conduit_ball_x,
	clk_clk,
	pio_led_external_connection_export,
	reset_reset_n);	

	output	[31:0]	ball_position_conduit_ball_y;
	output	[31:0]	ball_position_conduit_ball_x;
	input		clk_clk;
	output	[7:0]	pio_led_external_connection_export;
	input		reset_reset_n;
endmodule
