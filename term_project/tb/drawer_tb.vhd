library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.env.finish;
use std.standard.severity_level;

use IEEE.math_real.all;
use IEEE.math_real."ceil";
use IEEE.math_real."log2";

entity drawer_tb is
    generic (
        horizontal_pixels : integer := 1024;
        vertical_pixels   : integer := 768;

        ball_radius       : integer := 20;

        bat_width         : integer := 20;
        bat_height        : integer := 40;
        bat_pos_x         : integer := 30
    );
end drawer_tb;

architecture rtl of drawer_tb is

    type screen_arr is array (16 * 16 - 1 downto 0) of std_logic_vector(15 downto 0);

    signal clk              : std_logic;
    signal ball_pos_x       : std_logic_vector(15 downto 0);
    signal ball_pos_y       : std_logic_vector(15 downto 0);
    signal left_bat_y       : std_logic_vector(15 downto 0);
    signal right_bat_y      : std_logic_vector(15 downto 0);
    signal start_draw       : std_logic;
    signal ready            : std_logic;
    signal pixel_val        : std_logic;
    signal framebuffer_x    : std_logic_vector(15 downto 0);
    signal framebuffer_y    : std_logic_vector(15 downto 0);
    signal framebuffer_w_en : std_logic;

begin
    dut : entity work.drawer
        generic map(
            horizontal_pixels => horizontal_pixels,
            vertical_pixels   => vertical_pixels,
            ball_radius       => ball_radius,
            bat_width         => bat_width,
            bat_height        => bat_height,
            bat_pos_x         => bat_pos_x
        )
        port map(
            clk              => clk,
            ball_pos_x       => ball_pos_x,
            ball_pos_y       => ball_pos_y,

            left_bat_y       => left_bat_y,
            right_bat_y      => right_bat_y,

            start_draw       => start_draw,
            ready            => ready,

            pixel_val        => pixel_val,
            framebuffer_x    => framebuffer_x,
            framebuffer_y    => framebuffer_y,
            framebuffer_w_en => framebuffer_w_en

        );
    clk_gen : process is
    begin
        clk <= '1';
        wait for 6 ns;
        clk <= '0';
        wait for 6 ns;
    end process;

    main_p : process

        procedure testcase(
            constant pos_ball_x : integer;
            constant pos_ball_y : integer;
            constant left_bat   : integer;
            constant right_bat  : integer
        ) is
        begin

            ball_pos_x  <= std_logic_vector(to_unsigned(pos_ball_x, ball_pos_x'length));
            ball_pos_y  <= std_logic_vector(to_unsigned(pos_ball_y, ball_pos_y'length));
            left_bat_y  <= std_logic_vector(to_unsigned(left_bat, left_bat_y'length));
            right_bat_y <= std_logic_vector(to_unsigned(right_bat, right_bat_y'length));
            start_draw  <= '1';
            wait until rising_edge(clk);
            start_draw <= '0';
            
            while(ready = '0') loop
                wait until rising_edge(clk);
                if((to_integer(unsigned(framebuffer_y)) <= left_bat + bat_height) and (to_integer(unsigned(framebuffer_y)) >= left_bat - bat_height) and 
                    (to_integer(unsigned(framebuffer_x)) <= bat_pos_x + bat_width) and (to_integer(unsigned(framebuffer_x)) >= bat_pos_x - bat_height) ) or
                    ((to_integer(unsigned(framebuffer_y)) <= right_bat + bat_height) and (to_integer(unsigned(framebuffer_y)) >= right_bat - bat_height) and 
                    (to_integer(unsigned(framebuffer_x)) <= bat_pos_x + bat_width) and (to_integer(unsigned(framebuffer_x)) >= bat_pos_x - bat_height) ) or
                    (((to_integer(unsigned(framebuffer_x)) - pos_ball_x)**2 + (to_integer(unsigned(framebuffer_y)) - pos_ball_y)**2 <= ball_radius**2) ) then
                    assert(pixel_val = '1');
                    assert(framebuffer_w_en = '1');
                else
                    assert(pixel_val = '0');
                    assert(framebuffer_w_en = '1');
                end if;
            end loop;

        end procedure testcase;

    begin
        testcase( 300,300, 100, 600);
       wait until(ready = '1');
        testcase( 239,490, 509, 209);
       wait until(ready = '1');
        finish;
    end process main_p;
end rtl;
