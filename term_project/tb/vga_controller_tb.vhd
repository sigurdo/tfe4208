library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.env.finish;
use std.standard.severity_level;


entity vga_controller_tb is
    generic (
        --Every value is in number of pixels
        horizontal_pixels : integer := 800;
        front_porch_h     : integer := 56;
        sync_puls_h       : integer := 120;
        back_porch_h      : integer := 64;

        vertical_pixels   : integer := 600;
        front_porch_v     : integer := 37;
        sync_puls_v       : integer := 6;
        back_porch_v      : integer := 23;

        ball_radius       : integer := 20;
        --Width and hight of the ball is from center to one edge
        --Totall width is therfore 2*width
        bat_width         : integer := 20;
        bat_height        : integer := 40;
        bat_pos_x         : integer := 30
    );
end vga_controller_tb;

architecture rtl of vga_controller_tb is
    constant horizontal_pixel_line  : integer := horizontal_pixels + front_porch_h + sync_puls_h + back_porch_h;
    constant vertical_pixel_line    : integer := vertical_pixels + front_porch_v + sync_puls_v + back_porch_v;

    constant horizontal_before_sync : integer := horizontal_pixels + front_porch_h;
    constant horizontal_after_sync  : integer := horizontal_pixels + front_porch_h + sync_puls_h;

    constant vertical_before_sync   : integer := vertical_pixels + front_porch_v;
    constant vertical_after_sync    : integer := vertical_pixels + front_porch_v + sync_puls_v;

    constant right_bat_x            : integer := horizontal_pixels - bat_width;
    constant left_bat_x             : integer := bat_pos_x;

    constant all_ones               : std_logic_vector(7 downto 0) := (others => '1');
    constant all_zeros              : std_logic_vector(7 downto 0) := (others => '0');
    
    signal pixel_clock              : std_logic;
    signal reset                    : std_logic;
    
    signal h_sync                   : std_logic;
    signal v_sync                   : std_logic;
    signal n_sync                   : std_logic;
    signal n_blank                  : std_logic;
    
    signal ball_pos_x               : std_logic_vector(15 downto 0);
    signal ball_pos_y               : std_logic_vector(15 downto 0);
    signal left_bat_y               : std_logic_vector(15 downto 0);
    signal right_bat_y              : std_logic_vector(15 downto 0);
    
    
    
    signal write_r                  : std_logic_vector(7 downto 0);
    signal write_g                  : std_logic_vector(7 downto 0);
    signal write_b                  : std_logic_vector(7 downto 0);
    
begin
    dut : entity work.vga_controller
        generic map(
            horizontal_pixels => horizontal_pixels,
            front_porch_h     => front_porch_h,
            sync_puls_h       => sync_puls_h,
            back_porch_h      => back_porch_h,
            vertical_pixels   => vertical_pixels,
            front_porch_v     => front_porch_v,
            sync_puls_v       => sync_puls_v,
            back_porch_v      => back_porch_v,
            ball_radius       => ball_radius,
            bat_width         => bat_width,
            bat_height        => bat_height,
            bat_pos_x         => bat_pos_x

        )
        port map(
            pixel_clock => pixel_clock,
            reset       => reset,
            h_sync      => h_sync,
            v_sync      => v_sync,
            n_sync      => n_sync,
            n_blank     => n_blank,

            ball_pos_x  => ball_pos_x,
            ball_pos_y  => ball_pos_y,
            left_bat_y  => left_bat_y,
            right_bat_y => right_bat_y,

            write_r     => write_r,
            write_g     => write_g,
            write_b     => write_b
        );
    clk_gen : process is
    begin
        pixel_clock <= '1';
        wait for 6 ns;
        pixel_clock <= '0';
        wait for 6 ns;
    end process;

    main_p : process
    variable h_count                  : integer := 0;
    variable v_count                  : integer := 0;

    procedure testcase(
            constant pos_ball_x : integer;
            constant pos_ball_y : integer;
            constant left_bat   : integer;
            constant right_bat  : integer
        ) is
        begin
            ball_pos_x  <= std_logic_vector(to_unsigned(pos_ball_x, ball_pos_x'length));
            ball_pos_y  <= std_logic_vector(to_unsigned(pos_ball_y, ball_pos_y'length));
            left_bat_y  <= std_logic_vector(to_unsigned(left_bat, left_bat_y'length));
            right_bat_y <= std_logic_vector(to_unsigned(right_bat, right_bat_y'length));
            
            wait until rising_edge(pixel_clock);
            h_count := 1;
            v_count := 0;
            wait until rising_edge(pixel_clock);
            while (v_count < vertical_pixel_line) loop
                --Horizontal before and after sync
                wait until rising_edge(pixel_clock);
                if (h_count < horizontal_pixel_line) then
                    h_count := h_count + 1;
                else
                    h_count := 0;
    
                    if (v_count < vertical_pixel_line) then
                        v_count := v_count + 1;
                    else
                        v_count := 0;
                    end if;
                end if;
                if ((h_count < horizontal_before_sync) or (h_count > horizontal_after_sync)) then
                    
                    assert(h_sync = '1') report "H sync should be high ";
                    --We are in the screen
                    if (h_count < horizontal_pixels and v_count < vertical_pixels) then
                        
                        assert(v_sync = '1') report "V sync should be high ";
                        
                        if(v_count >= (right_bat - bat_height) )and (v_count <= (right_bat + bat_height) ) and 
                        (h_count >=  (right_bat_x - bat_width)) and (h_count <= (right_bat_x + bat_width)) then
                            
                            assert(write_r = all_ones) report "Pixel should be white ";
                            assert(write_g = all_ones) report "Pixel should be white ";
                            assert(write_b = all_ones) report "Pixel should be white ";
                            
                            --Left bat
                            elsif(v_count >= (left_bat - bat_height)) and (v_count <= (left_bat + bat_height)) and 
                            (h_count >=  (left_bat_x - bat_width)) and (h_count <= (left_bat_x + bat_width) )then
                                assert(write_r = all_ones) report "Pixel should be white ";
                                assert(write_g = all_ones) report "Pixel should be white ";
                                assert(write_b = all_ones) report "Pixel should be white ";
                                
                                
                                --Ball  x^2 + y^2 = ball_radius^2 equation of a circle
                            elsif((h_count - pos_ball_x)**2 + (v_count -pos_ball_y)**2 <= ball_radius**2) then
                                assert(write_r = all_ones) report "Pixel should be white ";
                                assert(write_g = all_ones) report "Pixel should be white ";
                                assert(write_b = all_ones) report "Pixel should be white ";
                                    
                                    
                                    --Draw set every other pixel to zero
                            else
                                assert(write_r = all_zeros) report "Pixel should be black ";
                                assert(write_g = all_zeros) report "Pixel should be black ";
                                assert(write_b = all_zeros) report "Pixel should be black ";

                        end if;

                    elsif ((v_count < vertical_before_sync) or (v_count > vertical_after_sync)) then
    
                        assert(v_sync = '1') report "V sync should be high ";
    
                    else
    
                        assert(v_sync = '0') report "V sync should be low ";
    
                    end if;
                    --In the horizontal sync area
                else
                    assert(h_sync = '0') report "h sync should be low ";
    
                    if ((v_count < vertical_before_sync) or (v_count > vertical_after_sync)) then
    
                        assert(v_sync = '1') report "V sync should be high ";
    
                    else
    
                        assert(v_sync = '0') report "V sync should be low ";
    
                    end if;
                end if;
                
            end loop;
    

        end procedure testcase;
    begin
        reset <= '1';
        wait until rising_edge(pixel_clock);
        reset <= '0';
        testcase(400, 400, 200, 200);

        
        finish;

    end process main_p;
end rtl;
