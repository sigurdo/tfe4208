	FRESH_SOPC u0 (
		.clk_clk                              (<connected-to-clk_clk>),                              //                           clk.clk
		.reset_reset_n                        (<connected-to-reset_reset_n>),                        //                         reset.reset_n
		.store_ball_position_0_conduit_ball_x (<connected-to-store_ball_position_0_conduit_ball_x>), // store_ball_position_0_conduit.ball_x
		.store_ball_position_0_conduit_ball_y (<connected-to-store_ball_position_0_conduit_ball_y>)  //                              .ball_y
	);

