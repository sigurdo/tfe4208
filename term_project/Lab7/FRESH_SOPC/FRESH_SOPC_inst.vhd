	component FRESH_SOPC is
		port (
			clk_clk                              : in  std_logic                     := 'X'; -- clk
			reset_reset_n                        : in  std_logic                     := 'X'; -- reset_n
			store_ball_position_0_conduit_ball_x : out std_logic_vector(31 downto 0);        -- ball_x
			store_ball_position_0_conduit_ball_y : out std_logic_vector(31 downto 0)         -- ball_y
		);
	end component FRESH_SOPC;

	u0 : component FRESH_SOPC
		port map (
			clk_clk                              => CONNECTED_TO_clk_clk,                              --                           clk.clk
			reset_reset_n                        => CONNECTED_TO_reset_reset_n,                        --                         reset.reset_n
			store_ball_position_0_conduit_ball_x => CONNECTED_TO_store_ball_position_0_conduit_ball_x, -- store_ball_position_0_conduit.ball_x
			store_ball_position_0_conduit_ball_y => CONNECTED_TO_store_ball_position_0_conduit_ball_y  --                              .ball_y
		);

