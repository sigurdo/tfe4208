-- Copyright (C) 2018  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 18.1.0 Build 625 09/12/2018 SJ Lite Edition"

-- DATE "03/18/2022 08:29:21"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim (VHDL) only
-- 

LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_ASDO_DATA1~	=>  Location: PIN_F4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_FLASH_nCE_nCSO~	=>  Location: PIN_E2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DCLK~	=>  Location: PIN_P3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DATA0~	=>  Location: PIN_N7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_nCEO~	=>  Location: PIN_P28,	 I/O Standard: 2.5 V,	 Current Strength: 8mA


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~padout\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~padout\ : std_logic;
SIGNAL \~ALTERA_DATA0~~padout\ : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_DATA0~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	Lab7 IS
    PORT (
	VGA_CLK : OUT std_logic;
	CLOCK_50 : IN std_logic;
	VGA_BLANK_N : OUT std_logic;
	VGA_HS : OUT std_logic;
	VGA_VS : OUT std_logic;
	VGA_SYNC_N : OUT std_logic;
	VGA_B : OUT std_logic_vector(7 DOWNTO 0);
	VGA_G : OUT std_logic_vector(7 DOWNTO 0);
	VGA_R : OUT std_logic_vector(7 DOWNTO 0)
	);
END Lab7;

-- Design Ports Information
-- VGA_CLK	=>  Location: PIN_A12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_BLANK_N	=>  Location: PIN_F11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_HS	=>  Location: PIN_G13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_VS	=>  Location: PIN_C13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_SYNC_N	=>  Location: PIN_C10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_B[7]	=>  Location: PIN_D12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_B[6]	=>  Location: PIN_D11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_B[5]	=>  Location: PIN_C12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_B[4]	=>  Location: PIN_A11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_B[3]	=>  Location: PIN_B11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_B[2]	=>  Location: PIN_C11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_B[1]	=>  Location: PIN_A10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_B[0]	=>  Location: PIN_B10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_G[7]	=>  Location: PIN_C9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_G[6]	=>  Location: PIN_F10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_G[5]	=>  Location: PIN_B8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_G[4]	=>  Location: PIN_C8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_G[3]	=>  Location: PIN_H12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_G[2]	=>  Location: PIN_F8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_G[1]	=>  Location: PIN_G11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_G[0]	=>  Location: PIN_G8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_R[7]	=>  Location: PIN_H10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_R[6]	=>  Location: PIN_H8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_R[5]	=>  Location: PIN_J12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_R[4]	=>  Location: PIN_G10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_R[3]	=>  Location: PIN_F12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_R[2]	=>  Location: PIN_D10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_R[1]	=>  Location: PIN_E11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- VGA_R[0]	=>  Location: PIN_E12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- CLOCK_50	=>  Location: PIN_Y2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default


ARCHITECTURE structure OF Lab7 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_VGA_CLK : std_logic;
SIGNAL ww_CLOCK_50 : std_logic;
SIGNAL ww_VGA_BLANK_N : std_logic;
SIGNAL ww_VGA_HS : std_logic;
SIGNAL ww_VGA_VS : std_logic;
SIGNAL ww_VGA_SYNC_N : std_logic;
SIGNAL ww_VGA_B : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_VGA_G : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_VGA_R : std_logic_vector(7 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a91_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a91_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a91_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a91_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a89_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a89_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a89_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a89_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a90_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a90_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a90_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a90_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a88_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a88_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a88_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a88_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a83_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a83_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a83_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a83_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a81_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a81_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a81_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a81_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a82_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a82_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a82_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a82_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a80_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a80_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a80_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a80_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a95_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a95_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a95_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a95_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a93_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a93_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a93_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a93_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a94_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a94_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a94_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a94_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a92_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a92_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a92_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a92_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a85_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a85_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a85_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a85_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a84_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a84_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a84_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a84_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a87_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a87_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a87_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a87_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a86_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a86_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a86_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a86_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a75_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a75_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a75_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a75_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a73_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a73_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a73_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a73_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a74_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a74_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a74_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a74_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a72_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a72_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a72_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a72_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a67_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a67_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a67_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a67_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a65_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a65_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a65_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a65_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a66_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a66_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a66_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a66_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a64_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a64_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a64_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a64_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a79_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a79_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a79_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a79_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a77_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a77_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a77_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a77_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a78_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a78_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a78_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a78_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a76_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a76_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a76_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a76_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a69_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a69_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a69_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a69_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a68_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a68_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a68_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a68_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a71_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a71_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a71_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a71_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a70_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a70_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a70_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a70_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a53_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a53_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a53_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a53_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a52_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a52_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a52_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a52_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a55_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a55_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a55_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a55_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a54_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a54_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a54_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a54_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a62_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a62_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a62_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a62_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a60_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a60_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a60_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a60_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a63_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a63_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a63_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a63_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a61_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a61_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a61_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a61_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a51_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a51_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a51_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a51_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a49_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a49_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a49_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a49_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a50_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a50_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a50_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a50_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a48_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a48_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a48_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a48_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a59_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a59_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a59_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a59_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a57_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a57_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a57_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a57_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a58_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a58_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a58_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a58_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a56_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a56_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a56_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a56_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a27_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a27_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a27_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a27_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a25_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a25_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a25_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a25_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a26_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a26_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a26_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a26_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a24_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a24_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a24_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a24_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a19_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a19_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a19_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a19_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a17_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a17_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a17_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a17_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a18_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a18_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a18_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a18_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a16_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a16_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a16_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a16_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a31_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a31_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a31_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a31_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a29_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a29_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a29_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a29_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a30_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a30_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a30_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a30_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a28_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a28_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a28_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a28_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a21_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a21_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a21_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a21_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a20_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a20_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a20_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a20_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a23_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a23_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a23_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a23_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a22_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a22_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a22_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a22_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a7_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a7_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a7_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a7_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a5_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a5_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a5_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a5_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a6_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a6_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a6_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a6_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a4_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a4_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a4_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a4_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a11_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a11_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a11_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a11_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a9_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a9_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a9_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a9_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a10_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a10_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a10_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a10_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a8_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a8_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a8_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a8_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a3_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a3_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a3_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a3_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a1_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a1_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a1_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a1_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a2_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a2_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a2_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a2_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a0_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a0_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a0_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a14_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a14_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a14_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a14_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a12_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a12_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a12_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a12_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a15_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a15_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a15_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a15_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a13_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a13_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a13_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a13_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a43_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a43_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a43_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a43_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a41_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a41_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a41_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a41_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a42_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a42_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a42_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a42_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a40_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a40_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a40_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a40_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a35_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a35_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a35_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a35_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a33_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a33_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a33_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a33_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a34_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a34_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a34_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a34_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a32_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a32_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a32_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a32_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a47_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a47_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a47_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a47_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a45_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a45_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a45_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a45_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a46_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a46_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a46_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a46_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a44_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a44_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a44_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a44_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a39_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a39_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a39_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a39_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a37_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a37_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a37_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a37_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a38_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a38_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a38_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a38_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a36_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a36_PORTAADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a36_PORTBADDR_bus\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a36_PORTBDATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \CLOCK_50~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \VGA_CLK~output_o\ : std_logic;
SIGNAL \VGA_BLANK_N~output_o\ : std_logic;
SIGNAL \VGA_HS~output_o\ : std_logic;
SIGNAL \VGA_VS~output_o\ : std_logic;
SIGNAL \VGA_SYNC_N~output_o\ : std_logic;
SIGNAL \VGA_B[7]~output_o\ : std_logic;
SIGNAL \VGA_B[6]~output_o\ : std_logic;
SIGNAL \VGA_B[5]~output_o\ : std_logic;
SIGNAL \VGA_B[4]~output_o\ : std_logic;
SIGNAL \VGA_B[3]~output_o\ : std_logic;
SIGNAL \VGA_B[2]~output_o\ : std_logic;
SIGNAL \VGA_B[1]~output_o\ : std_logic;
SIGNAL \VGA_B[0]~output_o\ : std_logic;
SIGNAL \VGA_G[7]~output_o\ : std_logic;
SIGNAL \VGA_G[6]~output_o\ : std_logic;
SIGNAL \VGA_G[5]~output_o\ : std_logic;
SIGNAL \VGA_G[4]~output_o\ : std_logic;
SIGNAL \VGA_G[3]~output_o\ : std_logic;
SIGNAL \VGA_G[2]~output_o\ : std_logic;
SIGNAL \VGA_G[1]~output_o\ : std_logic;
SIGNAL \VGA_G[0]~output_o\ : std_logic;
SIGNAL \VGA_R[7]~output_o\ : std_logic;
SIGNAL \VGA_R[6]~output_o\ : std_logic;
SIGNAL \VGA_R[5]~output_o\ : std_logic;
SIGNAL \VGA_R[4]~output_o\ : std_logic;
SIGNAL \VGA_R[3]~output_o\ : std_logic;
SIGNAL \VGA_R[2]~output_o\ : std_logic;
SIGNAL \VGA_R[1]~output_o\ : std_logic;
SIGNAL \VGA_R[0]~output_o\ : std_logic;
SIGNAL \CLOCK_50~input_o\ : std_logic;
SIGNAL \CLOCK_50~inputclkctrl_outclk\ : std_logic;
SIGNAL \inst1|Add0~0_combout\ : std_logic;
SIGNAL \inst1|horizontal_current_count~11_combout\ : std_logic;
SIGNAL \inst1|Add0~1\ : std_logic;
SIGNAL \inst1|Add0~2_combout\ : std_logic;
SIGNAL \inst1|horizontal_current_count~4_combout\ : std_logic;
SIGNAL \inst1|Add0~3\ : std_logic;
SIGNAL \inst1|Add0~4_combout\ : std_logic;
SIGNAL \inst1|horizontal_current_count~10_combout\ : std_logic;
SIGNAL \inst1|Add0~5\ : std_logic;
SIGNAL \inst1|Add0~6_combout\ : std_logic;
SIGNAL \inst1|horizontal_current_count~3_combout\ : std_logic;
SIGNAL \inst1|Add0~7\ : std_logic;
SIGNAL \inst1|Add0~8_combout\ : std_logic;
SIGNAL \inst1|horizontal_current_count~5_combout\ : std_logic;
SIGNAL \inst1|Add0~9\ : std_logic;
SIGNAL \inst1|Add0~10_combout\ : std_logic;
SIGNAL \inst1|horizontal_current_count~7_combout\ : std_logic;
SIGNAL \inst1|Add0~11\ : std_logic;
SIGNAL \inst1|Add0~12_combout\ : std_logic;
SIGNAL \inst1|horizontal_current_count~8_combout\ : std_logic;
SIGNAL \inst1|Add0~13\ : std_logic;
SIGNAL \inst1|Add0~14_combout\ : std_logic;
SIGNAL \inst1|horizontal_current_count~6_combout\ : std_logic;
SIGNAL \inst1|Add0~15\ : std_logic;
SIGNAL \inst1|Add0~17\ : std_logic;
SIGNAL \inst1|Add0~19\ : std_logic;
SIGNAL \inst1|Add0~20_combout\ : std_logic;
SIGNAL \inst1|horizontal_current_count~12_combout\ : std_logic;
SIGNAL \inst1|Add0~16_combout\ : std_logic;
SIGNAL \inst1|horizontal_current_count~9_combout\ : std_logic;
SIGNAL \inst1|LessThan0~0_combout\ : std_logic;
SIGNAL \inst1|Add0~18_combout\ : std_logic;
SIGNAL \inst1|horizontal_current_count~2_combout\ : std_logic;
SIGNAL \inst1|process_0~3_combout\ : std_logic;
SIGNAL \inst1|process_0~4_combout\ : std_logic;
SIGNAL \inst1|LessThan0~1_combout\ : std_logic;
SIGNAL \inst1|process_0~5_combout\ : std_logic;
SIGNAL \inst1|process_0~0_combout\ : std_logic;
SIGNAL \inst1|process_0~1_combout\ : std_logic;
SIGNAL \inst1|process_0~2_combout\ : std_logic;
SIGNAL \inst1|process_0~6_combout\ : std_logic;
SIGNAL \inst1|h_sync~q\ : std_logic;
SIGNAL \inst1|Add1~3_combout\ : std_logic;
SIGNAL \inst1|Add1~32_combout\ : std_logic;
SIGNAL \inst1|vertical_current_count[0]~feeder_combout\ : std_logic;
SIGNAL \inst1|Add1~4\ : std_logic;
SIGNAL \inst1|Add1~5_combout\ : std_logic;
SIGNAL \inst1|Add1~30_combout\ : std_logic;
SIGNAL \inst1|Add1~6\ : std_logic;
SIGNAL \inst1|Add1~7_combout\ : std_logic;
SIGNAL \inst1|Add1~31_combout\ : std_logic;
SIGNAL \inst1|Add1~8\ : std_logic;
SIGNAL \inst1|Add1~9_combout\ : std_logic;
SIGNAL \inst1|Add1~29_combout\ : std_logic;
SIGNAL \inst1|Add1~10\ : std_logic;
SIGNAL \inst1|Add1~11_combout\ : std_logic;
SIGNAL \inst1|Add1~27_combout\ : std_logic;
SIGNAL \inst1|Add1~12\ : std_logic;
SIGNAL \inst1|Add1~13_combout\ : std_logic;
SIGNAL \inst1|Add1~28_combout\ : std_logic;
SIGNAL \inst1|Add1~14\ : std_logic;
SIGNAL \inst1|Add1~15_combout\ : std_logic;
SIGNAL \inst1|Add1~26_combout\ : std_logic;
SIGNAL \inst1|Add1~16\ : std_logic;
SIGNAL \inst1|Add1~18\ : std_logic;
SIGNAL \inst1|Add1~20_combout\ : std_logic;
SIGNAL \inst1|Add1~22_combout\ : std_logic;
SIGNAL \inst1|Add1~21\ : std_logic;
SIGNAL \inst1|Add1~23_combout\ : std_logic;
SIGNAL \inst1|Add1~25_combout\ : std_logic;
SIGNAL \inst1|Add1~0_combout\ : std_logic;
SIGNAL \inst1|Add1~1_combout\ : std_logic;
SIGNAL \inst1|Add1~2_combout\ : std_logic;
SIGNAL \inst1|Add1~17_combout\ : std_logic;
SIGNAL \inst1|Add1~19_combout\ : std_logic;
SIGNAL \inst1|process_0~7_combout\ : std_logic;
SIGNAL \inst1|process_0~8_combout\ : std_logic;
SIGNAL \inst1|process_0~9_combout\ : std_logic;
SIGNAL \inst1|v_sync~q\ : std_logic;
SIGNAL \inst7|Add1~0_combout\ : std_logic;
SIGNAL \inst1|process_0~10_combout\ : std_logic;
SIGNAL \inst1|frame_buff_en~q\ : std_logic;
SIGNAL \inst7|Add1~1\ : std_logic;
SIGNAL \inst7|Add1~2_combout\ : std_logic;
SIGNAL \inst7|Add1~3\ : std_logic;
SIGNAL \inst7|Add1~4_combout\ : std_logic;
SIGNAL \inst7|Add1~5\ : std_logic;
SIGNAL \inst7|Add1~6_combout\ : std_logic;
SIGNAL \inst7|Add1~7\ : std_logic;
SIGNAL \inst7|Add1~8_combout\ : std_logic;
SIGNAL \inst7|Add1~9\ : std_logic;
SIGNAL \inst7|Add1~10_combout\ : std_logic;
SIGNAL \inst7|Add1~11\ : std_logic;
SIGNAL \inst7|Add1~12_combout\ : std_logic;
SIGNAL \inst7|Add1~13\ : std_logic;
SIGNAL \inst7|Add1~14_combout\ : std_logic;
SIGNAL \inst7|Add1~15\ : std_logic;
SIGNAL \inst7|Add1~16_combout\ : std_logic;
SIGNAL \inst7|Add1~17\ : std_logic;
SIGNAL \inst7|Add1~18_combout\ : std_logic;
SIGNAL \inst7|Add1~19\ : std_logic;
SIGNAL \inst7|Add1~20_combout\ : std_logic;
SIGNAL \inst7|Add1~21\ : std_logic;
SIGNAL \inst7|Add1~22_combout\ : std_logic;
SIGNAL \inst7|Add1~23\ : std_logic;
SIGNAL \inst7|Add1~24_combout\ : std_logic;
SIGNAL \inst7|Add1~25\ : std_logic;
SIGNAL \inst7|Add1~26_combout\ : std_logic;
SIGNAL \inst7|Add1~27\ : std_logic;
SIGNAL \inst7|Add1~28_combout\ : std_logic;
SIGNAL \inst7|Add1~29\ : std_logic;
SIGNAL \inst7|Add1~30_combout\ : std_logic;
SIGNAL \~GND~combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a5~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a7~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~52_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a6~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a4~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~53_combout\ : std_logic;
SIGNAL \inst7|Add1~31\ : std_logic;
SIGNAL \inst7|Add1~32_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~54_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a12~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a14~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~61_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a15~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a13~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~62_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~63_combout\ : std_logic;
SIGNAL \inst7|Add1~33\ : std_logic;
SIGNAL \inst7|Add1~34_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a3~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a1~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~58_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a2~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a0~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~59_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~5_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a10~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a8~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~56_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a11~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a9~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~55_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~57_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~60_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~78_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a47~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a45~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~70_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a32~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a34~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~68_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a33~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a35~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~67_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a43~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a41~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~64_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a42~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a40~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~65_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~66_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~69_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a44~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a46~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~71_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~72_combout\ : std_logic;
SIGNAL \inst7|Add1~35\ : std_logic;
SIGNAL \inst7|Add1~37\ : std_logic;
SIGNAL \inst7|Add1~38_combout\ : std_logic;
SIGNAL \inst7|read_address~1_combout\ : std_logic;
SIGNAL \inst7|Equal0~0_combout\ : std_logic;
SIGNAL \inst7|Equal0~5_combout\ : std_logic;
SIGNAL \inst7|Equal0~4_combout\ : std_logic;
SIGNAL \inst7|Equal0~2_combout\ : std_logic;
SIGNAL \inst7|Equal0~1_combout\ : std_logic;
SIGNAL \inst7|Equal0~3_combout\ : std_logic;
SIGNAL \inst7|Equal0~6_combout\ : std_logic;
SIGNAL \inst7|Add1~36_combout\ : std_logic;
SIGNAL \inst7|read_address~0_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a38~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a36~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~74_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a37~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a39~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~73_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~12_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~75_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~79_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a20~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a21~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~49_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a22~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a23~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~50_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a29~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a31~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~46_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a30~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a28~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~47_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a26~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a24~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~41_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a25~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a27~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~40_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~42_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a19~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a17~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~43_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a16~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a18~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~44_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~45_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~48_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~51_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a52~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a53~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~28_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a54~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a55~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~29_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~30_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a62~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a60~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~31_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a63~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a61~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~32_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~33_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a56~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a58~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~37_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a59~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a57~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~36_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~38_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a48~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a50~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~35_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a51~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a49~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~34_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~77_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~39_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~76_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a68~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a69~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~25_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a79~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a77~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~22_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a73~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a75~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~16_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a72~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a74~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~17_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~18_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a66~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a64~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~20_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a65~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a67~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~19_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~21_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a78~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a76~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~23_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~24_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a70~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a71~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~26_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~27_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a84~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a85~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~13_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a95~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a93~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~9_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a94~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a92~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~10_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a91~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a89~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~2_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a90~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a88~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~3_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~4_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a82~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a80~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~7_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a83~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a81~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~6_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~8_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~11_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a86~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a87~portbdataout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~14_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~15_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~0_combout\ : std_logic;
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\ : std_logic;
SIGNAL \inst7|read_address\ : std_logic_vector(19 DOWNTO 0);
SIGNAL \inst1|vertical_current_count\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \inst1|horizontal_current_count\ : std_logic_vector(10 DOWNTO 0);
SIGNAL \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\ : std_logic_vector(6 DOWNTO 0);

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

VGA_CLK <= ww_VGA_CLK;
ww_CLOCK_50 <= CLOCK_50;
VGA_BLANK_N <= ww_VGA_BLANK_N;
VGA_HS <= ww_VGA_HS;
VGA_VS <= ww_VGA_VS;
VGA_SYNC_N <= ww_VGA_SYNC_N;
VGA_B <= ww_VGA_B;
VGA_G <= ww_VGA_G;
VGA_R <= ww_VGA_R;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a91_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a91_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a91_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a91~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a91_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a89_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a89_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a89_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a89~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a89_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a90_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a90_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a90_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a90~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a90_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a88_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a88_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a88_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a88~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a88_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a83_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a83_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a83_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a83~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a83_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a81_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a81_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a81_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a81~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a81_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a82_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a82_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a82_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a82~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a82_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a80_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a80_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a80_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a80~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a80_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a95_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a95_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a95_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a95~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a95_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a93_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a93_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a93_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a93~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a93_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a94_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a94_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a94_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a94~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a94_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a92_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a92_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a92_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a92~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a92_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a85_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a85_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a85_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a85~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a85_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a84_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a84_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a84_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a84~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a84_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a87_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a87_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a87_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a87~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a87_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a86_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a86_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a86_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a86~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a86_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a75_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a75_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a75_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a75~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a75_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a73_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a73_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a73_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a73~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a73_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a74_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a74_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a74_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a74~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a74_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a72_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a72_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a72_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a72~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a72_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a67_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a67_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a67_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a67~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a67_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a65_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a65_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a65_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a65~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a65_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a66_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a66_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a66_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a66~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a66_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a64_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a64_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a64_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a64~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a64_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a79_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a79_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a79_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a79~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a79_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a77_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a77_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a77_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a77~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a77_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a78_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a78_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a78_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a78~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a78_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a76_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a76_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a76_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a76~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a76_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a69_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a69_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a69_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a69~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a69_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a68_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a68_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a68_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a68~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a68_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a71_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a71_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a71_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a71~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a71_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a70_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a70_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a70_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a70~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a70_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a53_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a53_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a53_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a53~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a53_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a52_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a52_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a52_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a52~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a52_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a55_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a55_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a55_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a55~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a55_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a54_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a54_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a54_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a54~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a54_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a62_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a62_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a62_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a62~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a62_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a60_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a60_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a60_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a60~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a60_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a63_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a63_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a63_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a63~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a63_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a61_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a61_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a61_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a61~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a61_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a51_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a51_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a51_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a51~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a51_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a49_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a49_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a49_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a49~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a49_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a50_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a50_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a50_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a50~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a50_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a48_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a48_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a48_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a48~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a48_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a59_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a59_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a59_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a59~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a59_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a57_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a57_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a57_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a57~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a57_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a58_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a58_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a58_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a58~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a58_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a56_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a56_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a56_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a56~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a56_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a27_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a27_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a27_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a27~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a27_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a25_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a25_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a25_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a25~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a25_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a26_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a26_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a26_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a26~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a26_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a24_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a24_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a24_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a24~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a24_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a19_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a19_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a19_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a19~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a19_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a17_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a17_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a17_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a17~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a17_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a18_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a18_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a18_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a18~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a18_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a16_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a16_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a16_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a16~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a16_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a31_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a31_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a31_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a31~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a31_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a29_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a29_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a29_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a29~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a29_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a30_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a30_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a30_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a30~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a30_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a28_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a28_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a28_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a28~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a28_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a21_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a21_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a21_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a21~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a21_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a20_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a20_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a20_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a20~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a20_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a23_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a23_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a23_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a23~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a23_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a22_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a22_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a22_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a22~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a22_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a7_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a7_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a7_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a7~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a7_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a5_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a5_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a5_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a5~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a5_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a6_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a6_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a6_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a6~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a6_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a4_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a4_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a4_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a4~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a4_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a11_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a11_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a11_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a11~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a11_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a9_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a9_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a9_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a9~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a9_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a10_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a10_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a10_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a10~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a10_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a8_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a8_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a8_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a8~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a8_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a3_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a3_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a3_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a3~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a3_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a1_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a1_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a1_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a1~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a1_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a2_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a2_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a2_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a2~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a2_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a0_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a0_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a0_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a0~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a14_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a14_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a14_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a14~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a14_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a12_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a12_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a12_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a12~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a12_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a15_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a15_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a15_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a15~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a15_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a13_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a13_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a13_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a13~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a13_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a43_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a43_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a43_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a43~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a43_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a41_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a41_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a41_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a41~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a41_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a42_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a42_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a42_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a42~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a42_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a40_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a40_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a40_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a40~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a40_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a35_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a35_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a35_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a35~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a35_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a33_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a33_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a33_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a33~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a33_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a34_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a34_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a34_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a34~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a34_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a32_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a32_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a32_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a32~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a32_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a47_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a47_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a47_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a47~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a47_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a45_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a45_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a45_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a45~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a45_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a46_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a46_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a46_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a46~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a46_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a44_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a44_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a44_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a44~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a44_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a39_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a39_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a39_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a39~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a39_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a37_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a37_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a37_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a37~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a37_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a38_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a38_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a38_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a38~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a38_PORTBDATAOUT_bus\(0);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a36_PORTADATAIN_bus\(0) <= vcc;

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a36_PORTAADDR_bus\ <= (\~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & \~GND~combout\ & 
\~GND~combout\ & \~GND~combout\ & vcc);

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a36_PORTBADDR_bus\ <= (\inst7|read_address\(12) & \inst7|read_address\(11) & \inst7|read_address\(10) & \inst7|read_address\(9) & \inst7|read_address\(8) & \inst7|read_address\(7) & 
\inst7|read_address\(6) & \inst7|read_address\(5) & \inst7|read_address\(4) & \inst7|read_address\(3) & \inst7|read_address\(2) & \inst7|read_address\(1) & \inst7|read_address\(0));

\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a36~portbdataout\ <= \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a36_PORTBDATAOUT_bus\(0);

\CLOCK_50~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \CLOCK_50~input_o\);
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: IOOBUF_X47_Y73_N2
\VGA_CLK~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \CLOCK_50~input_o\,
	devoe => ww_devoe,
	o => \VGA_CLK~output_o\);

-- Location: IOOBUF_X31_Y73_N9
\VGA_BLANK_N~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \VGA_BLANK_N~output_o\);

-- Location: IOOBUF_X38_Y73_N16
\VGA_HS~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst1|h_sync~q\,
	devoe => ww_devoe,
	o => \VGA_HS~output_o\);

-- Location: IOOBUF_X54_Y73_N2
\VGA_VS~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst1|v_sync~q\,
	devoe => ww_devoe,
	o => \VGA_VS~output_o\);

-- Location: IOOBUF_X35_Y73_N16
\VGA_SYNC_N~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \VGA_SYNC_N~output_o\);

-- Location: IOOBUF_X52_Y73_N23
\VGA_B[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\,
	devoe => ww_devoe,
	o => \VGA_B[7]~output_o\);

-- Location: IOOBUF_X23_Y73_N9
\VGA_B[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\,
	devoe => ww_devoe,
	o => \VGA_B[6]~output_o\);

-- Location: IOOBUF_X52_Y73_N16
\VGA_B[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\,
	devoe => ww_devoe,
	o => \VGA_B[5]~output_o\);

-- Location: IOOBUF_X42_Y73_N2
\VGA_B[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\,
	devoe => ww_devoe,
	o => \VGA_B[4]~output_o\);

-- Location: IOOBUF_X42_Y73_N9
\VGA_B[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\,
	devoe => ww_devoe,
	o => \VGA_B[3]~output_o\);

-- Location: IOOBUF_X23_Y73_N2
\VGA_B[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\,
	devoe => ww_devoe,
	o => \VGA_B[2]~output_o\);

-- Location: IOOBUF_X38_Y73_N2
\VGA_B[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\,
	devoe => ww_devoe,
	o => \VGA_B[1]~output_o\);

-- Location: IOOBUF_X38_Y73_N9
\VGA_B[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\,
	devoe => ww_devoe,
	o => \VGA_B[0]~output_o\);

-- Location: IOOBUF_X23_Y73_N16
\VGA_G[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\,
	devoe => ww_devoe,
	o => \VGA_G[7]~output_o\);

-- Location: IOOBUF_X20_Y73_N2
\VGA_G[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\,
	devoe => ww_devoe,
	o => \VGA_G[6]~output_o\);

-- Location: IOOBUF_X16_Y73_N2
\VGA_G[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\,
	devoe => ww_devoe,
	o => \VGA_G[5]~output_o\);

-- Location: IOOBUF_X16_Y73_N9
\VGA_G[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\,
	devoe => ww_devoe,
	o => \VGA_G[4]~output_o\);

-- Location: IOOBUF_X25_Y73_N23
\VGA_G[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\,
	devoe => ww_devoe,
	o => \VGA_G[3]~output_o\);

-- Location: IOOBUF_X11_Y73_N9
\VGA_G[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\,
	devoe => ww_devoe,
	o => \VGA_G[2]~output_o\);

-- Location: IOOBUF_X25_Y73_N16
\VGA_G[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\,
	devoe => ww_devoe,
	o => \VGA_G[1]~output_o\);

-- Location: IOOBUF_X11_Y73_N16
\VGA_G[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\,
	devoe => ww_devoe,
	o => \VGA_G[0]~output_o\);

-- Location: IOOBUF_X20_Y73_N16
\VGA_R[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\,
	devoe => ww_devoe,
	o => \VGA_R[7]~output_o\);

-- Location: IOOBUF_X11_Y73_N23
\VGA_R[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\,
	devoe => ww_devoe,
	o => \VGA_R[6]~output_o\);

-- Location: IOOBUF_X40_Y73_N9
\VGA_R[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\,
	devoe => ww_devoe,
	o => \VGA_R[5]~output_o\);

-- Location: IOOBUF_X20_Y73_N9
\VGA_R[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\,
	devoe => ww_devoe,
	o => \VGA_R[4]~output_o\);

-- Location: IOOBUF_X33_Y73_N9
\VGA_R[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\,
	devoe => ww_devoe,
	o => \VGA_R[3]~output_o\);

-- Location: IOOBUF_X35_Y73_N23
\VGA_R[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\,
	devoe => ww_devoe,
	o => \VGA_R[2]~output_o\);

-- Location: IOOBUF_X31_Y73_N2
\VGA_R[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\,
	devoe => ww_devoe,
	o => \VGA_R[1]~output_o\);

-- Location: IOOBUF_X33_Y73_N2
\VGA_R[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\,
	devoe => ww_devoe,
	o => \VGA_R[0]~output_o\);

-- Location: IOIBUF_X0_Y36_N15
\CLOCK_50~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLOCK_50,
	o => \CLOCK_50~input_o\);

-- Location: CLKCTRL_G4
\CLOCK_50~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLOCK_50~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLOCK_50~inputclkctrl_outclk\);

-- Location: FF_X54_Y51_N27
\inst1|horizontal_current_count[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	asdata => \inst1|horizontal_current_count~2_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|horizontal_current_count\(9));

-- Location: LCCOMB_X53_Y51_N2
\inst1|Add0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add0~0_combout\ = \inst1|horizontal_current_count\(0) $ (VCC)
-- \inst1|Add0~1\ = CARRY(\inst1|horizontal_current_count\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst1|horizontal_current_count\(0),
	datad => VCC,
	combout => \inst1|Add0~0_combout\,
	cout => \inst1|Add0~1\);

-- Location: LCCOMB_X53_Y51_N0
\inst1|horizontal_current_count~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|horizontal_current_count~11_combout\ = (\inst1|Add0~0_combout\ & ((\inst1|LessThan0~0_combout\) # (!\inst1|horizontal_current_count\(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst1|horizontal_current_count\(10),
	datac => \inst1|LessThan0~0_combout\,
	datad => \inst1|Add0~0_combout\,
	combout => \inst1|horizontal_current_count~11_combout\);

-- Location: FF_X53_Y51_N1
\inst1|horizontal_current_count[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst1|horizontal_current_count~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|horizontal_current_count\(0));

-- Location: LCCOMB_X53_Y51_N4
\inst1|Add0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add0~2_combout\ = (\inst1|horizontal_current_count\(1) & (!\inst1|Add0~1\)) # (!\inst1|horizontal_current_count\(1) & ((\inst1|Add0~1\) # (GND)))
-- \inst1|Add0~3\ = CARRY((!\inst1|Add0~1\) # (!\inst1|horizontal_current_count\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst1|horizontal_current_count\(1),
	datad => VCC,
	cin => \inst1|Add0~1\,
	combout => \inst1|Add0~2_combout\,
	cout => \inst1|Add0~3\);

-- Location: LCCOMB_X54_Y51_N30
\inst1|horizontal_current_count~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|horizontal_current_count~4_combout\ = (\inst1|Add0~2_combout\ & ((\inst1|LessThan0~0_combout\) # (!\inst1|horizontal_current_count\(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|LessThan0~0_combout\,
	datab => \inst1|Add0~2_combout\,
	datad => \inst1|horizontal_current_count\(10),
	combout => \inst1|horizontal_current_count~4_combout\);

-- Location: FF_X53_Y51_N5
\inst1|horizontal_current_count[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	asdata => \inst1|horizontal_current_count~4_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|horizontal_current_count\(1));

-- Location: LCCOMB_X53_Y51_N6
\inst1|Add0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add0~4_combout\ = (\inst1|horizontal_current_count\(2) & (\inst1|Add0~3\ $ (GND))) # (!\inst1|horizontal_current_count\(2) & (!\inst1|Add0~3\ & VCC))
-- \inst1|Add0~5\ = CARRY((\inst1|horizontal_current_count\(2) & !\inst1|Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|horizontal_current_count\(2),
	datad => VCC,
	cin => \inst1|Add0~3\,
	combout => \inst1|Add0~4_combout\,
	cout => \inst1|Add0~5\);

-- Location: LCCOMB_X53_Y51_N30
\inst1|horizontal_current_count~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|horizontal_current_count~10_combout\ = (\inst1|Add0~4_combout\ & ((\inst1|LessThan0~0_combout\) # (!\inst1|horizontal_current_count\(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst1|horizontal_current_count\(10),
	datac => \inst1|LessThan0~0_combout\,
	datad => \inst1|Add0~4_combout\,
	combout => \inst1|horizontal_current_count~10_combout\);

-- Location: FF_X53_Y51_N31
\inst1|horizontal_current_count[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst1|horizontal_current_count~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|horizontal_current_count\(2));

-- Location: LCCOMB_X53_Y51_N8
\inst1|Add0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add0~6_combout\ = (\inst1|horizontal_current_count\(3) & (!\inst1|Add0~5\)) # (!\inst1|horizontal_current_count\(3) & ((\inst1|Add0~5\) # (GND)))
-- \inst1|Add0~7\ = CARRY((!\inst1|Add0~5\) # (!\inst1|horizontal_current_count\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst1|horizontal_current_count\(3),
	datad => VCC,
	cin => \inst1|Add0~5\,
	combout => \inst1|Add0~6_combout\,
	cout => \inst1|Add0~7\);

-- Location: LCCOMB_X53_Y51_N28
\inst1|horizontal_current_count~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|horizontal_current_count~3_combout\ = (\inst1|Add0~6_combout\ & ((\inst1|LessThan0~0_combout\) # (!\inst1|horizontal_current_count\(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000010110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|LessThan0~0_combout\,
	datab => \inst1|horizontal_current_count\(10),
	datac => \inst1|Add0~6_combout\,
	combout => \inst1|horizontal_current_count~3_combout\);

-- Location: FF_X53_Y51_N29
\inst1|horizontal_current_count[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst1|horizontal_current_count~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|horizontal_current_count\(3));

-- Location: LCCOMB_X53_Y51_N10
\inst1|Add0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add0~8_combout\ = (\inst1|horizontal_current_count\(4) & (\inst1|Add0~7\ $ (GND))) # (!\inst1|horizontal_current_count\(4) & (!\inst1|Add0~7\ & VCC))
-- \inst1|Add0~9\ = CARRY((\inst1|horizontal_current_count\(4) & !\inst1|Add0~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst1|horizontal_current_count\(4),
	datad => VCC,
	cin => \inst1|Add0~7\,
	combout => \inst1|Add0~8_combout\,
	cout => \inst1|Add0~9\);

-- Location: LCCOMB_X53_Y51_N24
\inst1|horizontal_current_count~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|horizontal_current_count~5_combout\ = (\inst1|Add0~8_combout\ & ((\inst1|LessThan0~0_combout\) # (!\inst1|horizontal_current_count\(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst1|horizontal_current_count\(10),
	datac => \inst1|LessThan0~0_combout\,
	datad => \inst1|Add0~8_combout\,
	combout => \inst1|horizontal_current_count~5_combout\);

-- Location: FF_X53_Y51_N25
\inst1|horizontal_current_count[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst1|horizontal_current_count~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|horizontal_current_count\(4));

-- Location: LCCOMB_X53_Y51_N12
\inst1|Add0~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add0~10_combout\ = (\inst1|horizontal_current_count\(5) & (!\inst1|Add0~9\)) # (!\inst1|horizontal_current_count\(5) & ((\inst1|Add0~9\) # (GND)))
-- \inst1|Add0~11\ = CARRY((!\inst1|Add0~9\) # (!\inst1|horizontal_current_count\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|horizontal_current_count\(5),
	datad => VCC,
	cin => \inst1|Add0~9\,
	combout => \inst1|Add0~10_combout\,
	cout => \inst1|Add0~11\);

-- Location: LCCOMB_X54_Y51_N2
\inst1|horizontal_current_count~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|horizontal_current_count~7_combout\ = (\inst1|Add0~10_combout\ & ((\inst1|LessThan0~0_combout\) # (!\inst1|horizontal_current_count\(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst1|LessThan0~0_combout\,
	datac => \inst1|horizontal_current_count\(10),
	datad => \inst1|Add0~10_combout\,
	combout => \inst1|horizontal_current_count~7_combout\);

-- Location: FF_X54_Y51_N15
\inst1|horizontal_current_count[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	asdata => \inst1|horizontal_current_count~7_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|horizontal_current_count\(5));

-- Location: LCCOMB_X53_Y51_N14
\inst1|Add0~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add0~12_combout\ = (\inst1|horizontal_current_count\(6) & (\inst1|Add0~11\ $ (GND))) # (!\inst1|horizontal_current_count\(6) & (!\inst1|Add0~11\ & VCC))
-- \inst1|Add0~13\ = CARRY((\inst1|horizontal_current_count\(6) & !\inst1|Add0~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|horizontal_current_count\(6),
	datad => VCC,
	cin => \inst1|Add0~11\,
	combout => \inst1|Add0~12_combout\,
	cout => \inst1|Add0~13\);

-- Location: LCCOMB_X54_Y51_N12
\inst1|horizontal_current_count~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|horizontal_current_count~8_combout\ = (\inst1|Add0~12_combout\ & ((\inst1|LessThan0~0_combout\) # (!\inst1|horizontal_current_count\(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst1|horizontal_current_count\(10),
	datac => \inst1|LessThan0~0_combout\,
	datad => \inst1|Add0~12_combout\,
	combout => \inst1|horizontal_current_count~8_combout\);

-- Location: FF_X54_Y51_N5
\inst1|horizontal_current_count[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	asdata => \inst1|horizontal_current_count~8_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|horizontal_current_count\(6));

-- Location: LCCOMB_X53_Y51_N16
\inst1|Add0~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add0~14_combout\ = (\inst1|horizontal_current_count\(7) & (!\inst1|Add0~13\)) # (!\inst1|horizontal_current_count\(7) & ((\inst1|Add0~13\) # (GND)))
-- \inst1|Add0~15\ = CARRY((!\inst1|Add0~13\) # (!\inst1|horizontal_current_count\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|horizontal_current_count\(7),
	datad => VCC,
	cin => \inst1|Add0~13\,
	combout => \inst1|Add0~14_combout\,
	cout => \inst1|Add0~15\);

-- Location: LCCOMB_X54_Y51_N4
\inst1|horizontal_current_count~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|horizontal_current_count~6_combout\ = (\inst1|Add0~14_combout\ & ((\inst1|LessThan0~0_combout\) # (!\inst1|horizontal_current_count\(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|LessThan0~0_combout\,
	datab => \inst1|horizontal_current_count\(10),
	datad => \inst1|Add0~14_combout\,
	combout => \inst1|horizontal_current_count~6_combout\);

-- Location: FF_X54_Y51_N31
\inst1|horizontal_current_count[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	asdata => \inst1|horizontal_current_count~6_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|horizontal_current_count\(7));

-- Location: LCCOMB_X53_Y51_N18
\inst1|Add0~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add0~16_combout\ = (\inst1|horizontal_current_count\(8) & (\inst1|Add0~15\ $ (GND))) # (!\inst1|horizontal_current_count\(8) & (!\inst1|Add0~15\ & VCC))
-- \inst1|Add0~17\ = CARRY((\inst1|horizontal_current_count\(8) & !\inst1|Add0~15\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst1|horizontal_current_count\(8),
	datad => VCC,
	cin => \inst1|Add0~15\,
	combout => \inst1|Add0~16_combout\,
	cout => \inst1|Add0~17\);

-- Location: LCCOMB_X53_Y51_N20
\inst1|Add0~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add0~18_combout\ = (\inst1|horizontal_current_count\(9) & (!\inst1|Add0~17\)) # (!\inst1|horizontal_current_count\(9) & ((\inst1|Add0~17\) # (GND)))
-- \inst1|Add0~19\ = CARRY((!\inst1|Add0~17\) # (!\inst1|horizontal_current_count\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst1|horizontal_current_count\(9),
	datad => VCC,
	cin => \inst1|Add0~17\,
	combout => \inst1|Add0~18_combout\,
	cout => \inst1|Add0~19\);

-- Location: LCCOMB_X53_Y51_N22
\inst1|Add0~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add0~20_combout\ = \inst1|horizontal_current_count\(10) $ (!\inst1|Add0~19\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001111000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst1|horizontal_current_count\(10),
	cin => \inst1|Add0~19\,
	combout => \inst1|Add0~20_combout\);

-- Location: LCCOMB_X54_Y51_N26
\inst1|horizontal_current_count~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|horizontal_current_count~12_combout\ = (\inst1|Add0~20_combout\ & ((\inst1|LessThan0~0_combout\) # (!\inst1|horizontal_current_count\(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|LessThan0~0_combout\,
	datab => \inst1|horizontal_current_count\(10),
	datad => \inst1|Add0~20_combout\,
	combout => \inst1|horizontal_current_count~12_combout\);

-- Location: FF_X54_Y51_N3
\inst1|horizontal_current_count[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	asdata => \inst1|horizontal_current_count~12_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|horizontal_current_count\(10));

-- Location: LCCOMB_X54_Y51_N28
\inst1|horizontal_current_count~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|horizontal_current_count~9_combout\ = (\inst1|Add0~16_combout\ & ((\inst1|LessThan0~0_combout\) # (!\inst1|horizontal_current_count\(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst1|horizontal_current_count\(10),
	datac => \inst1|LessThan0~0_combout\,
	datad => \inst1|Add0~16_combout\,
	combout => \inst1|horizontal_current_count~9_combout\);

-- Location: FF_X54_Y51_N29
\inst1|horizontal_current_count[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst1|horizontal_current_count~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|horizontal_current_count\(8));

-- Location: LCCOMB_X54_Y51_N20
\inst1|LessThan0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|LessThan0~0_combout\ = (!\inst1|horizontal_current_count\(9) & (((!\inst1|horizontal_current_count\(6) & !\inst1|horizontal_current_count\(7))) # (!\inst1|horizontal_current_count\(8))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000100010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|horizontal_current_count\(9),
	datab => \inst1|horizontal_current_count\(8),
	datac => \inst1|horizontal_current_count\(6),
	datad => \inst1|horizontal_current_count\(7),
	combout => \inst1|LessThan0~0_combout\);

-- Location: LCCOMB_X54_Y51_N14
\inst1|horizontal_current_count~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|horizontal_current_count~2_combout\ = (\inst1|Add0~18_combout\ & ((\inst1|LessThan0~0_combout\) # (!\inst1|horizontal_current_count\(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|LessThan0~0_combout\,
	datab => \inst1|horizontal_current_count\(10),
	datad => \inst1|Add0~18_combout\,
	combout => \inst1|horizontal_current_count~2_combout\);

-- Location: LCCOMB_X53_Y51_N26
\inst1|process_0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|process_0~3_combout\ = (((!\inst1|LessThan0~0_combout\ & \inst1|horizontal_current_count\(10))) # (!\inst1|Add0~8_combout\)) # (!\inst1|Add0~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|LessThan0~0_combout\,
	datab => \inst1|horizontal_current_count\(10),
	datac => \inst1|Add0~6_combout\,
	datad => \inst1|Add0~8_combout\,
	combout => \inst1|process_0~3_combout\);

-- Location: LCCOMB_X54_Y51_N6
\inst1|process_0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|process_0~4_combout\ = (\inst1|process_0~3_combout\ & (!\inst1|horizontal_current_count~7_combout\ & !\inst1|horizontal_current_count~8_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|process_0~3_combout\,
	datac => \inst1|horizontal_current_count~7_combout\,
	datad => \inst1|horizontal_current_count~8_combout\,
	combout => \inst1|process_0~4_combout\);

-- Location: LCCOMB_X54_Y51_N0
\inst1|LessThan0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|LessThan0~1_combout\ = (\inst1|LessThan0~0_combout\) # (!\inst1|horizontal_current_count\(10))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst1|horizontal_current_count\(10),
	datad => \inst1|LessThan0~0_combout\,
	combout => \inst1|LessThan0~1_combout\);

-- Location: LCCOMB_X54_Y51_N8
\inst1|process_0~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|process_0~5_combout\ = (\inst1|process_0~4_combout\ & (((\inst1|LessThan0~1_combout\ & \inst1|Add0~16_combout\)) # (!\inst1|horizontal_current_count~6_combout\))) # (!\inst1|process_0~4_combout\ & (\inst1|LessThan0~1_combout\ & 
-- (\inst1|Add0~16_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|process_0~4_combout\,
	datab => \inst1|LessThan0~1_combout\,
	datac => \inst1|Add0~16_combout\,
	datad => \inst1|horizontal_current_count~6_combout\,
	combout => \inst1|process_0~5_combout\);

-- Location: LCCOMB_X54_Y51_N10
\inst1|process_0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|process_0~0_combout\ = (\inst1|LessThan0~0_combout\ & (((\inst1|Add0~4_combout\) # (\inst1|Add0~0_combout\)))) # (!\inst1|LessThan0~0_combout\ & (!\inst1|horizontal_current_count\(10) & ((\inst1|Add0~4_combout\) # (\inst1|Add0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|LessThan0~0_combout\,
	datab => \inst1|horizontal_current_count\(10),
	datac => \inst1|Add0~4_combout\,
	datad => \inst1|Add0~0_combout\,
	combout => \inst1|process_0~0_combout\);

-- Location: LCCOMB_X54_Y51_N16
\inst1|process_0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|process_0~1_combout\ = (\inst1|process_0~0_combout\) # ((\inst1|horizontal_current_count~5_combout\) # ((\inst1|horizontal_current_count~4_combout\) # (\inst1|horizontal_current_count~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|process_0~0_combout\,
	datab => \inst1|horizontal_current_count~5_combout\,
	datac => \inst1|horizontal_current_count~4_combout\,
	datad => \inst1|horizontal_current_count~3_combout\,
	combout => \inst1|process_0~1_combout\);

-- Location: LCCOMB_X54_Y51_N18
\inst1|process_0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|process_0~2_combout\ = (\inst1|Add0~14_combout\ & ((\inst1|Add0~12_combout\) # ((\inst1|Add0~10_combout\ & \inst1|process_0~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|Add0~12_combout\,
	datab => \inst1|Add0~10_combout\,
	datac => \inst1|Add0~14_combout\,
	datad => \inst1|process_0~1_combout\,
	combout => \inst1|process_0~2_combout\);

-- Location: LCCOMB_X54_Y51_N24
\inst1|process_0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|process_0~6_combout\ = (\inst1|horizontal_current_count~2_combout\) # (((\inst1|process_0~5_combout\) # (\inst1|process_0~2_combout\)) # (!\inst1|horizontal_current_count~12_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|horizontal_current_count~2_combout\,
	datab => \inst1|horizontal_current_count~12_combout\,
	datac => \inst1|process_0~5_combout\,
	datad => \inst1|process_0~2_combout\,
	combout => \inst1|process_0~6_combout\);

-- Location: FF_X54_Y51_N25
\inst1|h_sync\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst1|process_0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|h_sync~q\);

-- Location: FF_X54_Y52_N11
\inst1|vertical_current_count[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst1|Add1~19_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|vertical_current_count\(7));

-- Location: LCCOMB_X55_Y52_N10
\inst1|Add1~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add1~3_combout\ = \inst1|vertical_current_count\(0) $ (VCC)
-- \inst1|Add1~4\ = CARRY(\inst1|vertical_current_count\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst1|vertical_current_count\(0),
	datad => VCC,
	combout => \inst1|Add1~3_combout\,
	cout => \inst1|Add1~4\);

-- Location: LCCOMB_X54_Y52_N2
\inst1|Add1~32\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add1~32_combout\ = (\inst1|vertical_current_count\(0) & ((\inst1|LessThan0~1_combout\) # ((\inst1|Add1~3_combout\ & \inst1|Add1~2_combout\)))) # (!\inst1|vertical_current_count\(0) & (((\inst1|Add1~3_combout\ & \inst1|Add1~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|vertical_current_count\(0),
	datab => \inst1|LessThan0~1_combout\,
	datac => \inst1|Add1~3_combout\,
	datad => \inst1|Add1~2_combout\,
	combout => \inst1|Add1~32_combout\);

-- Location: LCCOMB_X55_Y52_N4
\inst1|vertical_current_count[0]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|vertical_current_count[0]~feeder_combout\ = \inst1|Add1~32_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add1~32_combout\,
	combout => \inst1|vertical_current_count[0]~feeder_combout\);

-- Location: FF_X55_Y52_N5
\inst1|vertical_current_count[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst1|vertical_current_count[0]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|vertical_current_count\(0));

-- Location: LCCOMB_X55_Y52_N12
\inst1|Add1~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add1~5_combout\ = (\inst1|vertical_current_count\(1) & (!\inst1|Add1~4\)) # (!\inst1|vertical_current_count\(1) & ((\inst1|Add1~4\) # (GND)))
-- \inst1|Add1~6\ = CARRY((!\inst1|Add1~4\) # (!\inst1|vertical_current_count\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst1|vertical_current_count\(1),
	datad => VCC,
	cin => \inst1|Add1~4\,
	combout => \inst1|Add1~5_combout\,
	cout => \inst1|Add1~6\);

-- Location: LCCOMB_X54_Y52_N6
\inst1|Add1~30\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add1~30_combout\ = (\inst1|Add1~5_combout\ & ((\inst1|Add1~2_combout\) # ((\inst1|LessThan0~1_combout\ & \inst1|vertical_current_count\(1))))) # (!\inst1|Add1~5_combout\ & (\inst1|LessThan0~1_combout\ & (\inst1|vertical_current_count\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|Add1~5_combout\,
	datab => \inst1|LessThan0~1_combout\,
	datac => \inst1|vertical_current_count\(1),
	datad => \inst1|Add1~2_combout\,
	combout => \inst1|Add1~30_combout\);

-- Location: FF_X54_Y52_N7
\inst1|vertical_current_count[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst1|Add1~30_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|vertical_current_count\(1));

-- Location: LCCOMB_X55_Y52_N14
\inst1|Add1~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add1~7_combout\ = (\inst1|vertical_current_count\(2) & (\inst1|Add1~6\ $ (GND))) # (!\inst1|vertical_current_count\(2) & (!\inst1|Add1~6\ & VCC))
-- \inst1|Add1~8\ = CARRY((\inst1|vertical_current_count\(2) & !\inst1|Add1~6\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst1|vertical_current_count\(2),
	datad => VCC,
	cin => \inst1|Add1~6\,
	combout => \inst1|Add1~7_combout\,
	cout => \inst1|Add1~8\);

-- Location: LCCOMB_X54_Y52_N0
\inst1|Add1~31\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add1~31_combout\ = (\inst1|vertical_current_count\(2) & ((\inst1|LessThan0~1_combout\) # ((\inst1|Add1~7_combout\ & \inst1|Add1~2_combout\)))) # (!\inst1|vertical_current_count\(2) & (((\inst1|Add1~7_combout\ & \inst1|Add1~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|vertical_current_count\(2),
	datab => \inst1|LessThan0~1_combout\,
	datac => \inst1|Add1~7_combout\,
	datad => \inst1|Add1~2_combout\,
	combout => \inst1|Add1~31_combout\);

-- Location: FF_X54_Y52_N29
\inst1|vertical_current_count[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	asdata => \inst1|Add1~31_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|vertical_current_count\(2));

-- Location: LCCOMB_X55_Y52_N16
\inst1|Add1~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add1~9_combout\ = (\inst1|vertical_current_count\(3) & (!\inst1|Add1~8\)) # (!\inst1|vertical_current_count\(3) & ((\inst1|Add1~8\) # (GND)))
-- \inst1|Add1~10\ = CARRY((!\inst1|Add1~8\) # (!\inst1|vertical_current_count\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst1|vertical_current_count\(3),
	datad => VCC,
	cin => \inst1|Add1~8\,
	combout => \inst1|Add1~9_combout\,
	cout => \inst1|Add1~10\);

-- Location: LCCOMB_X54_Y52_N24
\inst1|Add1~29\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add1~29_combout\ = (\inst1|LessThan0~1_combout\ & ((\inst1|vertical_current_count\(3)) # ((\inst1|Add1~9_combout\ & \inst1|Add1~2_combout\)))) # (!\inst1|LessThan0~1_combout\ & (\inst1|Add1~9_combout\ & ((\inst1|Add1~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|LessThan0~1_combout\,
	datab => \inst1|Add1~9_combout\,
	datac => \inst1|vertical_current_count\(3),
	datad => \inst1|Add1~2_combout\,
	combout => \inst1|Add1~29_combout\);

-- Location: FF_X54_Y52_N25
\inst1|vertical_current_count[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst1|Add1~29_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|vertical_current_count\(3));

-- Location: LCCOMB_X55_Y52_N18
\inst1|Add1~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add1~11_combout\ = (\inst1|vertical_current_count\(4) & (\inst1|Add1~10\ $ (GND))) # (!\inst1|vertical_current_count\(4) & (!\inst1|Add1~10\ & VCC))
-- \inst1|Add1~12\ = CARRY((\inst1|vertical_current_count\(4) & !\inst1|Add1~10\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst1|vertical_current_count\(4),
	datad => VCC,
	cin => \inst1|Add1~10\,
	combout => \inst1|Add1~11_combout\,
	cout => \inst1|Add1~12\);

-- Location: LCCOMB_X54_Y52_N22
\inst1|Add1~27\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add1~27_combout\ = (\inst1|LessThan0~1_combout\ & ((\inst1|vertical_current_count\(4)) # ((\inst1|Add1~11_combout\ & \inst1|Add1~2_combout\)))) # (!\inst1|LessThan0~1_combout\ & (\inst1|Add1~11_combout\ & ((\inst1|Add1~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|LessThan0~1_combout\,
	datab => \inst1|Add1~11_combout\,
	datac => \inst1|vertical_current_count\(4),
	datad => \inst1|Add1~2_combout\,
	combout => \inst1|Add1~27_combout\);

-- Location: FF_X54_Y52_N23
\inst1|vertical_current_count[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst1|Add1~27_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|vertical_current_count\(4));

-- Location: LCCOMB_X55_Y52_N20
\inst1|Add1~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add1~13_combout\ = (\inst1|vertical_current_count\(5) & (!\inst1|Add1~12\)) # (!\inst1|vertical_current_count\(5) & ((\inst1|Add1~12\) # (GND)))
-- \inst1|Add1~14\ = CARRY((!\inst1|Add1~12\) # (!\inst1|vertical_current_count\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|vertical_current_count\(5),
	datad => VCC,
	cin => \inst1|Add1~12\,
	combout => \inst1|Add1~13_combout\,
	cout => \inst1|Add1~14\);

-- Location: LCCOMB_X54_Y52_N26
\inst1|Add1~28\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add1~28_combout\ = (\inst1|LessThan0~1_combout\ & ((\inst1|vertical_current_count\(5)) # ((\inst1|Add1~2_combout\ & \inst1|Add1~13_combout\)))) # (!\inst1|LessThan0~1_combout\ & (\inst1|Add1~2_combout\ & ((\inst1|Add1~13_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|LessThan0~1_combout\,
	datab => \inst1|Add1~2_combout\,
	datac => \inst1|vertical_current_count\(5),
	datad => \inst1|Add1~13_combout\,
	combout => \inst1|Add1~28_combout\);

-- Location: FF_X54_Y52_N27
\inst1|vertical_current_count[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst1|Add1~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|vertical_current_count\(5));

-- Location: LCCOMB_X55_Y52_N22
\inst1|Add1~15\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add1~15_combout\ = (\inst1|vertical_current_count\(6) & (\inst1|Add1~14\ $ (GND))) # (!\inst1|vertical_current_count\(6) & (!\inst1|Add1~14\ & VCC))
-- \inst1|Add1~16\ = CARRY((\inst1|vertical_current_count\(6) & !\inst1|Add1~14\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|vertical_current_count\(6),
	datad => VCC,
	cin => \inst1|Add1~14\,
	combout => \inst1|Add1~15_combout\,
	cout => \inst1|Add1~16\);

-- Location: LCCOMB_X54_Y52_N14
\inst1|Add1~26\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add1~26_combout\ = (\inst1|vertical_current_count\(6) & ((\inst1|LessThan0~1_combout\) # ((\inst1|Add1~15_combout\ & \inst1|Add1~2_combout\)))) # (!\inst1|vertical_current_count\(6) & (\inst1|Add1~15_combout\ & ((\inst1|Add1~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|vertical_current_count\(6),
	datab => \inst1|Add1~15_combout\,
	datac => \inst1|LessThan0~1_combout\,
	datad => \inst1|Add1~2_combout\,
	combout => \inst1|Add1~26_combout\);

-- Location: FF_X54_Y52_N17
\inst1|vertical_current_count[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	asdata => \inst1|Add1~26_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|vertical_current_count\(6));

-- Location: LCCOMB_X55_Y52_N24
\inst1|Add1~17\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add1~17_combout\ = (\inst1|vertical_current_count\(7) & (!\inst1|Add1~16\)) # (!\inst1|vertical_current_count\(7) & ((\inst1|Add1~16\) # (GND)))
-- \inst1|Add1~18\ = CARRY((!\inst1|Add1~16\) # (!\inst1|vertical_current_count\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|vertical_current_count\(7),
	datad => VCC,
	cin => \inst1|Add1~16\,
	combout => \inst1|Add1~17_combout\,
	cout => \inst1|Add1~18\);

-- Location: LCCOMB_X55_Y52_N26
\inst1|Add1~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add1~20_combout\ = (\inst1|vertical_current_count\(8) & (\inst1|Add1~18\ $ (GND))) # (!\inst1|vertical_current_count\(8) & (!\inst1|Add1~18\ & VCC))
-- \inst1|Add1~21\ = CARRY((\inst1|vertical_current_count\(8) & !\inst1|Add1~18\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|vertical_current_count\(8),
	datad => VCC,
	cin => \inst1|Add1~18\,
	combout => \inst1|Add1~20_combout\,
	cout => \inst1|Add1~21\);

-- Location: LCCOMB_X54_Y52_N8
\inst1|Add1~22\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add1~22_combout\ = (\inst1|LessThan0~1_combout\ & ((\inst1|vertical_current_count\(8)) # ((\inst1|Add1~2_combout\ & \inst1|Add1~20_combout\)))) # (!\inst1|LessThan0~1_combout\ & (\inst1|Add1~2_combout\ & ((\inst1|Add1~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|LessThan0~1_combout\,
	datab => \inst1|Add1~2_combout\,
	datac => \inst1|vertical_current_count\(8),
	datad => \inst1|Add1~20_combout\,
	combout => \inst1|Add1~22_combout\);

-- Location: FF_X54_Y52_N9
\inst1|vertical_current_count[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst1|Add1~22_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|vertical_current_count\(8));

-- Location: LCCOMB_X55_Y52_N28
\inst1|Add1~23\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add1~23_combout\ = \inst1|Add1~21\ $ (\inst1|vertical_current_count\(9))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \inst1|vertical_current_count\(9),
	cin => \inst1|Add1~21\,
	combout => \inst1|Add1~23_combout\);

-- Location: LCCOMB_X54_Y52_N30
\inst1|Add1~25\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add1~25_combout\ = (\inst1|LessThan0~1_combout\ & ((\inst1|vertical_current_count\(9)) # ((\inst1|Add1~2_combout\ & \inst1|Add1~23_combout\)))) # (!\inst1|LessThan0~1_combout\ & (\inst1|Add1~2_combout\ & ((\inst1|Add1~23_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|LessThan0~1_combout\,
	datab => \inst1|Add1~2_combout\,
	datac => \inst1|vertical_current_count\(9),
	datad => \inst1|Add1~23_combout\,
	combout => \inst1|Add1~25_combout\);

-- Location: FF_X54_Y52_N31
\inst1|vertical_current_count[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst1|Add1~25_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|vertical_current_count\(9));

-- Location: LCCOMB_X54_Y52_N28
\inst1|Add1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add1~0_combout\ = (\inst1|vertical_current_count\(4)) # ((\inst1|vertical_current_count\(3)) # ((\inst1|vertical_current_count\(1) & \inst1|vertical_current_count\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|vertical_current_count\(4),
	datab => \inst1|vertical_current_count\(1),
	datac => \inst1|vertical_current_count\(2),
	datad => \inst1|vertical_current_count\(3),
	combout => \inst1|Add1~0_combout\);

-- Location: LCCOMB_X54_Y52_N16
\inst1|Add1~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add1~1_combout\ = (\inst1|vertical_current_count\(7)) # ((\inst1|vertical_current_count\(6)) # ((\inst1|vertical_current_count\(5) & \inst1|Add1~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|vertical_current_count\(5),
	datab => \inst1|vertical_current_count\(7),
	datac => \inst1|vertical_current_count\(6),
	datad => \inst1|Add1~0_combout\,
	combout => \inst1|Add1~1_combout\);

-- Location: LCCOMB_X54_Y52_N20
\inst1|Add1~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add1~2_combout\ = (!\inst1|LessThan0~1_combout\ & (((!\inst1|Add1~1_combout\) # (!\inst1|vertical_current_count\(9))) # (!\inst1|vertical_current_count\(8))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001100110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|vertical_current_count\(8),
	datab => \inst1|LessThan0~1_combout\,
	datac => \inst1|vertical_current_count\(9),
	datad => \inst1|Add1~1_combout\,
	combout => \inst1|Add1~2_combout\);

-- Location: LCCOMB_X54_Y52_N10
\inst1|Add1~19\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|Add1~19_combout\ = (\inst1|LessThan0~1_combout\ & ((\inst1|vertical_current_count\(7)) # ((\inst1|Add1~2_combout\ & \inst1|Add1~17_combout\)))) # (!\inst1|LessThan0~1_combout\ & (\inst1|Add1~2_combout\ & ((\inst1|Add1~17_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|LessThan0~1_combout\,
	datab => \inst1|Add1~2_combout\,
	datac => \inst1|vertical_current_count\(7),
	datad => \inst1|Add1~17_combout\,
	combout => \inst1|Add1~19_combout\);

-- Location: LCCOMB_X54_Y52_N12
\inst1|process_0~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|process_0~7_combout\ = (\inst1|Add1~30_combout\ & ((\inst1|Add1~29_combout\) # ((!\inst1|Add1~32_combout\ & !\inst1|Add1~31_combout\)))) # (!\inst1|Add1~30_combout\ & ((\inst1|Add1~29_combout\ $ (!\inst1|Add1~31_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010100111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|Add1~30_combout\,
	datab => \inst1|Add1~32_combout\,
	datac => \inst1|Add1~29_combout\,
	datad => \inst1|Add1~31_combout\,
	combout => \inst1|process_0~7_combout\);

-- Location: LCCOMB_X54_Y52_N18
\inst1|process_0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|process_0~8_combout\ = (\inst1|Add1~27_combout\) # ((\inst1|Add1~26_combout\) # ((\inst1|Add1~28_combout\) # (\inst1|process_0~7_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|Add1~27_combout\,
	datab => \inst1|Add1~26_combout\,
	datac => \inst1|Add1~28_combout\,
	datad => \inst1|process_0~7_combout\,
	combout => \inst1|process_0~8_combout\);

-- Location: LCCOMB_X54_Y52_N4
\inst1|process_0~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|process_0~9_combout\ = (\inst1|Add1~19_combout\) # (((\inst1|process_0~8_combout\) # (!\inst1|Add1~25_combout\)) # (!\inst1|Add1~22_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|Add1~19_combout\,
	datab => \inst1|Add1~22_combout\,
	datac => \inst1|Add1~25_combout\,
	datad => \inst1|process_0~8_combout\,
	combout => \inst1|process_0~9_combout\);

-- Location: FF_X54_Y52_N5
\inst1|v_sync\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst1|process_0~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|v_sync~q\);

-- Location: LCCOMB_X53_Y46_N12
\inst7|Add1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Add1~0_combout\ = \inst7|read_address\(0) $ (VCC)
-- \inst7|Add1~1\ = CARRY(\inst7|read_address\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|read_address\(0),
	datad => VCC,
	combout => \inst7|Add1~0_combout\,
	cout => \inst7|Add1~1\);

-- Location: LCCOMB_X54_Y51_N22
\inst1|process_0~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|process_0~10_combout\ = (!\inst1|horizontal_current_count~12_combout\ & ((!\inst1|Add1~22_combout\) # (!\inst1|Add1~25_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001100010011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|Add1~25_combout\,
	datab => \inst1|horizontal_current_count~12_combout\,
	datac => \inst1|Add1~22_combout\,
	combout => \inst1|process_0~10_combout\);

-- Location: FF_X54_Y51_N23
\inst1|frame_buff_en\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst1|process_0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|frame_buff_en~q\);

-- Location: FF_X53_Y46_N13
\inst7|read_address[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst7|Add1~0_combout\,
	ena => \inst1|frame_buff_en~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|read_address\(0));

-- Location: LCCOMB_X53_Y46_N14
\inst7|Add1~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Add1~2_combout\ = (\inst7|read_address\(1) & (!\inst7|Add1~1\)) # (!\inst7|read_address\(1) & ((\inst7|Add1~1\) # (GND)))
-- \inst7|Add1~3\ = CARRY((!\inst7|Add1~1\) # (!\inst7|read_address\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst7|read_address\(1),
	datad => VCC,
	cin => \inst7|Add1~1\,
	combout => \inst7|Add1~2_combout\,
	cout => \inst7|Add1~3\);

-- Location: FF_X53_Y46_N15
\inst7|read_address[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst7|Add1~2_combout\,
	ena => \inst1|frame_buff_en~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|read_address\(1));

-- Location: LCCOMB_X53_Y46_N16
\inst7|Add1~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Add1~4_combout\ = (\inst7|read_address\(2) & (\inst7|Add1~3\ $ (GND))) # (!\inst7|read_address\(2) & (!\inst7|Add1~3\ & VCC))
-- \inst7|Add1~5\ = CARRY((\inst7|read_address\(2) & !\inst7|Add1~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst7|read_address\(2),
	datad => VCC,
	cin => \inst7|Add1~3\,
	combout => \inst7|Add1~4_combout\,
	cout => \inst7|Add1~5\);

-- Location: FF_X53_Y46_N17
\inst7|read_address[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst7|Add1~4_combout\,
	ena => \inst1|frame_buff_en~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|read_address\(2));

-- Location: LCCOMB_X53_Y46_N18
\inst7|Add1~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Add1~6_combout\ = (\inst7|read_address\(3) & (!\inst7|Add1~5\)) # (!\inst7|read_address\(3) & ((\inst7|Add1~5\) # (GND)))
-- \inst7|Add1~7\ = CARRY((!\inst7|Add1~5\) # (!\inst7|read_address\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst7|read_address\(3),
	datad => VCC,
	cin => \inst7|Add1~5\,
	combout => \inst7|Add1~6_combout\,
	cout => \inst7|Add1~7\);

-- Location: FF_X53_Y46_N19
\inst7|read_address[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst7|Add1~6_combout\,
	ena => \inst1|frame_buff_en~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|read_address\(3));

-- Location: LCCOMB_X53_Y46_N20
\inst7|Add1~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Add1~8_combout\ = (\inst7|read_address\(4) & (\inst7|Add1~7\ $ (GND))) # (!\inst7|read_address\(4) & (!\inst7|Add1~7\ & VCC))
-- \inst7|Add1~9\ = CARRY((\inst7|read_address\(4) & !\inst7|Add1~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst7|read_address\(4),
	datad => VCC,
	cin => \inst7|Add1~7\,
	combout => \inst7|Add1~8_combout\,
	cout => \inst7|Add1~9\);

-- Location: FF_X53_Y46_N21
\inst7|read_address[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst7|Add1~8_combout\,
	ena => \inst1|frame_buff_en~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|read_address\(4));

-- Location: LCCOMB_X53_Y46_N22
\inst7|Add1~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Add1~10_combout\ = (\inst7|read_address\(5) & (!\inst7|Add1~9\)) # (!\inst7|read_address\(5) & ((\inst7|Add1~9\) # (GND)))
-- \inst7|Add1~11\ = CARRY((!\inst7|Add1~9\) # (!\inst7|read_address\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|read_address\(5),
	datad => VCC,
	cin => \inst7|Add1~9\,
	combout => \inst7|Add1~10_combout\,
	cout => \inst7|Add1~11\);

-- Location: FF_X53_Y46_N23
\inst7|read_address[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst7|Add1~10_combout\,
	ena => \inst1|frame_buff_en~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|read_address\(5));

-- Location: LCCOMB_X53_Y46_N24
\inst7|Add1~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Add1~12_combout\ = (\inst7|read_address\(6) & (\inst7|Add1~11\ $ (GND))) # (!\inst7|read_address\(6) & (!\inst7|Add1~11\ & VCC))
-- \inst7|Add1~13\ = CARRY((\inst7|read_address\(6) & !\inst7|Add1~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst7|read_address\(6),
	datad => VCC,
	cin => \inst7|Add1~11\,
	combout => \inst7|Add1~12_combout\,
	cout => \inst7|Add1~13\);

-- Location: FF_X53_Y46_N25
\inst7|read_address[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst7|Add1~12_combout\,
	ena => \inst1|frame_buff_en~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|read_address\(6));

-- Location: LCCOMB_X53_Y46_N26
\inst7|Add1~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Add1~14_combout\ = (\inst7|read_address\(7) & (!\inst7|Add1~13\)) # (!\inst7|read_address\(7) & ((\inst7|Add1~13\) # (GND)))
-- \inst7|Add1~15\ = CARRY((!\inst7|Add1~13\) # (!\inst7|read_address\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|read_address\(7),
	datad => VCC,
	cin => \inst7|Add1~13\,
	combout => \inst7|Add1~14_combout\,
	cout => \inst7|Add1~15\);

-- Location: FF_X53_Y46_N27
\inst7|read_address[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst7|Add1~14_combout\,
	ena => \inst1|frame_buff_en~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|read_address\(7));

-- Location: LCCOMB_X53_Y46_N28
\inst7|Add1~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Add1~16_combout\ = (\inst7|read_address\(8) & (\inst7|Add1~15\ $ (GND))) # (!\inst7|read_address\(8) & (!\inst7|Add1~15\ & VCC))
-- \inst7|Add1~17\ = CARRY((\inst7|read_address\(8) & !\inst7|Add1~15\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst7|read_address\(8),
	datad => VCC,
	cin => \inst7|Add1~15\,
	combout => \inst7|Add1~16_combout\,
	cout => \inst7|Add1~17\);

-- Location: FF_X53_Y46_N29
\inst7|read_address[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst7|Add1~16_combout\,
	ena => \inst1|frame_buff_en~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|read_address\(8));

-- Location: LCCOMB_X53_Y46_N30
\inst7|Add1~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Add1~18_combout\ = (\inst7|read_address\(9) & (!\inst7|Add1~17\)) # (!\inst7|read_address\(9) & ((\inst7|Add1~17\) # (GND)))
-- \inst7|Add1~19\ = CARRY((!\inst7|Add1~17\) # (!\inst7|read_address\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|read_address\(9),
	datad => VCC,
	cin => \inst7|Add1~17\,
	combout => \inst7|Add1~18_combout\,
	cout => \inst7|Add1~19\);

-- Location: FF_X53_Y46_N31
\inst7|read_address[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst7|Add1~18_combout\,
	ena => \inst1|frame_buff_en~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|read_address\(9));

-- Location: LCCOMB_X53_Y45_N0
\inst7|Add1~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Add1~20_combout\ = (\inst7|read_address\(10) & (\inst7|Add1~19\ $ (GND))) # (!\inst7|read_address\(10) & (!\inst7|Add1~19\ & VCC))
-- \inst7|Add1~21\ = CARRY((\inst7|read_address\(10) & !\inst7|Add1~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst7|read_address\(10),
	datad => VCC,
	cin => \inst7|Add1~19\,
	combout => \inst7|Add1~20_combout\,
	cout => \inst7|Add1~21\);

-- Location: FF_X53_Y45_N1
\inst7|read_address[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst7|Add1~20_combout\,
	ena => \inst1|frame_buff_en~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|read_address\(10));

-- Location: LCCOMB_X53_Y45_N2
\inst7|Add1~22\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Add1~22_combout\ = (\inst7|read_address\(11) & (!\inst7|Add1~21\)) # (!\inst7|read_address\(11) & ((\inst7|Add1~21\) # (GND)))
-- \inst7|Add1~23\ = CARRY((!\inst7|Add1~21\) # (!\inst7|read_address\(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst7|read_address\(11),
	datad => VCC,
	cin => \inst7|Add1~21\,
	combout => \inst7|Add1~22_combout\,
	cout => \inst7|Add1~23\);

-- Location: FF_X53_Y45_N3
\inst7|read_address[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst7|Add1~22_combout\,
	ena => \inst1|frame_buff_en~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|read_address\(11));

-- Location: LCCOMB_X53_Y45_N4
\inst7|Add1~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Add1~24_combout\ = (\inst7|read_address\(12) & (\inst7|Add1~23\ $ (GND))) # (!\inst7|read_address\(12) & (!\inst7|Add1~23\ & VCC))
-- \inst7|Add1~25\ = CARRY((\inst7|read_address\(12) & !\inst7|Add1~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst7|read_address\(12),
	datad => VCC,
	cin => \inst7|Add1~23\,
	combout => \inst7|Add1~24_combout\,
	cout => \inst7|Add1~25\);

-- Location: FF_X53_Y45_N5
\inst7|read_address[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst7|Add1~24_combout\,
	ena => \inst1|frame_buff_en~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|read_address\(12));

-- Location: LCCOMB_X53_Y45_N6
\inst7|Add1~26\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Add1~26_combout\ = (\inst7|read_address\(13) & (!\inst7|Add1~25\)) # (!\inst7|read_address\(13) & ((\inst7|Add1~25\) # (GND)))
-- \inst7|Add1~27\ = CARRY((!\inst7|Add1~25\) # (!\inst7|read_address\(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|read_address\(13),
	datad => VCC,
	cin => \inst7|Add1~25\,
	combout => \inst7|Add1~26_combout\,
	cout => \inst7|Add1~27\);

-- Location: FF_X53_Y45_N7
\inst7|read_address[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst7|Add1~26_combout\,
	ena => \inst1|frame_buff_en~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|read_address\(13));

-- Location: LCCOMB_X53_Y45_N8
\inst7|Add1~28\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Add1~28_combout\ = (\inst7|read_address\(14) & (\inst7|Add1~27\ $ (GND))) # (!\inst7|read_address\(14) & (!\inst7|Add1~27\ & VCC))
-- \inst7|Add1~29\ = CARRY((\inst7|read_address\(14) & !\inst7|Add1~27\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst7|read_address\(14),
	datad => VCC,
	cin => \inst7|Add1~27\,
	combout => \inst7|Add1~28_combout\,
	cout => \inst7|Add1~29\);

-- Location: FF_X53_Y45_N9
\inst7|read_address[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst7|Add1~28_combout\,
	ena => \inst1|frame_buff_en~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|read_address\(14));

-- Location: LCCOMB_X53_Y45_N10
\inst7|Add1~30\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Add1~30_combout\ = (\inst7|read_address\(15) & (!\inst7|Add1~29\)) # (!\inst7|read_address\(15) & ((\inst7|Add1~29\) # (GND)))
-- \inst7|Add1~31\ = CARRY((!\inst7|Add1~29\) # (!\inst7|read_address\(15)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|read_address\(15),
	datad => VCC,
	cin => \inst7|Add1~29\,
	combout => \inst7|Add1~30_combout\,
	cout => \inst7|Add1~31\);

-- Location: FF_X53_Y45_N11
\inst7|read_address[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst7|Add1~30_combout\,
	ena => \inst1|frame_buff_en~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|read_address\(15));

-- Location: FF_X52_Y45_N27
\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	asdata => \inst7|read_address\(15),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2));

-- Location: LCCOMB_X16_Y45_N24
\~GND\ : cycloneive_lcell_comb
-- Equation(s):
-- \~GND~combout\ = GND

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \~GND~combout\);

-- Location: M9K_X51_Y45_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a5\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a5_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a5_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a5_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a5_PORTBDATAOUT_bus\);

-- Location: FF_X52_Y45_N31
\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	asdata => \inst7|read_address\(14),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1));

-- Location: M9K_X51_Y37_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a7\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a7_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a7_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a7_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a7_PORTBDATAOUT_bus\);

-- Location: FF_X52_Y45_N21
\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	asdata => \inst7|read_address\(13),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0));

-- Location: LCCOMB_X50_Y45_N14
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~52\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~52_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a7~portbdataout\))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a5~portbdataout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a5~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a7~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~52_combout\);

-- Location: M9K_X37_Y43_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a6\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a6_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a6_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a6_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a6_PORTBDATAOUT_bus\);

-- Location: M9K_X37_Y46_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a4\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a4_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a4_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a4_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a4_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X38_Y45_N24
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~53\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~53_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a6~portbdataout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a4~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a6~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a4~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~53_combout\);

-- Location: LCCOMB_X53_Y45_N12
\inst7|Add1~32\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Add1~32_combout\ = (\inst7|read_address\(16) & (\inst7|Add1~31\ $ (GND))) # (!\inst7|read_address\(16) & (!\inst7|Add1~31\ & VCC))
-- \inst7|Add1~33\ = CARRY((\inst7|read_address\(16) & !\inst7|Add1~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|read_address\(16),
	datad => VCC,
	cin => \inst7|Add1~31\,
	combout => \inst7|Add1~32_combout\,
	cout => \inst7|Add1~33\);

-- Location: FF_X53_Y45_N13
\inst7|read_address[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst7|Add1~32_combout\,
	ena => \inst1|frame_buff_en~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|read_address\(16));

-- Location: FF_X52_Y45_N25
\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	asdata => \inst7|read_address\(16),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(3));

-- Location: LCCOMB_X52_Y45_N12
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~54\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~54_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2) & (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(3) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~52_combout\) # (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~53_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2),
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~52_combout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~53_combout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(3),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~54_combout\);

-- Location: M9K_X37_Y37_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a12\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a12_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a12_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a12_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a12_PORTBDATAOUT_bus\);

-- Location: M9K_X37_Y33_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a14\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a14_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a14_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a14_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a14_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X38_Y45_N6
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~61\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~61_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a14~portbdataout\))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a12~portbdataout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a12~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a14~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~61_combout\);

-- Location: M9K_X37_Y42_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a15\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a15_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a15_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a15_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a15_PORTBDATAOUT_bus\);

-- Location: M9K_X37_Y48_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a13\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a13_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a13_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a13_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a13_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X38_Y45_N4
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~62\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~62_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a15~portbdataout\)) # 
-- (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a13~portbdataout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a15~portbdataout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a13~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~62_combout\);

-- Location: LCCOMB_X38_Y45_N2
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~63\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~63_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~61_combout\) # (((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & 
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~62_combout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~61_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~62_combout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~63_combout\);

-- Location: LCCOMB_X53_Y45_N14
\inst7|Add1~34\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Add1~34_combout\ = (\inst7|read_address\(17) & (!\inst7|Add1~33\)) # (!\inst7|read_address\(17) & ((\inst7|Add1~33\) # (GND)))
-- \inst7|Add1~35\ = CARRY((!\inst7|Add1~33\) # (!\inst7|read_address\(17)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst7|read_address\(17),
	datad => VCC,
	cin => \inst7|Add1~33\,
	combout => \inst7|Add1~34_combout\,
	cout => \inst7|Add1~35\);

-- Location: FF_X53_Y45_N15
\inst7|read_address[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst7|Add1~34_combout\,
	ena => \inst1|frame_buff_en~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|read_address\(17));

-- Location: FF_X52_Y45_N1
\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	asdata => \inst7|read_address\(17),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(4));

-- Location: M9K_X64_Y35_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a3\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a3_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a3_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a3_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a3_PORTBDATAOUT_bus\);

-- Location: M9K_X64_Y52_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a1\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a1_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a1_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a1_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a1_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X63_Y45_N6
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~58\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~58_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a3~portbdataout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a1~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a3~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a1~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~58_combout\);

-- Location: M9K_X64_Y44_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a2\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a2_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a2_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a2_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a2_PORTBDATAOUT_bus\);

-- Location: M9K_X64_Y36_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a0\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => VCC,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a0_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a0_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a0_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X63_Y45_N20
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~59\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~59_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a2~portbdataout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a0~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a2~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a0~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~59_combout\);

-- Location: LCCOMB_X38_Y45_N26
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~5_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(3) & !\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(3),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~5_combout\);

-- Location: M9K_X78_Y37_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a10\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a10_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a10_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a10_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a10_PORTBDATAOUT_bus\);

-- Location: M9K_X78_Y49_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a8\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a8_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a8_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a8_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a8_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X77_Y45_N26
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~56\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~56_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a10~portbdataout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a8~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a10~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a8~portbdataout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~56_combout\);

-- Location: M9K_X78_Y50_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a11\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a11_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a11_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a11_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a11_PORTBDATAOUT_bus\);

-- Location: M9K_X78_Y45_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a9\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a9_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a9_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a9_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a9_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X77_Y45_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~55\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~55_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a11~portbdataout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a9~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a11~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a9~portbdataout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~55_combout\);

-- Location: LCCOMB_X77_Y45_N24
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~57\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~57_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(3) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~56_combout\) # 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2)) # (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~55_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~56_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(3),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~55_combout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~57_combout\);

-- Location: LCCOMB_X63_Y45_N2
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~60\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~60_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~57_combout\) # ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~5_combout\ & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~58_combout\) # (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~59_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~58_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~59_combout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~5_combout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~57_combout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~60_combout\);

-- Location: LCCOMB_X52_Y45_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~78\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~78_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(4) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~54_combout\) # 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~63_combout\ & \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~60_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~54_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~63_combout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(4),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~60_combout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~78_combout\);

-- Location: M9K_X78_Y54_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a47\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a47_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a47_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a47_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a47_PORTBDATAOUT_bus\);

-- Location: M9K_X78_Y43_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a45\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a45_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a45_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a45_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a45_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X77_Y45_N22
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~70\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~70_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a47~portbdataout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a45~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a47~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a45~portbdataout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~70_combout\);

-- Location: M9K_X78_Y36_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a32\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a32_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a32_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a32_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a32_PORTBDATAOUT_bus\);

-- Location: M9K_X78_Y38_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a34\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a34_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a34_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a34_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a34_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X77_Y45_N6
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~68\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~68_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a34~portbdataout\))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a32~portbdataout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a32~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a34~portbdataout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~68_combout\);

-- Location: M9K_X78_Y52_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a33\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a33_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a33_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a33_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a33_PORTBDATAOUT_bus\);

-- Location: M9K_X78_Y55_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a35\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a35_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a35_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a35_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a35_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X77_Y45_N8
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~67\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~67_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a35~portbdataout\))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a33~portbdataout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a33~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a35~portbdataout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~67_combout\);

-- Location: M9K_X78_Y40_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a43\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a43_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a43_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a43_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a43_PORTBDATAOUT_bus\);

-- Location: M9K_X78_Y48_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a41\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a41_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a41_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a41_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a41_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X77_Y45_N30
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~64\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~64_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a43~portbdataout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a41~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a43~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a41~portbdataout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~64_combout\);

-- Location: M9K_X78_Y44_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a42\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a42_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a42_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a42_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a42_PORTBDATAOUT_bus\);

-- Location: M9K_X78_Y53_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a40\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a40_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a40_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a40_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a40_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X77_Y45_N16
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~65\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~65_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a42~portbdataout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a40~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a42~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a40~portbdataout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~65_combout\);

-- Location: LCCOMB_X77_Y45_N2
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~66\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~66_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(3) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~64_combout\) # 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2)) # (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~65_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~64_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(3),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~65_combout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~66_combout\);

-- Location: LCCOMB_X77_Y45_N28
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~69\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~69_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~66_combout\) # ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~5_combout\ & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~68_combout\) # (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~67_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~68_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~67_combout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~5_combout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~66_combout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~69_combout\);

-- Location: M9K_X78_Y51_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a44\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a44_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a44_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a44_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a44_PORTBDATAOUT_bus\);

-- Location: M9K_X78_Y47_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a46\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a46_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a46_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a46_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a46_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X77_Y45_N4
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~71\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~71_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a46~portbdataout\))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a44~portbdataout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a44~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a46~portbdataout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~71_combout\);

-- Location: LCCOMB_X77_Y45_N14
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~72\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~72_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~69_combout\ & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~70_combout\) # 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~71_combout\) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~70_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~69_combout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~71_combout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~72_combout\);

-- Location: LCCOMB_X53_Y45_N16
\inst7|Add1~36\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Add1~36_combout\ = (\inst7|read_address\(18) & (\inst7|Add1~35\ $ (GND))) # (!\inst7|read_address\(18) & (!\inst7|Add1~35\ & VCC))
-- \inst7|Add1~37\ = CARRY((\inst7|read_address\(18) & !\inst7|Add1~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|read_address\(18),
	datad => VCC,
	cin => \inst7|Add1~35\,
	combout => \inst7|Add1~36_combout\,
	cout => \inst7|Add1~37\);

-- Location: LCCOMB_X53_Y45_N18
\inst7|Add1~38\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Add1~38_combout\ = \inst7|Add1~37\ $ (\inst7|read_address\(19))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \inst7|read_address\(19),
	cin => \inst7|Add1~37\,
	combout => \inst7|Add1~38_combout\);

-- Location: LCCOMB_X53_Y45_N28
\inst7|read_address~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|read_address~1_combout\ = (!\inst7|Equal0~6_combout\ & \inst7|Add1~38_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst7|Equal0~6_combout\,
	datad => \inst7|Add1~38_combout\,
	combout => \inst7|read_address~1_combout\);

-- Location: FF_X53_Y45_N29
\inst7|read_address[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst7|read_address~1_combout\,
	ena => \inst1|frame_buff_en~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|read_address\(19));

-- Location: LCCOMB_X52_Y45_N24
\inst7|Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Equal0~0_combout\ = (!\inst7|read_address\(18) & (\inst7|read_address\(17) & (\inst7|read_address\(16) & \inst7|read_address\(19))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|read_address\(18),
	datab => \inst7|read_address\(17),
	datac => \inst7|read_address\(16),
	datad => \inst7|read_address\(19),
	combout => \inst7|Equal0~0_combout\);

-- Location: LCCOMB_X53_Y45_N20
\inst7|Equal0~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Equal0~5_combout\ = (\inst7|read_address\(15) & (\inst7|read_address\(14) & (\inst7|read_address\(12) & \inst7|read_address\(13))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|read_address\(15),
	datab => \inst7|read_address\(14),
	datac => \inst7|read_address\(12),
	datad => \inst7|read_address\(13),
	combout => \inst7|Equal0~5_combout\);

-- Location: LCCOMB_X53_Y45_N26
\inst7|Equal0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Equal0~4_combout\ = (\inst7|read_address\(8) & (\inst7|read_address\(10) & (\inst7|read_address\(9) & \inst7|read_address\(11))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|read_address\(8),
	datab => \inst7|read_address\(10),
	datac => \inst7|read_address\(9),
	datad => \inst7|read_address\(11),
	combout => \inst7|Equal0~4_combout\);

-- Location: LCCOMB_X53_Y46_N10
\inst7|Equal0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Equal0~2_combout\ = (\inst7|read_address\(7) & (\inst7|read_address\(6) & (\inst7|read_address\(5) & \inst7|read_address\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|read_address\(7),
	datab => \inst7|read_address\(6),
	datac => \inst7|read_address\(5),
	datad => \inst7|read_address\(4),
	combout => \inst7|Equal0~2_combout\);

-- Location: LCCOMB_X53_Y46_N4
\inst7|Equal0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Equal0~1_combout\ = (\inst7|read_address\(1) & \inst7|read_address\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst7|read_address\(1),
	datad => \inst7|read_address\(0),
	combout => \inst7|Equal0~1_combout\);

-- Location: LCCOMB_X53_Y46_N8
\inst7|Equal0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Equal0~3_combout\ = (\inst7|Equal0~2_combout\ & (\inst7|read_address\(2) & (\inst7|Equal0~1_combout\ & \inst7|read_address\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|Equal0~2_combout\,
	datab => \inst7|read_address\(2),
	datac => \inst7|Equal0~1_combout\,
	datad => \inst7|read_address\(3),
	combout => \inst7|Equal0~3_combout\);

-- Location: LCCOMB_X53_Y45_N22
\inst7|Equal0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|Equal0~6_combout\ = (\inst7|Equal0~0_combout\ & (\inst7|Equal0~5_combout\ & (\inst7|Equal0~4_combout\ & \inst7|Equal0~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|Equal0~0_combout\,
	datab => \inst7|Equal0~5_combout\,
	datac => \inst7|Equal0~4_combout\,
	datad => \inst7|Equal0~3_combout\,
	combout => \inst7|Equal0~6_combout\);

-- Location: LCCOMB_X53_Y45_N30
\inst7|read_address~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|read_address~0_combout\ = (!\inst7|Equal0~6_combout\ & \inst7|Add1~36_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst7|Equal0~6_combout\,
	datad => \inst7|Add1~36_combout\,
	combout => \inst7|read_address~0_combout\);

-- Location: FF_X53_Y45_N31
\inst7|read_address[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	d => \inst7|read_address~0_combout\,
	ena => \inst1|frame_buff_en~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|read_address\(18));

-- Location: FF_X52_Y45_N7
\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	asdata => \inst7|read_address\(18),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(5));

-- Location: M9K_X51_Y32_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a38\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a38_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a38_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a38_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a38_PORTBDATAOUT_bus\);

-- Location: M9K_X51_Y33_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a36\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a36_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a36_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a36_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a36_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X52_Y45_N30
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~74\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~74_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a38~portbdataout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a36~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a38~portbdataout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a36~portbdataout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~74_combout\);

-- Location: M9K_X51_Y56_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a37\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a37_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a37_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a37_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a37_PORTBDATAOUT_bus\);

-- Location: M9K_X51_Y35_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a39\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a39_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a39_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a39_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a39_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X52_Y45_N20
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~73\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~73_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a39~portbdataout\))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a37~portbdataout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a37~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a39~portbdataout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~73_combout\);

-- Location: LCCOMB_X63_Y45_N28
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~12_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2) & !\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(3),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~12_combout\);

-- Location: LCCOMB_X52_Y45_N18
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~75\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~75_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(4)) # ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~12_combout\ & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~74_combout\) # (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~73_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~74_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~73_combout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~12_combout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(4),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~75_combout\);

-- Location: LCCOMB_X52_Y45_N6
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~79\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~79_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(5) & (((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~72_combout\) # 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~75_combout\)))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(5) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~78_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~78_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~72_combout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(5),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~75_combout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~79_combout\);

-- Location: M9K_X64_Y42_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a20\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a20_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a20_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a20_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a20_PORTBDATAOUT_bus\);

-- Location: M9K_X64_Y50_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a21\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a21_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a21_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a21_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a21_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X63_Y45_N8
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~49\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~49_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a21~portbdataout\))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a20~portbdataout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a20~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a21~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~49_combout\);

-- Location: M9K_X64_Y40_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a22\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a22_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a22_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a22_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a22_PORTBDATAOUT_bus\);

-- Location: M9K_X64_Y47_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a23\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a23_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a23_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a23_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a23_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X63_Y45_N18
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~50\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~50_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a23~portbdataout\))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a22~portbdataout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a22~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a23~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~50_combout\);

-- Location: M9K_X64_Y55_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a29\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a29_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a29_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a29_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a29_PORTBDATAOUT_bus\);

-- Location: M9K_X64_Y56_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a31\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a31_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a31_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a31_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a31_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X63_Y45_N30
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~46_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a31~portbdataout\))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a29~portbdataout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a29~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a31~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~46_combout\);

-- Location: M9K_X64_Y39_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a30\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a30_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a30_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a30_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a30_PORTBDATAOUT_bus\);

-- Location: M9K_X64_Y48_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a28\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a28_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a28_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a28_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a28_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X63_Y45_N24
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~47\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~47_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a30~portbdataout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a28~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a30~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a28~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~47_combout\);

-- Location: M9K_X64_Y49_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a26\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a26_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a26_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a26_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a26_PORTBDATAOUT_bus\);

-- Location: M9K_X64_Y37_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a24\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a24_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a24_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a24_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a24_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X63_Y45_N12
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~41\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~41_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a26~portbdataout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a24~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a26~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a24~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~41_combout\);

-- Location: M9K_X64_Y41_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a25\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a25_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a25_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a25_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a25_PORTBDATAOUT_bus\);

-- Location: M9K_X64_Y53_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a27\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a27_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a27_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a27_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a27_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X63_Y45_N22
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~40_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a27~portbdataout\))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a25~portbdataout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a25~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a27~portbdataout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~40_combout\);

-- Location: LCCOMB_X63_Y45_N26
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~42_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(3) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2)) # 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~41_combout\) # (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~40_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2),
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~41_combout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~40_combout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(3),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~42_combout\);

-- Location: M9K_X64_Y34_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a19\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a19_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a19_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a19_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a19_PORTBDATAOUT_bus\);

-- Location: M9K_X64_Y51_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a17\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a17_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a17_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a17_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a17_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X63_Y45_N4
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~43\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~43_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a19~portbdataout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a17~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a19~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a17~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~43_combout\);

-- Location: M9K_X64_Y38_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a16\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a16_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a16_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a16_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a16_PORTBDATAOUT_bus\);

-- Location: M9K_X64_Y43_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a18\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a18_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a18_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a18_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a18_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X63_Y45_N10
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~44_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a18~portbdataout\))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a16~portbdataout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a16~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a18~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~44_combout\);

-- Location: LCCOMB_X63_Y45_N16
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~45\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~45_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~42_combout\) # ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~5_combout\ & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~43_combout\) # (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~44_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~42_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~43_combout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~5_combout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~44_combout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~45_combout\);

-- Location: LCCOMB_X63_Y45_N14
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~48_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~45_combout\ & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~46_combout\) # 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~47_combout\) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~46_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~47_combout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~45_combout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~48_combout\);

-- Location: LCCOMB_X63_Y45_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~51\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~51_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~48_combout\) # ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~12_combout\ & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~49_combout\) # (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~50_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~49_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~50_combout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~48_combout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~12_combout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~51_combout\);

-- Location: M9K_X51_Y47_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a52\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a52_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a52_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a52_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a52_PORTBDATAOUT_bus\);

-- Location: M9K_X51_Y51_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a53\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a53_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a53_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a53_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a53_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X52_Y45_N2
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~28\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~28_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a53~portbdataout\))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a52~portbdataout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a52~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a53~portbdataout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~28_combout\);

-- Location: M9K_X64_Y45_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a54\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a54_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a54_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a54_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a54_PORTBDATAOUT_bus\);

-- Location: M9K_X51_Y43_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a55\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a55_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a55_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a55_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a55_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X52_Y45_N4
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~29\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~29_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a55~portbdataout\))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a54~portbdataout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a54~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a55~portbdataout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~29_combout\);

-- Location: LCCOMB_X52_Y45_N14
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~30\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~30_combout\ = ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~12_combout\ & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~28_combout\) # 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~29_combout\)))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~12_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~28_combout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~29_combout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(4),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~30_combout\);

-- Location: M9K_X64_Y46_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a62\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a62_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a62_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a62_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a62_PORTBDATAOUT_bus\);

-- Location: M9K_X15_Y46_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a60\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a60_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a60_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a60_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a60_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X53_Y46_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~31\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~31_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a62~portbdataout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a60~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a62~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a60~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~31_combout\);

-- Location: M9K_X78_Y46_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a63\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a63_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a63_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a63_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a63_PORTBDATAOUT_bus\);

-- Location: M9K_X64_Y54_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a61\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a61_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a61_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a61_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a61_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X65_Y46_N4
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~32\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~32_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a63~portbdataout\)) # 
-- (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a61~portbdataout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a63~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a61~portbdataout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~32_combout\);

-- Location: LCCOMB_X53_Y46_N6
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~33\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~33_combout\ = ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~31_combout\) # ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~32_combout\ & 
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0)))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110111011101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2),
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~31_combout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~32_combout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~33_combout\);

-- Location: M9K_X51_Y53_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a56\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a56_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a56_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a56_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a56_PORTBDATAOUT_bus\);

-- Location: M9K_X51_Y42_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a58\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a58_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a58_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a58_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a58_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X52_Y45_N10
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~37\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~37_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a58~portbdataout\))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a56~portbdataout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a56~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a58~portbdataout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~37_combout\);

-- Location: M9K_X51_Y50_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a59\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a59_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a59_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a59_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a59_PORTBDATAOUT_bus\);

-- Location: M9K_X51_Y46_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a57\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a57_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a57_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a57_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a57_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X52_Y45_N8
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~36\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~36_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a59~portbdataout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a57~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a59~portbdataout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a57~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~36_combout\);

-- Location: LCCOMB_X52_Y45_N26
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~38\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~38_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(3) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~37_combout\) # 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~36_combout\) # (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~37_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~36_combout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(3),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~38_combout\);

-- Location: M9K_X78_Y42_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a48\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a48_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a48_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a48_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a48_PORTBDATAOUT_bus\);

-- Location: M9K_X78_Y41_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a50\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a50_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a50_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a50_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a50_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X77_Y45_N10
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~35\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~35_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a50~portbdataout\))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a48~portbdataout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a48~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a50~portbdataout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~35_combout\);

-- Location: M9K_X78_Y39_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a51\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a51_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a51_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a51_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a51_PORTBDATAOUT_bus\);

-- Location: M9K_X78_Y56_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a49\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a49_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a49_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a49_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a49_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X77_Y45_N20
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~34\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~34_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a51~portbdataout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a49~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a51~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a49~portbdataout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~34_combout\);

-- Location: LCCOMB_X77_Y45_N12
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~77\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~77_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2) & (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(3) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~35_combout\) # (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~34_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~35_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(3),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~34_combout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~77_combout\);

-- Location: LCCOMB_X53_Y45_N24
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~39\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~39_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~30_combout\) # ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~33_combout\ & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~38_combout\) # (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~77_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~30_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~33_combout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~38_combout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~77_combout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~39_combout\);

-- Location: LCCOMB_X52_Y45_N16
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~76\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~76_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~79_combout\ & (((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~39_combout\)))) # 
-- (!\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~79_combout\ & (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(4) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~51_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~79_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(4),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~51_combout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~39_combout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~76_combout\);

-- Location: FF_X52_Y45_N23
\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputclkctrl_outclk\,
	asdata => \inst7|read_address\(19),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(6));

-- Location: M9K_X51_Y38_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a68\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a68_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a68_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a68_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a68_PORTBDATAOUT_bus\);

-- Location: M9K_X51_Y36_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a69\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a69_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a69_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a69_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a69_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X50_Y45_N30
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~25\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~25_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a69~portbdataout\))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a68~portbdataout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a68~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a69~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~25_combout\);

-- Location: M9K_X51_Y48_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a79\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a79_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a79_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a79_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a79_PORTBDATAOUT_bus\);

-- Location: M9K_X37_Y32_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a77\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a77_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a77_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a77_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a77_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X50_Y45_N22
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~22\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~22_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a79~portbdataout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a77~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a79~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a77~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~22_combout\);

-- Location: M9K_X37_Y44_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a73\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a73_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a73_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a73_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a73_PORTBDATAOUT_bus\);

-- Location: M9K_X37_Y47_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a75\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a75_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a75_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a75_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a75_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X38_Y45_N10
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~16_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a75~portbdataout\))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a73~portbdataout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a73~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a75~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~16_combout\);

-- Location: M9K_X37_Y41_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a72\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a72_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a72_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a72_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a72_PORTBDATAOUT_bus\);

-- Location: M9K_X37_Y50_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a74\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a74_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a74_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a74_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a74_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X38_Y45_N8
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~17\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~17_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a74~portbdataout\))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a72~portbdataout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a72~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a74~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~17_combout\);

-- Location: LCCOMB_X38_Y45_N30
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~18_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(3) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~16_combout\) # 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2)) # (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~17_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~16_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~17_combout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(3),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~18_combout\);

-- Location: M9K_X51_Y49_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a66\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a66_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a66_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a66_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a66_PORTBDATAOUT_bus\);

-- Location: M9K_X15_Y45_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a64\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a64_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a64_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a64_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a64_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X50_Y45_N26
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~20_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a66~portbdataout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a64~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a66~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a64~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~20_combout\);

-- Location: M9K_X51_Y55_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a65\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a65_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a65_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a65_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a65_PORTBDATAOUT_bus\);

-- Location: M9K_X51_Y54_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a67\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a67_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a67_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a67_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a67_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X50_Y45_N24
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~19\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~19_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a67~portbdataout\))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a65~portbdataout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a65~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a67~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~19_combout\);

-- Location: LCCOMB_X50_Y45_N20
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~21\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~21_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~18_combout\) # ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~5_combout\ & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~20_combout\) # (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~19_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~5_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~18_combout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~20_combout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~19_combout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~21_combout\);

-- Location: M9K_X37_Y55_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a78\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a78_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a78_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a78_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a78_PORTBDATAOUT_bus\);

-- Location: M9K_X37_Y51_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a76\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a76_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a76_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a76_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a76_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X38_Y45_N28
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~23\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~23_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a78~portbdataout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a76~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a78~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a76~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~23_combout\);

-- Location: LCCOMB_X50_Y45_N16
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~24_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~21_combout\ & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~22_combout\) # 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~23_combout\) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~22_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~21_combout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~23_combout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~24_combout\);

-- Location: M9K_X37_Y40_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a70\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a70_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a70_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a70_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a70_PORTBDATAOUT_bus\);

-- Location: M9K_X37_Y39_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a71\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a71_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a71_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a71_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a71_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X38_Y45_N18
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~26\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~26_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a71~portbdataout\))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a70~portbdataout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a70~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a71~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~26_combout\);

-- Location: LCCOMB_X50_Y45_N4
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~27\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~27_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~24_combout\) # ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~12_combout\ & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~25_combout\) # (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~26_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~25_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~24_combout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~26_combout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~12_combout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~27_combout\);

-- Location: M9K_X51_Y41_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a84\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a84_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a84_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a84_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a84_PORTBDATAOUT_bus\);

-- Location: M9K_X37_Y45_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a85\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a85_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a85_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a85_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a85_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X50_Y45_N10
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~13_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a85~portbdataout\))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a84~portbdataout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a84~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a85~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~13_combout\);

-- Location: M9K_X51_Y52_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a95\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a95_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a95_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a95_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a95_PORTBDATAOUT_bus\);

-- Location: M9K_X51_Y40_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a93\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a93_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a93_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a93_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a93_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X50_Y45_N12
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~9_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a95~portbdataout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a93~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a95~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a93~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~9_combout\);

-- Location: M9K_X37_Y56_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a94\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a94_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a94_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a94_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a94_PORTBDATAOUT_bus\);

-- Location: M9K_X51_Y44_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a92\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a92_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a92_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a92_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a92_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X50_Y45_N18
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~10_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a94~portbdataout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a92~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a94~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a92~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~10_combout\);

-- Location: M9K_X37_Y38_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a91\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a91_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a91_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a91_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a91_PORTBDATAOUT_bus\);

-- Location: M9K_X37_Y54_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a89\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a89_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a89_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a89_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a89_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X38_Y45_N12
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~2_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a91~portbdataout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a89~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a91~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a89~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~2_combout\);

-- Location: M9K_X37_Y35_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a90\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a90_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a90_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a90_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a90_PORTBDATAOUT_bus\);

-- Location: M9K_X37_Y52_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a88\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a88_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a88_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a88_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a88_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X38_Y45_N14
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~3_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a90~portbdataout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a88~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a90~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a88~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~3_combout\);

-- Location: LCCOMB_X38_Y45_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~4_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(3) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~2_combout\) # 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2)) # (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~2_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~3_combout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(3),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~4_combout\);

-- Location: M9K_X37_Y34_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a82\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a82_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a82_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a82_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a82_PORTBDATAOUT_bus\);

-- Location: M9K_X37_Y49_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a80\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a80_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a80_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a80_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a80_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X38_Y45_N22
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~7_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a82~portbdataout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a80~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a82~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a80~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~7_combout\);

-- Location: M9K_X37_Y36_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a83\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a83_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a83_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a83_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a83_PORTBDATAOUT_bus\);

-- Location: M9K_X37_Y53_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a81\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a81_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a81_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a81_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a81_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X38_Y45_N16
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~6_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & 
-- (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a83~portbdataout\)) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a81~portbdataout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a83~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a81~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~6_combout\);

-- Location: LCCOMB_X38_Y45_N20
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~8_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~4_combout\) # ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~5_combout\ & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~7_combout\) # (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~5_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~4_combout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~7_combout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~6_combout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~8_combout\);

-- Location: LCCOMB_X50_Y45_N28
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~11_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~8_combout\ & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~9_combout\) # 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~10_combout\) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~9_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~10_combout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~8_combout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(2),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~11_combout\);

-- Location: M9K_X51_Y34_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a86\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a86_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a86_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a86_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a86_PORTBDATAOUT_bus\);

-- Location: M9K_X51_Y39_N0
\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a87\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "framebuffer:inst7|ram_dual:ram_i|altsyncram:ram_block_rtl_0|altsyncram_olg1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 13,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 8191,
	port_a_logical_ram_depth => 786432,
	port_a_logical_ram_width => 1,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 13,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 8191,
	port_b_logical_ram_depth => 786432,
	port_b_logical_ram_width => 1,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbre => VCC,
	clk0 => \CLOCK_50~inputclkctrl_outclk\,
	portadatain => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a87_PORTADATAIN_bus\,
	portaaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a87_PORTAADDR_bus\,
	portbaddr => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a87_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a87_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X50_Y45_N8
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~14_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a87~portbdataout\))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a86~portbdataout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a86~portbdataout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(0),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|ram_block1a87~portbdataout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(1),
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~14_combout\);

-- Location: LCCOMB_X50_Y45_N6
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~15\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~15_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~11_combout\) # ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~12_combout\ & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~13_combout\) # (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~14_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~13_combout\,
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~11_combout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~14_combout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~12_combout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~15_combout\);

-- Location: LCCOMB_X52_Y45_N28
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~0_combout\ = (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(5) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(4) & 
-- ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~15_combout\))) # (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(4) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~27_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(5),
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(4),
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~27_combout\,
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~15_combout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~0_combout\);

-- Location: LCCOMB_X52_Y45_N22
\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\ = (\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(6) & ((\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~0_combout\))) # 
-- (!\inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(6) & (\inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~76_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|_~76_combout\,
	datac => \inst7|ram_i|ram_block_rtl_0|auto_generated|address_reg_b\(6),
	datad => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~0_combout\,
	combout => \inst7|ram_i|ram_block_rtl_0|auto_generated|mux3|result_node[0]~1_combout\);

ww_VGA_CLK <= \VGA_CLK~output_o\;

ww_VGA_BLANK_N <= \VGA_BLANK_N~output_o\;

ww_VGA_HS <= \VGA_HS~output_o\;

ww_VGA_VS <= \VGA_VS~output_o\;

ww_VGA_SYNC_N <= \VGA_SYNC_N~output_o\;

ww_VGA_B(7) <= \VGA_B[7]~output_o\;

ww_VGA_B(6) <= \VGA_B[6]~output_o\;

ww_VGA_B(5) <= \VGA_B[5]~output_o\;

ww_VGA_B(4) <= \VGA_B[4]~output_o\;

ww_VGA_B(3) <= \VGA_B[3]~output_o\;

ww_VGA_B(2) <= \VGA_B[2]~output_o\;

ww_VGA_B(1) <= \VGA_B[1]~output_o\;

ww_VGA_B(0) <= \VGA_B[0]~output_o\;

ww_VGA_G(7) <= \VGA_G[7]~output_o\;

ww_VGA_G(6) <= \VGA_G[6]~output_o\;

ww_VGA_G(5) <= \VGA_G[5]~output_o\;

ww_VGA_G(4) <= \VGA_G[4]~output_o\;

ww_VGA_G(3) <= \VGA_G[3]~output_o\;

ww_VGA_G(2) <= \VGA_G[2]~output_o\;

ww_VGA_G(1) <= \VGA_G[1]~output_o\;

ww_VGA_G(0) <= \VGA_G[0]~output_o\;

ww_VGA_R(7) <= \VGA_R[7]~output_o\;

ww_VGA_R(6) <= \VGA_R[6]~output_o\;

ww_VGA_R(5) <= \VGA_R[5]~output_o\;

ww_VGA_R(4) <= \VGA_R[4]~output_o\;

ww_VGA_R(3) <= \VGA_R[3]~output_o\;

ww_VGA_R(2) <= \VGA_R[2]~output_o\;

ww_VGA_R(1) <= \VGA_R[1]~output_o\;

ww_VGA_R(0) <= \VGA_R[0]~output_o\;
END structure;


