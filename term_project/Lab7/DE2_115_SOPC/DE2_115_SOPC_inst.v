	DE2_115_SOPC u0 (
		.ball_position_conduit_ball_y       (<connected-to-ball_position_conduit_ball_y>),       //       ball_position_conduit.ball_y
		.ball_position_conduit_ball_x       (<connected-to-ball_position_conduit_ball_x>),       //                            .ball_x
		.clk_clk                            (<connected-to-clk_clk>),                            //                         clk.clk
		.pio_led_external_connection_export (<connected-to-pio_led_external_connection_export>), // pio_led_external_connection.export
		.reset_reset_n                      (<connected-to-reset_reset_n>)                       //                       reset.reset_n
	);

