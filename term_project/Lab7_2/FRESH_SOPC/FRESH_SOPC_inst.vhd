	component FRESH_SOPC is
		port (
			clk_clk                                       : in  std_logic                     := 'X'; -- clk
			get_button_statuses_0_conduit_end_0_button_0  : in  std_logic                     := 'X'; -- button_0
			get_button_statuses_0_conduit_end_0_button_1  : in  std_logic                     := 'X'; -- button_1
			get_button_statuses_0_conduit_end_0_button_2  : in  std_logic                     := 'X'; -- button_2
			get_button_statuses_0_conduit_end_0_button_3  : in  std_logic                     := 'X'; -- button_3
			get_vsync_0_conduit_beginbursttransfer        : in  std_logic                     := 'X'; -- beginbursttransfer
			reset_reset_n                                 : in  std_logic                     := 'X'; -- reset_n
			reset_hardware_0_conduit_reset_hardware       : out std_logic;                            -- reset_hardware
			set_score_0_conduit_end_left_score            : out std_logic_vector(3 downto 0);         -- left_score
			set_score_0_conduit_end_right_score           : out std_logic_vector(3 downto 0);         -- right_score
			store_ball_position_0_conduit_ball_x          : out std_logic_vector(15 downto 0);        -- ball_x
			store_ball_position_0_conduit_ball_y          : out std_logic_vector(15 downto 0);        -- ball_y
			store_bat_positions_0_conduit_end_bat_left_y  : out std_logic_vector(15 downto 0);        -- bat_left_y
			store_bat_positions_0_conduit_end_bat_right_y : out std_logic_vector(15 downto 0)         -- bat_right_y
		);
	end component FRESH_SOPC;

	u0 : component FRESH_SOPC
		port map (
			clk_clk                                       => CONNECTED_TO_clk_clk,                                       --                                 clk.clk
			get_button_statuses_0_conduit_end_0_button_0  => CONNECTED_TO_get_button_statuses_0_conduit_end_0_button_0,  -- get_button_statuses_0_conduit_end_0.button_0
			get_button_statuses_0_conduit_end_0_button_1  => CONNECTED_TO_get_button_statuses_0_conduit_end_0_button_1,  --                                    .button_1
			get_button_statuses_0_conduit_end_0_button_2  => CONNECTED_TO_get_button_statuses_0_conduit_end_0_button_2,  --                                    .button_2
			get_button_statuses_0_conduit_end_0_button_3  => CONNECTED_TO_get_button_statuses_0_conduit_end_0_button_3,  --                                    .button_3
			get_vsync_0_conduit_beginbursttransfer        => CONNECTED_TO_get_vsync_0_conduit_beginbursttransfer,        --                 get_vsync_0_conduit.beginbursttransfer
			reset_reset_n                                 => CONNECTED_TO_reset_reset_n,                                 --                               reset.reset_n
			reset_hardware_0_conduit_reset_hardware       => CONNECTED_TO_reset_hardware_0_conduit_reset_hardware,       --            reset_hardware_0_conduit.reset_hardware
			set_score_0_conduit_end_left_score            => CONNECTED_TO_set_score_0_conduit_end_left_score,            --             set_score_0_conduit_end.left_score
			set_score_0_conduit_end_right_score           => CONNECTED_TO_set_score_0_conduit_end_right_score,           --                                    .right_score
			store_ball_position_0_conduit_ball_x          => CONNECTED_TO_store_ball_position_0_conduit_ball_x,          --       store_ball_position_0_conduit.ball_x
			store_ball_position_0_conduit_ball_y          => CONNECTED_TO_store_ball_position_0_conduit_ball_y,          --                                    .ball_y
			store_bat_positions_0_conduit_end_bat_left_y  => CONNECTED_TO_store_bat_positions_0_conduit_end_bat_left_y,  --   store_bat_positions_0_conduit_end.bat_left_y
			store_bat_positions_0_conduit_end_bat_right_y => CONNECTED_TO_store_bat_positions_0_conduit_end_bat_right_y  --                                    .bat_right_y
		);

