
module FRESH_SOPC (
	clk_clk,
	get_button_statuses_0_conduit_end_0_button_0,
	get_button_statuses_0_conduit_end_0_button_1,
	get_button_statuses_0_conduit_end_0_button_2,
	get_button_statuses_0_conduit_end_0_button_3,
	get_vsync_0_conduit_beginbursttransfer,
	reset_reset_n,
	reset_hardware_0_conduit_reset_hardware,
	set_score_0_conduit_end_left_score,
	set_score_0_conduit_end_right_score,
	store_ball_position_0_conduit_ball_x,
	store_ball_position_0_conduit_ball_y,
	store_bat_positions_0_conduit_end_bat_left_y,
	store_bat_positions_0_conduit_end_bat_right_y);	

	input		clk_clk;
	input		get_button_statuses_0_conduit_end_0_button_0;
	input		get_button_statuses_0_conduit_end_0_button_1;
	input		get_button_statuses_0_conduit_end_0_button_2;
	input		get_button_statuses_0_conduit_end_0_button_3;
	input		get_vsync_0_conduit_beginbursttransfer;
	input		reset_reset_n;
	output		reset_hardware_0_conduit_reset_hardware;
	output	[3:0]	set_score_0_conduit_end_left_score;
	output	[3:0]	set_score_0_conduit_end_right_score;
	output	[15:0]	store_ball_position_0_conduit_ball_x;
	output	[15:0]	store_ball_position_0_conduit_ball_y;
	output	[15:0]	store_bat_positions_0_conduit_end_bat_left_y;
	output	[15:0]	store_bat_positions_0_conduit_end_bat_right_y;
endmodule
