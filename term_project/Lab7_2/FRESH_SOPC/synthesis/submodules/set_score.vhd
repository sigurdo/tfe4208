library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity set_score is
    port (
        -- Processor interface
        clk         : in std_logic;
        clk_en      : in std_logic;
        start       : in std_logic;
        reset       : in std_logic;
        done        : out std_logic;
        dataa       : in std_logic_vector(31 downto 0);
        datab       : in std_logic_vector(31 downto 0);
        result      : out std_logic_vector(31 downto 0);

        -- External interface
        left_score  : out std_logic_vector(3 downto 0);
        right_score : out std_logic_vector(3 downto 0)
    );
end set_score;

architecture behavior of set_score is
    signal left_score_internal  : std_logic_vector(3 downto 0);
    signal right_score_internal : std_logic_vector(3 downto 0);
begin
    left_score  <= left_score_internal;
    right_score <= right_score_internal;
    main_p : process (clk)
    begin
        if rising_edge(clk) and clk_en = '1' then
            if reset = '1' then
                result               <= (others => '0');
                done                 <= '0';
                left_score_internal  <= (others => '0');
                right_score_internal <= (others => '0');
            else
                result               <= (others => '0');
                done                 <= '0';
                left_score_internal  <= left_score_internal;
                right_score_internal <= right_score_internal;
                if start = '1' then
                    result               <= x"0000002A";
                    done                 <= '1';
                    left_score_internal  <= dataa(3 downto 0);
                    right_score_internal <= datab(3 downto 0);
                end if;
            end if;
        end if;
    end process main_p;
end;
