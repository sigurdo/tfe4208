	FRESH_SOPC u0 (
		.clk_clk                                       (<connected-to-clk_clk>),                                       //                                 clk.clk
		.get_button_statuses_0_conduit_end_0_button_0  (<connected-to-get_button_statuses_0_conduit_end_0_button_0>),  // get_button_statuses_0_conduit_end_0.button_0
		.get_button_statuses_0_conduit_end_0_button_1  (<connected-to-get_button_statuses_0_conduit_end_0_button_1>),  //                                    .button_1
		.get_button_statuses_0_conduit_end_0_button_2  (<connected-to-get_button_statuses_0_conduit_end_0_button_2>),  //                                    .button_2
		.get_button_statuses_0_conduit_end_0_button_3  (<connected-to-get_button_statuses_0_conduit_end_0_button_3>),  //                                    .button_3
		.get_vsync_0_conduit_beginbursttransfer        (<connected-to-get_vsync_0_conduit_beginbursttransfer>),        //                 get_vsync_0_conduit.beginbursttransfer
		.reset_reset_n                                 (<connected-to-reset_reset_n>),                                 //                               reset.reset_n
		.reset_hardware_0_conduit_reset_hardware       (<connected-to-reset_hardware_0_conduit_reset_hardware>),       //            reset_hardware_0_conduit.reset_hardware
		.set_score_0_conduit_end_left_score            (<connected-to-set_score_0_conduit_end_left_score>),            //             set_score_0_conduit_end.left_score
		.set_score_0_conduit_end_right_score           (<connected-to-set_score_0_conduit_end_right_score>),           //                                    .right_score
		.store_ball_position_0_conduit_ball_x          (<connected-to-store_ball_position_0_conduit_ball_x>),          //       store_ball_position_0_conduit.ball_x
		.store_ball_position_0_conduit_ball_y          (<connected-to-store_ball_position_0_conduit_ball_y>),          //                                    .ball_y
		.store_bat_positions_0_conduit_end_bat_left_y  (<connected-to-store_bat_positions_0_conduit_end_bat_left_y>),  //   store_bat_positions_0_conduit_end.bat_left_y
		.store_bat_positions_0_conduit_end_bat_right_y (<connected-to-store_bat_positions_0_conduit_end_bat_right_y>)  //                                    .bat_right_y
	);

