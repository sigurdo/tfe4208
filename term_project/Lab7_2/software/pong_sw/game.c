#include <stdio.h>
#include <stdlib.h>
#include "system.h"
#include "game.h"
#define _USE_MATH_DEFINES // for C
#include <math.h>

void set_bat_pos(enum Button button_press, Bat *bat)
{
    if (button_press == Up && bat->y - bat_height > 0)
    {
        bat->y -= button_increment;
    }
    else if (button_press == Down && bat->y + bat_height < vertical_pixels)
    {
        bat->y += button_increment;
    }
};

void set_ball_pos_and_speed(Ball *ball, Bat right_bat, Bat left_bat, Score *score)
{

    Ball next_ball;
    next_ball.x = ball->x + ball->speed_x;
    next_ball.y = ball->y + ball->speed_y;

    if (next_ball.y + ball_radius >= vertical_pixels || next_ball.y - ball_radius <= 0)
    {
        // If the ball collides with the top or bottom of the screen we invert the speed in y direction
        ball->speed_y = -ball->speed_y;
        ball->x += ball->speed_x;

        if (next_ball.y + ball_radius >= vertical_pixels)
        {
            ball->y = vertical_pixels - ball_radius;
        }
        else
        {
            ball->y = ball_radius;
        }
    }
    else if (next_ball.x + ball_radius >= horizontal_pixels || next_ball.x - ball_radius <= 0)
    {
        // If the ball collieds with one of the sides it is a goal, and we reset it back to the middel.
        ball->x = horizontal_pixels / 2;
        ball->y = vertical_pixels / 2;
        ball->absolut_speed = 4;
        ball->speed_x = 4;
        ball->speed_y = 0;

        if (next_ball.x + ball_radius >= horizontal_pixels)
        {
            score->left_player++;
        }
        else
        {
            score->right_player++;
        }
        ball->speed_y = 0;
    }
    else if (next_ball.x + ball_radius >= right_bat.x - bat_width && next_ball.y + ball_radius >= right_bat.y - bat_height && next_ball.y - ball_radius <= right_bat.y + bat_height)
    {
        // It hits  the right bat
        ball->x = right_bat.x - bat_width - ball_radius;

        double max_bounce_angle = M_PI / 4;
        double intersect_y = (next_ball.y - right_bat.y) / bat_height;
        double intersect_angle = atan(ball->speed_y / abs(ball->speed_x));
        double angle = intersect_y * max_bounce_angle + intersect_angle;

        if (angle > (M_PI / 4))
            angle = (M_PI / 4);
        if (angle < -(M_PI / 4))
            angle = -(M_PI / 4);
        ball->speed_x = -ball->absolut_speed * cos(angle);
        ball->speed_y = ball->absolut_speed * sin(angle);

        ball->absolut_speed += ball_speed_increment;
    }
    else if (next_ball.x - ball_radius <= left_bat.x + bat_width && next_ball.y + ball_radius >= left_bat.y - bat_height && next_ball.y - ball_radius <= left_bat.y + bat_height)
    {

        // It hits  the left bat

        ball->x = left_bat.x + bat_width + ball_radius;

        double max_bounce_angle = M_PI / 4;
        double intersect_y = (next_ball.y - left_bat.y) / bat_height;
        double intersect_angle = atan(ball->speed_y / abs(ball->speed_x));
        double angle = intersect_y * max_bounce_angle + intersect_angle;
        // printf("angle: %f\n", angle);
        if (angle > (M_PI / 4))
            angle = (M_PI / 4);
        if (angle < -(M_PI / 4))
            angle = -(M_PI / 4);
        ball->speed_x = ball->absolut_speed * cos(angle);
        ball->speed_y = ball->absolut_speed * sin(angle);

        ball->absolut_speed += ball_speed_increment;
    }
    else
    {
        ball->x += ball->speed_x;
        ball->y += ball->speed_y;
    }
};
