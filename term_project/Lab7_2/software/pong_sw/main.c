#include "game.h"
#include "system.h"

int main()
{
    enum GameState state = IDLE;
    enum Button button_press = None;
    int button_statuses;
    Score game_score;
    Ball ball;

    Bat right_bat;
    Bat left_bat;

    right_bat.x = horizontal_pixels - bat_pos_x;
    left_bat.x = bat_pos_x;

    ball.x = horizontal_pixels / 2;
    ball.y = vertical_pixels / 2;
    left_bat.y = vertical_pixels / 2;
    right_bat.y = vertical_pixels / 2;
    ball.absolut_speed = 4;
    ball.speed_x = 4;
    ball.speed_y = 0;
    game_score.left_player = 0;
    game_score.right_player = 0;

    // Store positions to hardware
    ALT_CI_STORE_BALL_POSITION_0(ball.x, ball.y);
    ALT_CI_STORE_BAT_POSITIONS_0(left_bat.y, right_bat.y);
    ALT_CI_SET_SCORE_0(game_score.left_player, game_score.right_player);

    while (1)
    {
        switch (state)
        {
        case IDLE:
            // Wait for button press
            button_statuses = ALT_CI_GET_BUTTON_STATUSES_0(69, 420);
            if (button_statuses & 0xf)
            {
                ball.x = horizontal_pixels / 2;
                ball.y = vertical_pixels / 2;
                left_bat.y = vertical_pixels / 2;
                right_bat.y = vertical_pixels / 2;
                ball.absolut_speed = 4;
                ball.speed_x = 4;
                ball.speed_y = 0;
                game_score.left_player = 0;
                game_score.right_player = 0;
                state = PLAYING;
            }
            break;

        case PLAYING:
            // Read button press
            button_statuses = ALT_CI_GET_BUTTON_STATUSES_0(69, 420);

            // Update batt positon
            button_press = None;
            switch (button_statuses & 0b1100)
            {
            case 0b1000:
                button_press = Down;
                break;
            case 0b0100:
                button_press = Up;
                break;
            }
            update_bat_pos(button_press, &left_bat);
            button_press = None;
            switch (button_statuses & 0b0011)
            {
            case 0b0010:
                button_press = Down;
                break;
            case 0b0001:
                button_press = Up;
                break;
            }
            update_bat_pos(button_press, &right_bat);

            // Call get ball_poss and speed
            get_ball_pos_and_speed(&ball, right_bat, left_bat, &game_score);

            // Store positions to hardware
            ALT_CI_STORE_BALL_POSITION_0(ball.x, ball.y);
            ALT_CI_STORE_BAT_POSITIONS_0(left_bat.y, right_bat.y);
            ALT_CI_SET_SCORE_0(game_score.left_player, game_score.right_player);

            // Hacky sleep
            // for (int i = 0; i < 100000; i++) {
            //     ALT_CI_STORE_BALL_POSITION_0(ball.x, ball.y);
            // }

            // Good sleep
            while (1)
            {
                if (ALT_CI_GET_VSYNC_0(69, 420) == 0)
                    break;
            }
            while (1)
            {
                if (ALT_CI_GET_VSYNC_0(69, 420) == 1)
                    break;
            }

            if (game_score.left_player == max_score || game_score.right_player == max_score)
            {
                state = IDLE;
            }
            break;
        }
    }
}
