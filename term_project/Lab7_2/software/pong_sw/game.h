#define vertical_pixels 600
#define horizontal_pixels 800

#define ball_radius 20

#define bat_width 20
#define bat_height 40
#define bat_pos_x 30

#define button_increment 8
#define ball_speed_increment 0.3

#define max_score 10

typedef struct Ball
{
  // Every value is in number of pixels.
  double x;
  double y;
  double speed_x;
  double speed_y;
  double absolut_speed;
} Ball;

typedef struct Bat
{
  double x;
  double y;
} Bat;

typedef struct Score
{
  int right_player;
  int left_player;
} Score;

enum GameState
{
  IDLE,
  PLAYING,
};

enum Button
{
  Up,
  Down,
  None
};

void update_bat_pos(enum Button button_press, Bat *bat);

void get_ball_pos_and_speed(Ball *ball, Bat right_bat, Bat left_bat, Score *score);
