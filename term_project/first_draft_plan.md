# Ting som må på plass for en førsteutkast-test

- Førsteutkast-testen skal rendre en statisk ball og køller men uten noe spillogikk og knappefunksjonalitet. 

## TODO
- SW
    - Resette systemet
    - Sette ball- og kølleposisjoner
    - Generere nytt SW-prosjekt
- HW
    - Drawer må lese inn v_sync istedenfor start (done)
    - Koble v_sync til drawer
    - Lage custom instruction for reset (done)
    - Lage custom instruction for kølleposisjoner (done)
    - Koble opp nye custom instructions
